<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('mobile-number', 'API\AuthController@mobile_number');
Route::post('verify-otp', 'API\AuthController@verify_otp');
Route::get('get-event', 'API\AuthController@get_event');
Route::get('get-event-detail/{id}', 'API\AuthController@get_event_detail');
Route::get('get-contact', 'API\AuthController@get_contact');
Route::get('get-society', 'API\AuthController@get_society');

Route::post('add-member', 'API\AuthController@add_member');
Route::get('get-member-detail/{user_id}', 'API\AuthController@get_member_detail');
Route::get('remove-member/{member_id}', 'API\AuthController@remove_member');


Route::post('add-staff', 'API\AuthController@add_staff');
Route::get('get-staff-detail/{user_id}', 'API\AuthController@get_staff_detail');
Route::get('remove-staff/{staff_id}', 'API\AuthController@remove_staff');





