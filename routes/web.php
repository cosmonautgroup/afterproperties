<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Clear Cache
Route::get('/cache-clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:cache');
    return '<h1>Config cache successfully !!</h1>';
});

Route::get('/', function(){
	return redirect('login');
});

/*AUTH ROUTES*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/login', 'Auth\LoginController@index')->name('login');
Route::get('/login', 'Auth\LoginController@login')->name('showlogin');

Route::post('/forgot', 'Auth\ForgotPasswordController@forgot')->name('forgot');
Route::post('/resetPassword', 'Auth\ResetPasswordController@resetPassword')->name('resetPassword');
Route::post('/register', 'Auth\RegisterController@create')->name('register');
Route::get('/accountactivate/{key}', 'Auth\RegisterController@accountActivate')->name('accountactivate');
Route::get('/reset-password/{key}', 'Auth\ForgotPasswordController@reset')->name('reset-password');

/*END AUTH ROUTES*/


Route::group(['middleware' => ['auth']], function ($request) {

	/*Dashboard*/
	Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard');
	/*Profile*/
	Route::get('admin/profile', 'Admin\ProfileController@index')->name('profile');
	Route::post('admin/profile/update', 'Admin\ProfileController@update')->name('profile-update');

	Route::group(['middleware' => ['admin-auth']], function ($request) {
		//Roles
		Route::get('admin/roles', 'Admin\RolesController@index')->name('roles');
		Route::post('admin/roles/list', 'Admin\RolesController@getData')->name('get-roles');
		Route::get('admin/roles/add', 'Admin\RolesController@add')->name('add-role');
		Route::post('admin/roles/create', 'Admin\RolesController@create')->name('create-role');
		Route::get('admin/roles/edit/{id}', 'Admin\RolesController@edit')->name('edit-role');
		Route::post('admin/roles/update', 'Admin\RolesController@update')->name('update-role');
		Route::get('admin/roles/delete/{id}', 'Admin\RolesController@delete')->name('delete-role');
		Route::get('admin/roles/permission/{id}', 'Admin\RolesController@permission')->name('menu-permission');
		Route::post('admin/roles/create-permission', 'Admin\RolesController@createPermission')->name('create-permission');

		//Menu
		Route::get('admin/menus', 'Admin\MenusController@index')->name('menus');
		Route::post('admin/menus/list', 'Admin\MenusController@getData')->name('get-menus');
		Route::get('admin/menus/add', 'Admin\MenusController@add')->name('add-menu');
		Route::post('admin/menus/create', 'Admin\MenusController@create')->name('create-menu');
		Route::get('admin/menus/edit/{id}', 'Admin\MenusController@edit')->name('edit-menu');
		Route::post('admin/menus/update', 'Admin\MenusController@update')->name('update-menu');
		Route::get('admin/menus/delete/{id}', 'Admin\MenusController@delete')->name('delete-menu');

		//SubAdmin
		Route::get('admin/subadmin', 'Admin\SubAdminController@index')->name('subadmin');
		Route::post('admin/subadmin/list', 'Admin\SubAdminController@getData')->name('get-subadmin');
		Route::get('admin/subadmin/add', 'Admin\SubAdminController@add')->name('add-subadmin');
		Route::post('admin/subadmin/create', 'Admin\SubAdminController@create')->name('create-subadmin');
		Route::get('admin/subadmin/edit/{id}', 'Admin\SubAdminController@edit')->name('edit-subadmin');
		Route::post('admin/subadmin/update', 'Admin\SubAdminController@update')->name('update-subadmin');
		Route::get('admin/subadmin/delete/{id}', 'Admin\SubAdminController@delete')->name('delete-subadmin');
		Route::get('admin/subadmin/status/{id}', 'Admin\SubAdminController@status')->name('subadmin-status');

		//SocityAdmin
		Route::get('admin/socityadmin', 'Admin\SocityAdminController@index')->name('socityadmin');
		Route::post('admin/socityadmin/list', 'Admin\SocityAdminController@getData')->name('get-socityadmin');
		Route::get('admin/socityadmin/add', 'Admin\SocityAdminController@add')->name('add-socityadmin');
		Route::post('admin/socityadmin/create', 'Admin\SocityAdminController@create')->name('create-socityadmin');
		Route::get('admin/socityadmin/edit/{id}', 'Admin\SocityAdminController@edit')->name('edit-socityadmin');
		Route::post('admin/socityadmin/update', 'Admin\SocityAdminController@update')->name('update-socityadmin');
		Route::get('admin/socityadmin/delete/{id}', 'Admin\SocityAdminController@delete')->name('delete-socityadmin');
		Route::get('admin/socityadmin/status/{id}', 'Admin\SocityAdminController@status')->name('socityadmin-status');
		Route::get('admin/socityadmin/view/{id}', 'Admin\SocityAdminController@view')->name('view-socityadmin');

		//SocityBlockAdmin
		Route::get('admin/blockadmin', 'Admin\SocityBlockAdminController@index')->name('blockadmin');
		Route::post('admin/blockadmin/list', 'Admin\SocityBlockAdminController@getData')->name('get-blockadmin');
		Route::get('admin/blockadmin/add', 'Admin\SocityBlockAdminController@add')->name('add-blockadmin');
		Route::post('admin/blockadmin/create', 'Admin\SocityBlockAdminController@create')->name('create-blockadmin');
		Route::get('admin/blockadmin/edit/{id}', 'Admin\SocityBlockAdminController@edit')->name('edit-blockadmin');
		Route::post('admin/blockadmin/update', 'Admin\SocityBlockAdminController@update')->name('update-blockadmin');
		Route::get('admin/blockadmin/delete/{id}', 'Admin\SocityBlockAdminController@delete')->name('delete-blockadmin');
		Route::get('admin/blockadmin/status/{id}', 'Admin\SocityBlockAdminController@status')->name('blockadmin-status');
		Route::get('admin/socityadmin/view/{id}', 'Admin\SocityBlockAdminController@view')->name('view-blockadmin');

		//Subscription
		Route::get('admin/subscription', 'Admin\SubscriptionController@index')->name('subscription');
		Route::post('admin/subscription/list', 'Admin\SubscriptionController@getData')->name('get-subscription');
		Route::get('admin/subscription/add', 'Admin\SubscriptionController@add')->name('add-subscription');
		Route::post('admin/subscription/create', 'Admin\SubscriptionController@create')->name('subscription-create');
		Route::get('admin/subscription/edit/{id}', 'Admin\SubscriptionController@edit')->name('edit-subscription');
		Route::post('admin/subscription/update', 'Admin\SubscriptionController@update')->name('update-subscription');
		Route::get('admin/subscription/delete/{id}', 'Admin\SubscriptionController@delete')->name('delete-subscription');

		//Subscription
		Route::get('admin/socity', 'Admin\SocityController@index')->name('socity');
		Route::post('admin/socity/list', 'Admin\SocityController@getData')->name('get-socity');
		Route::get('admin/socity/add', 'Admin\SocityController@add')->name('add-socity');
		Route::post('admin/socity/create', 'Admin\SocityController@create')->name('create-socity');
		Route::get('admin/socity/edit/{id}', 'Admin\SocityController@edit')->name('edit-socity');
		Route::post('admin/socity/update', 'Admin\SocityController@update')->name('update-socity');
		Route::get('admin/socity/delete/{id}', 'Admin\SocityController@delete')->name('delete-socity');


		//Subscription
		Route::get('admin/block', 'Admin\BlockController@index')->name('block');
		Route::post('admin/block/list', 'Admin\BlockController@getData')->name('get-block');
		Route::get('admin/block/add', 'Admin\BlockController@add')->name('add-block');
		Route::post('admin/block/create', 'Admin\BlockController@create')->name('create-block');
		Route::get('admin/block/edit/{id}', 'Admin\BlockController@edit')->name('edit-block');
		Route::post('admin/block/update', 'Admin\BlockController@update')->name('update-block');
		Route::get('admin/block/delete/{id}', 'Admin\BlockController@delete')->name('delete-block');

		//Units
		Route::get('admin/units', 'Admin\UnitsController@index')->name('units');
		Route::post('admin/units/list', 'Admin\UnitsController@getData')->name('get-units');
		Route::get('admin/units/add', 'Admin\UnitsController@add')->name('add-units');
		Route::post('admin/units/create', 'Admin\UnitsController@create')->name('create-units');
		Route::get('admin/units/edit/{id}', 'Admin\UnitsController@edit')->name('edit-units');
		Route::post('admin/units/update', 'Admin\UnitsController@update')->name('update-units');
		Route::get('admin/units/delete/{id}', 'Admin\UnitsController@delete')->name('delete-units');
		Route::get('admin/units/floor','Admin\UnitsController@getFloor')->name('units/floor');
		Route::get('admin/units/unit','Admin\UnitsController@getUnit')->name('units/unit');

		//Parking
		Route::get('admin/parking', 'Admin\ParkingController@index')->name('parking');
		Route::post('admin/parking/list', 'Admin\ParkingController@getData')->name('get-parking');
		Route::get('admin/parking/add', 'Admin\ParkingController@add')->name('add-parking');
		Route::post('admin/parking/create', 'Admin\ParkingController@create')->name('create-parking');
		Route::get('admin/parking/edit/{id}', 'Admin\ParkingController@edit')->name('edit-parking');
		Route::post('admin/parking/update', 'Admin\ParkingController@update')->name('update-parking');
		Route::get('admin/parking/delete/{id}', 'Admin\ParkingController@delete')->name('delete-parking');
		Route::get('admin/parking/floor','Admin\ParkingController@getFloor')->name('parking/floor');
		Route::get('admin/parking/unit','Admin\ParkingController@getUnit')->name('parking/unit');

		//Parking
		Route::get('admin/maintenance', 'Admin\MaintenanceController@index')->name('maintenance');
		Route::post('admin/maintenance/list', 'Admin\MaintenanceController@getData')->name('get-maintenance');
		Route::get('admin/maintenance/add', 'Admin\MaintenanceController@add')->name('add-maintenance');
		Route::post('admin/maintenance/create', 'Admin\MaintenanceController@create')->name('create-maintenance');
		Route::get('admin/maintenance/edit/{id}', 'Admin\MaintenanceController@edit')->name('edit-maintenance');
		Route::post('admin/maintenance/update', 'Admin\MaintenanceController@update')->name('update-maintenance');
		Route::get('admin/maintenance/delete/{id}', 'Admin\MaintenanceController@delete')->name('delete-maintenance');
		Route::get('admin/maintenance/block','Admin\MaintenanceController@getBlock')->name('maintenance/block');

		/*Event*/
		Route::get('admin/event', 'Admin\EventController@index')->name('event');
		Route::post('admin/event/list', 'Admin\EventController@getData')->name('get-event');
		Route::get('admin/event/add', 'Admin\EventController@add')->name('add-event');
		Route::post('admin/event/create', 'Admin\EventController@create')->name('create-event');
		Route::get('admin/event/edit/{id}', 'Admin\EventController@edit')->name('edit-event');
		Route::post('admin/event/update', 'Admin\EventController@update')->name('update-event');
		Route::get('admin/event/delete/{id}', 'Admin\EventController@delete')->name('delete-event');

		/*Amenites*/
		Route::get('admin/amenites', 'Admin\AmenitesController@index')->name('amenites');
		Route::post('admin/amenites/list', 'Admin\AmenitesController@getData')->name('get-amenites');
		Route::get('admin/amenites/add', 'Admin\AmenitesController@add')->name('add-amenites');
		Route::post('admin/amenites/create', 'Admin\AmenitesController@create')->name('create-amenites');
		Route::get('admin/amenites/edit/{id}', 'Admin\AmenitesController@edit')->name('edit-amenites');
		Route::post('admin/amenites/update', 'Admin\AmenitesController@update')->name('update-amenites');
		Route::get('admin/amenites/delete/{id}', 'Admin\AmenitesController@delete')->name('delete-amenites');

		/*Country*/
		Route::get('admin/country', 'Admin\CountryController@index')->name('country');
		Route::post('admin/country/list', 'Admin\CountryController@getData')->name('get-country');
		Route::get('admin/country/add', 'Admin\CountryController@add')->name('add-country');
		Route::post('admin/country/create', 'Admin\CountryController@create')->name('create-country');
		Route::get('admin/country/edit/{id}', 'Admin\CountryController@edit')->name('edit-country');
		Route::get('admin/country/delete/{id}', 'Admin\CountryController@delete')->name('delete-country');

		/*State*/
		Route::get('admin/state', 'Admin\StateController@index')->name('state');
		Route::post('admin/state/list', 'Admin\StateController@getData')->name('get-state');
		Route::get('admin/state/add', 'Admin\StateController@add')->name('add-state');
		Route::post('admin/state/create', 'Admin\StateController@create')->name('create-state');
		Route::get('admin/state/edit/{id}', 'Admin\StateController@edit')->name('edit-state');
		Route::get('admin/state/delete/{id}', 'Admin\StateController@delete')->name('delete-state');

		/*State*/
		Route::get('admin/city', 'Admin\CityController@index')->name('city');
		Route::post('admin/city/list', 'Admin\CityController@getData')->name('get-city');
		Route::get('admin/city/add', 'Admin\CityController@add')->name('add-city');
		Route::post('admin/city/create', 'Admin\CityController@create')->name('create-city');
		Route::get('admin/city/edit/{id}', 'Admin\CityController@edit')->name('edit-city');
		Route::get('admin/city/delete/{id}', 'Admin\CityController@delete')->name('delete-city');
		Route::get('admin/city/getstate', 'Admin\CityController@getState')->name('city-state');

		//Logs
		Route::get('admin/logs', 'Admin\LogsController@index')->name('logs');
		Route::post('admin/logs/list', 'Admin\LogsController@getData')->name('get-logs');

		//Settings
		Route::get('admin/settings', 'Admin\SettingsController@index')->name('settings');
		Route::post('admin/settings/create', 'Admin\SettingsController@create')->name('create-settings');

		//Contactleads
		Route::get('admin/news-letter', 'Admin\ContactleadsController@index')->name('news-letter');
		Route::post('admin/news-letter/list', 'Admin\ContactleadsController@getData')->name('get-news-letter');

		/*CMS PAGES*/
		Route::get('admin/cms', 'Admin\CmsController@index')->name('cms-page');
		Route::post('admin/cms/list', 'Admin\CmsController@getData')->name('get-cms');
		Route::get('admin/cms/add', 'Admin\CmsController@add')->name('add-cms');
		Route::post('admin/cms/create', 'Admin\CmsController@create')->name('create-cms');
		Route::get('admin/cms/edit/{id}', 'Admin\CmsController@edit')->name('edit-cms');
		Route::post('admin/cms/update', 'Admin\CmsController@update')->name('update-cms');
		Route::get('admin/cms/status/{id}', 'Admin\CmsController@status')->name('cms-status');
		Route::get('admin/cms/delete/{id}', 'Admin\CmsController@delete')->name('delete-cms');
		Route::get('admin/cms/view/{id}', 'Admin\CmsController@view')->name('view-cms');

		/*Post*/
		Route::get('admin/posts', 'Admin\PostController@index')->name('posts');
		Route::post('admin/posts/list', 'Admin\PostController@getData')->name('get-posts');
		Route::get('admin/posts/add', 'Admin\PostController@add')->name('add-posts');
		Route::post('admin/posts/create', 'Admin\PostController@create')->name('create-posts');
		Route::get('admin/posts/edit/{id}', 'Admin\PostController@edit')->name('edit-posts');
		Route::post('admin/posts/update', 'Admin\PostController@update')->name('update-posts');
		Route::get('admin/posts/delete/{id}', 'Admin\PostController@delete')->name('delete-posts');
		Route::get('admin/posts/status/{id}', 'Admin\PostController@status')->name('posts-status');
		Route::get('admin/posts/menu/{id}', 'Admin\PostController@menu')->name('posts-menu');
		Route::get('admin/posts/view/{id}', 'Admin\PostController@view')->name('view-posts');


	});

	Route::group(['middleware' => ['sub-admin-auth']], function ($request) {
	});

	Route::group(['middleware' => ['society-admin-auth']], function ($request) {
	});

	Route::group(['middleware' => ['society-block-admin-auth']], function ($request) {
	});

});