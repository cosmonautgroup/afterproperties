<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class Maintenance extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'maintenance';
    protected $fillable = [
       'socity_id', 'block_id','ammount','unit_resident_type','payment_cycle', 'is_active','is_delete','creatde_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function getMaintenance($data=[]){
        $request = Self::select('maintenance.*','socity.society_name','blocks.name as blockname')
                 ->leftjoin('socity','socity.id','maintenance.socity_id')
                 ->leftjoin('blocks','blocks.id','maintenance.block_id')
                 ->orderBy('id','DESC')
                 ->get();
               /*  echo "<pre>";
                 print_r($request);
                 exit();*/
        return $request;
    }
}
