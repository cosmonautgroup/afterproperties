<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Auth;

class Logs extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'role_id', 'user_id', 'username','message','action', 'log_date_time', 'created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static function getLogs($data=[]){

        $response = Self::select('logs.*','roles.name as role_name');
        $response = $response->join('roles','roles.id','logs.role_id');

        if (isset($data['user_id']) && intval($data['user_id']) > 0) {
            $response = $response->where('logs.user_id', $data['user_id']);
        }
        $response = $response->orderBy('logs.id','desc');
        $response = $response->get();

        /*print_r($response);
        exit;*/
        return $response;            
    }


    public static function addLog($action,$message){

        $user = Auth::user();

        if($user->hasRole('Admin')){
           $role_id = 1;
           $role = "Admin";

           $message = $role ." <b>' ".$user->name." '</b> ".$message;

           Self::insert([
                'role_id' => $role_id,
                'user_id' => $user->id,
                'username' => $user->name,
                'role_name' => $role,
                'action' => $action,
                'message' => $message,
                'log_date_time' => date('Y-m-d H:i:s')
            ]);
        }
        if($user->hasRole('Sub-Admin')){
            $role_id = 2;
            $role = "Sub Admin";

            $message = $role ." <b>' ".$user->name." '</b> ".$message;

            Self::insert([
                'role_id' => $role_id,
                'user_id' => $user->id,
                'username' => $user->name,
                'role_name' => $role,
                'action' => $action,
                'message' => $message,
                'log_date_time' => date('Y-m-d H:i:s')
            ]);
        }
        if($user->hasRole('Society-Admin')){
            $role_id = 3;
            $role = "Society Admin";

            $message = $role ." <b>' ".$user->name." '</b> ".$message;

            Self::insert([
                'role_id' => $role_id,
                'user_id' => $user->id,
                'username' => $user->name,
                'role_name' => $role,
                'action' => $action,
                'message' => $message,
                'log_date_time' => date('Y-m-d H:i:s')
            ]);
        }
        if($user->hasRole('Society-Block-Admin')){
            $role_id = 4;
            $role = "Society Block Admin";

            $message = $role ." <b>' ".$user->name." '</b> ".$message;

            Self::insert([
                'role_id' => $role_id,
                'user_id' => $user->id,
                'username' => $user->name,
                'role_name' => $role,
                'action' => $action,
                'message' => $message,
                'log_date_time' => date('Y-m-d H:i:s')
            ]);
        }
    }
}
