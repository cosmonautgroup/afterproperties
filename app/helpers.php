<?php

function resizeSmallThumb($imageName){

         $sourceUrl = public_path('images/users/');
         $sourceUrl .= "/".$imageName;
         $destinationPath = public_path('/images/users_thumb/');
         $destinationPath .= "/".$imageName;
         if(file_exists($sourceUrl)){
             switch(mime_content_type($sourceUrl)) {
                 case 'image/png':
                     $image = imagecreatefrompng($sourceUrl);
                     break;
                 case 'image/jpeg':
                     $image = imagecreatefromjpeg($sourceUrl);
                     break;
                 case 'image/jpg':
                     $newimage = imagecreatefromjpeg($sourceUrl);
                     break;
               default:
                 $image = null;
             }
             $filename       = $destinationPath;
             $imageSize      = getimagesize($sourceUrl);
             $thumb_width    = 400;
             $thumb_height   = $imageSize[1];
             $width          = $imageSize[0];
             $height         = $imageSize[1];
             $thumb_height   = ($thumb_width * $height) / $width;
             $thumb_height   = 350;
             $destinationPath = getResizeImage($image,$filename,$thumb_height,$thumb_width);
             imageCompress($destinationPath);
         }
     }

function getResizeImage($image,$filename,$thumb_height,$thumb_width){

   $width = imagesx($image);
   $height = imagesy($image);

   $original_aspect = $width / $height;
   $thumb_aspect = $thumb_width / $thumb_height;

   if ( $original_aspect >= $thumb_aspect ){
      // If image is wider than thumbnail (in aspect ratio sense)
      $new_height  = $thumb_height;
      $new_width   = $width / ($height / $thumb_height);
   }else{
       $new_width  = $thumb_width;
       $new_height = $height / ($width / $thumb_width);
      // If the thumbnail is wider than the image
   }
   $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
   // Resize and crop
   imagecopyresampled($thumb,
                      $image,
                      0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                      0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                      0, 0,
                      $new_width, $new_height,
                      $width, $height);
   imagejpeg($thumb, $filename, 100);
   return $filename;
}

function imageCompress($url){
   $factory = new \ImageOptimizer\OptimizerFactory();
   $optimizer = $factory->get();
   $optimizer->optimize($url);
}

function getAction($key){
     $action = array(
      '1' => 'Logged in',
      '2' => 'Added',
      '3' => 'Update',
      '4' => 'Delete',
      '5' => 'Set',
      '6' => 'Scanned'
    );
    if (array_key_exists($key, $action)) {
      return $action[$key];
    }else{
      return "Action";
    }
}

function getClientBillSevices($bill_id){
  $bill_service =  App\BillService::getClientBillServices(['client_bill_id'=>$bill_id]);
  return $bill_service;
}

function getZoneName($zone_id){
  if(isset($zone_id) && $zone_id > 0){
    $zone = App\Zone::where('id',$zone_id)->first();
    $zone_name = isset($zone->name)?($zone->name):"NA";
    return $zone_name;
  }else{
    return "NA";
  }
}

function getUserById($user_id){
  $user_row = App\User::where("id",$user_id)->first();
  return $user_row;
}

function status_action($url,$status){
  if($status == 1){
      $action = "<a href='".$url."'><span class='m-badge  m-badge--success m-badge--wide'>Active</span></a>";
  }else{
    $action = "<a href='".$url."'><span class='m-badge  m-badge--danger m-badge--wide'>Deactive</span></a>";
  }
  return $action;
}



?>