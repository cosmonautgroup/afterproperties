<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class State extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'state';
    protected $fillable = [
       'id', 'country_id','state_name', 'is_active','is_delete','creatde_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function getState($data=[]){
        $request = Self::select('state.*','country.country_name')
                 ->leftjoin('country','country.id','state.country_id')
                 ->orderBy("id","DESC")
                 ->get();
        return $request;
    }
}
