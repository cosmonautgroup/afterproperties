<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Auth;

class Settings extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'settings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'meta_title', 'meta_keyword','meta_description','site_title', 'site_logo','favicon','copyright_text','contactus_email','facebook_link','twitter_link','instagram_link','address','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static function getSettings(){
        $settings = Self::where("id",1)->first();
        return $settings;
    }
}
