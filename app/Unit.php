<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class Unit extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'unit';
    protected $fillable = [
       'block_id', 'floor_id','unit_id','bhk','view','squarfeet','parking_no','assign_user', 'is_active','is_delete','creatde_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function getUnit($data=[]){
        $request = Self::select('unit.*','units.unit_name','floors.floor','blocks.name','users.first_name')
                 ->leftjoin('units','units.id','unit.unit_id')
                 ->leftjoin('floors','floors.id','unit.floor_id')
                 ->leftjoin('blocks','blocks.id','unit.block_id')
                 ->leftjoin('users','users.id','unit.assign_user')
                 ->orderBy("id","DESC")
                 ->get();
        return $request;
    }
}
