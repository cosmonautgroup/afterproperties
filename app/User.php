<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use Notifiable, HasApiTokens;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email','phone', 'city', 'address', 'password','image','is_active','is_verified','created_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function getUsers($data=[]){
        $response = Self::select('users.*');
        $response = $response->leftjoin('model_has_roles','users.id','model_has_roles.model_id');
        if (isset($data['role']) && intval($data['role']) > 0) {
            $response = $response->where('model_has_roles.role_id', $data['role']);
        }
        if (!empty($data['collectors'])) {
            $response = $response->whereIn('users.created_by', $data['collectors']);
        }
        if (isset($data['created_by']) && intval($data['created_by']) > 0) {
            $response = $response->where('users.created_by', $data['created_by']);
        }

        $response = $response->get();
        return $response;            
    }

    public static function getCollectors($data=[]){
        $response = Self::select('users.id');
        $response = $response->leftjoin('model_has_roles','users.id','model_has_roles.model_id');
        if (isset($data['role']) && intval($data['role']) > 0) {
            $response = $response->where('model_has_roles.role_id', $data['role']);
        }
        if (isset($data['created_by']) && intval($data['created_by']) > 0) {
            $response = $response->where('users.created_by', $data['created_by']);
        }
        $data =[];
        $response = $response->get();
        if (!empty($response)) {
            foreach ($response as $row) {
                $data[] = $row->id;
            }
        }
        return $data;            
    }
}
