<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class Amenites extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'amenites';
    protected $fillable = [
       'amenites_name','time','end_time','avilable','amenites_rule', 'is_active','is_delete','creatde_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function getAmenites($data=[]){
        $request = Self::select('amenites.*','socity.society_name')
                 ->leftjoin('socity','socity.id','amenites.socity_id')
                 ->orderBy('id','DESC')
                 ->get();
        return $request;
    }
}
