<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Auth;

class Posts extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'posts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'category_id', 'is_menu','is_active','is_delete','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static function getPost($data=[]){

        $response = Self::select('posts.*');
        $response = $response->orderBy('posts.id','desc');
        $response = $response->get();
        return $response;
    }
}
