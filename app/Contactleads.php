<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Auth;

class Contactleads extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'contact_leads';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email','subject','description','created_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public static function getleads($data=[]){

        $response = Self::select('contact_leads.*');
        $response = $response->orderBy('contact_leads.id','desc');
        $response = $response->get();

        /*print_r($response);
        exit;*/
        return $response;
    }
}
