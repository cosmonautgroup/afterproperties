<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;

class Parking extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'parking';
    protected $fillable = [
       'id', 'socity_id','floor','parking_no', 'is_active','is_delete','creatde_at','updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    public static function getParking($data=[]){
        $request = Self::select('parking.*','socity.society_name')
                ->leftjoin('socity','socity.id','parking.socity_id')
                ->orderBy('id','DESC')
                ->get();
        return $request;
    }
}
