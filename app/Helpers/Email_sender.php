<?php

namespace App\Helpers;

use Config;
use Mail;

class Email_sender
{

    public static function sendEmail($view = null, $settings = null)
    {
        if (!empty($settings) && $view != null) {
            $sent = Mail::send($view, $settings, function ($message) use ($settings) {
                $message->from($settings['from'], $settings['sender']);
                $message->to($settings['to'], $settings['receiver'])->subject($settings['subject']);
            });
        }
    }


    public static function sendAccountVerification($data = null)
    {
        if ($data != null) 
        {
            $settings                 = [];
            $settings['data']         = $data;
            $settings["subject"]      = "AfterProperties - Account Verification";
            $settings['emailType']    = 'AfterProperties - Account Verification';
            $settings['from']         = 'no-reply@thelistareyouonit.com';
            $settings['to']           = $data['receiver_email'];
            $settings['sender']       = 'AfterProperties';
            $settings['receiver']     = $data['user_name'];
            $settings['txtBody']      = view('emails.verification', $settings)->render();
            unset($settings['txtBody']);
            Self::sendEmail('emails.verification', $settings);
        }
    }
    

    public static function sendForgotPasswordEmail($data = null)
    {
        if ($data != null) {
            $settings                 = [];
            $settings['data']         = $data;
            $settings["subject"]      = "AfterProperties - Forgot Password";
            $settings['emailType']    = 'AfterProperties - Forgot Password';
            $settings['from']         = 'no-reply@thelistareyouonit.com';
            $settings['to']           = $data['receiver_email'];
            $settings['sender']       = 'AfterProperties';
            $settings['receiver']     = $data['user_name'];
            $settings['txtBody']      = view('emails.forgot_password', $settings)->render();
            unset($settings['txtBody']);
            Self::sendEmail('emails.forgot_password', $settings);
        }
    }
}
