<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfNotSocietyBlockAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard ='web')
    {
        $userObj = Auth::guard('web')->user();
        if(!empty($userObj))
        {
            if(!$userObj->hasRole('Admin'))
            {
                if(!$userObj->hasRole('Sub-Admin'))
                {
                    if (!$userObj->hasRole('Society-Admin')){
                         if (!$userObj->hasRole('Society-Block-Admin')){
                            return redirect('/admin/dashboard');
                        }

                    }
                }
               
            }  
        }
        return $next($request);
    }
}
