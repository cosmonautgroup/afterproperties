<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\Logs;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    public function login(){ 
        if(!session()->has('url.intended'))
        {
            session(['url.intended' => url()->previous()]);
        }
        return view('auth.login');
    }

    public function index(Request $request){

        $rules = [
                'email'     => 'required|email',
                'password'  => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {

            $formData = $request->all();
            $user = User::where(['email'=>$formData['email']])->first();

            if($user){
                if(isset($user->is_verified) && $user->is_verified == 0){
                    $request->session()->flash('error',"Please Verify Your Email Address");
                    return redirect()->route('showlogin');
                }elseif (isset($user->is_active) && $user->is_active == 0) {
                    $request->session()->flash('error',"Your Account is Inactive");
                    return redirect()->route('showlogin');
                }else{
                    $credentials = [
                        'email'     => $formData['email'],
                        'password'  => $formData['password']
                    ];
                    $authSuccess = Auth::attempt($credentials);
                    if ($authSuccess) {

                        Logs::addLog(1,'Logged in');
                       // $request->session()->flash('success',"Welcome back ".ucfirst($user->first_name));
                        $id = Auth::user()->id;
                        $url = session('url.intended');

                        if (isset($url) && $url!='') {
                            session(['url.intended' => '']);
                            return redirect($url); 
                        }else{
                            return redirect('admin/dashboard'); 
                        }   
                    }else{
                        $request->session()->flash('error',"Invalid Login Detail.");
                        return redirect()->route('showlogin');
                    }
                }
            }else{
                $request->session()->flash('error',"Account Not Found");
                return redirect()->route('showlogin');
            }
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }

    public function logout()
    {
        Auth::logout();
        session()->flush();
        session()->regenerate();
        return redirect('/');
    }
}
