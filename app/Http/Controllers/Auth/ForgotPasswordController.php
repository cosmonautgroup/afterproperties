<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;
use App\User;
use App\Helpers\Email_sender;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function forgot(Request $request)
    {
        $data = Input::get();
        $rules = [
                'email' => 'required|string|email',
            ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            $user = User::where(['email'=>$data['email']])->first();
            if($user){
                if(isset($user->is_verified) && $user->is_verified == 0){

                    $request->session()->flash('error',"Please Verify Your Email Address");
                    return redirect('password/reset');
                }elseif(isset($user->is_active) && $user->is_active == 0){

                    $request->session()->flash('error',"Please Verify Your Email Address");
                    return redirect('password/reset');
                }else{
                    $remember_token = str_random(50);
                    User::where('id',$user->id)->update(['remember_token'=>$remember_token]);
                    $token_key = $remember_token;
                    //end of user email code
                    Email_sender::sendForgotPasswordEmail(['user_name'=>$user->name,'receiver_email'=>$data['email'],'token_key'=>$token_key]);
                    session()->flash('success',"Please check your mail to reset your password.");
                    return redirect('password/reset');
                }
            }else{
                $request->session()->flash('error',"Email Not Found");
                return redirect('password/reset');
            }    
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    public function reset($key){
        $users=User::select('*')->where('remember_token','=',$key)->first();
        if($users!=null)
        {   
            $id= isset($users->id)?$users->id:0;
            return view('auth.passwords.reset',compact('id'));
        }else{
            session()->flash('error','Token has Expired');
        }
        return redirect()->route('showlogin');
    } 
}

