<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\User;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function resetPassword(Request $request)
    {
        $data = Input::get();
        $rules = [
                'old_password' => 'required|string',
                'password' => 'required|min:6|confirmed'
            ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            $user = User::where(['id'=>$data['user_id']])->first();
            if($user){
                if (Hash::check($data['old_password'], $user->password)) { 
                    User::where('id',$data['user_id'])->update(['password'=>Hash::make($data['password']),'remember_token'=>'']);
                    session()->flash('success',"Password Reset successfuly, please login with your new password.");
                    return redirect()->route('showlogin');
                }else{
                    $request->session()->flash('error',"Old password mismatch.");
                    return redirect()->back();
                }  
            }else{
                $request->session()->flash('error',"User Not Found");
                return redirect('password/reset');
            }      
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }
}
