<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Helpers\Email_sender;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(Request $request)
    {
        $data = Input::get();
        $rules = [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|min:6|confirmed',
            ];
        $validator = Validator::make($data, $rules);
        if ($validator->passes()) {
            
            $user = User::create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'name' => $data['first_name'].' '.$data['last_name'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);
            $user->assignRole('Author');

            $remember_token = str_random(50);
            User::where('id',$user->id)->update(['remember_token'=>$remember_token]);
            $token_key = $remember_token;
            //end of user email code
            Email_sender::sendAccountVerification(['user_name'=>$data['first_name'].' '.$data['last_name'],'receiver_email'=>$data['email'],'token_key'=>$token_key]);
            session()->flash('success',"You have registered successfuly, please verify your Account.");
            return redirect()->route('showlogin');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    public function accountActivate($key){
        $users=User::select('*')->where('remember_token','=',$key)->first();
        if($users!=null)
        {
            User::where('id','=',$users->id)->update(['remember_token'=>'','is_verified'=>1,'is_active'=>1]);
            session()->flash('success','Account verified successfuly');
        }else{
            session()->flash('error','Token has Expired');
        }
        return redirect()->route('showlogin');
    } 
}
