<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;
use DB;
use App\Reviews;
use App\Country;
use App\State;
use App\City;
use App\Crop;
use App\Product;
use App\Post_Media;
use App\Posts;
use App\Mettings;
use App\Metting_Media;
use App\User_Meeting;
use File;
use App\Feedback;


class ReviewController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */

    public function add_review(Request $request)
    {

        // echo "<pre>";
        // print_r($request->all());

        // print_r($_FILES);
        // exit;

        $rules = [
            'person_name' => 'required',
            'district_name' => 'required',
            'village_name' => 'required',
            'taluka_name' => 'required',
            'crop_name' => 'required',
            'wiga' => 'required',
            'image' => 'required',
            'product_name' => 'required',
            'rating' => 'required',
            'review' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }


        $user_profile = '';

        if ($file = $request->hasFile('image')) {
            $base_url = url('/');
            $file = $request->file('image');
            $customimagename = time() . '_' . $file->getClientOriginalName();
            $destinationPath = public_path() . '/assets/images/userimage/';
            $upload = $file->move($destinationPath, $customimagename);
            $user_profile = $base_url . '/public/assets/images/userimage/' . $customimagename;
//                if (isset($user->image) && $user->image!='' && file_exists(public_path().'/assets/images/userimage/'.$user->image)) {
//                    unlink(public_path().'/assets/images/userimage/'.$user->image);
//                }
        }


        $data = [
            'person_name' => $request->person_name,
            'district_name' => $request->district_name,
            'village_name' => $request->village_name,
            'taluka_name' => $request->taluka_name,
            'crop_name' => $request->crop_name,
            'wiga' => $request->wiga,
            // 'image' =>$request->image,
            'image' => $user_profile,
            'product_name' => $request->product_name,
            'rating' => $request->rating,
            'file' => $request->file,
            'review' => $request->review,
            'user_id' => $request->user_id
        ];

        DB::table('review')->insert($data);

        return response()->json([
            'success' => 'Success ! Review submitted.'
        ], 201);
    }

    /**get_per_post_single_image
     * Get the authenticated User
     *
     * @return [json] user object
     */


    public function get_review($user_id)
    {

        $rows = DB::table('review')->where(['user_id' => $user_id, 'is_delete' => 0])->get();


        if (count($rows) > 0) {
            return response()->json([
                'reviews' => $rows
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any review found.'
            ], 201);
        }
    }


    public function get_per_review($review_id)
    {


        $rows = DB::table('review')->where(['id' => $review_id, 'is_delete' => 0])->first();

        // $rows1 = Reviews::where('id',$review_id)->get();

        if (!empty($rows)) {
            return response()->json([
                'reviews_detail' => $rows
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any review found.'
            ], 201);
        }
    }


    public function add_post(Request $request)
    {

        $formData = $request->all();


        $rules = [
            'user_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'district_id' => 'required',
            'crop_id' => 'required',
            'product_id' => 'required',

            'taluka_name' => 'required',
            'village_name' => 'required',
            // 'rating' => 'required',
            'comment' => 'required',
        ];

        if ($formData['media_type'] == 0) {
            $rules['image'] = 'required';
            $rules['image.*'] = 'image|mimes:jpg,jpeg,png,bmp';
        }


        if(empty($formData['media_type']))
        {
            $rules['image'] = 'required';
            $rules['image.*'] = 'image|mimes:jpg,jpeg,png,bmp';
        }

        if ($formData['media_type'] == 1) {
            $rules['url'] = 'required';

        }

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        date_default_timezone_set("Asia/Calcutta");

        $reviewObj = new Posts();
        $reviewObj->user_id = $formData['user_id'];
        $reviewObj->country_id = $formData['country_id'];
        $reviewObj->state_id = $formData['state_id'];
        $reviewObj->district_id = $formData['district_id'];
        $reviewObj->crop_id = $formData['crop_id'];
        $reviewObj->product_id = $formData['product_id'];
        // $reviewObj->person_name = $formData['person_name'];
        $reviewObj->taluka_name = $formData['taluka_name'];
        $reviewObj->village_name = $formData['village_name'];
        // $reviewObj->rating = $formData['rating'];
        $reviewObj->comment = $formData['comment'];
        $reviewObj->date = date("Y/m/d");
        $reviewObj->time = date("h:i");


        if ($reviewObj->save()) {

            $post_last_id = $reviewObj->id;


            if ($formData['media_type'] == 0) {
                $images = $request->file('image');

// echo "<pre>";
// print_r($images);
// exit;

                $post_image_media = array();
                if ($file = $request->hasFile('image')) {
                    foreach ($images as $item) {
                        $base_url = url('/');
                        // $customimagename = time() . '_' . $item->getClientOriginalName();
                        $customimagename = $item->getClientOriginalName();
                        $destinationPath = public_path() . '/assets/images/post_image/';
                        $upload = $item->move($destinationPath, $customimagename);
                        $post_image = $base_url . '/public/assets/images/post_image/' . $customimagename;
                        $post_media = new Post_Media();
                        $post_media->post_id = $post_last_id;
                        $post_media->media_type = $formData['media_type'];
                        $post_media->url = $post_image;
                        $post_media->save();
                        $post_image_media [] = $post_media;
                    }
                }
            } else {
                $post_media = new Post_Media();
                $post_media->post_id = $post_last_id;
                $post_media->media_type = $formData['media_type'];
                $post_media->url = $formData['url'];
                $post_media->save();
                $post_image_media = $post_media;
            }
            return response()->json([
                'post' => $reviewObj,
                'post_media' => $post_image_media,
                'success' => 'Success ! Post submitted.'
            ], 201);

        } else {
            return response()->json([
                'message' => 'Error ! Post not submitted.'
            ], 201);
        }


    }


    public function get_per_post($post_id)
    {
        // $ReviewObj = Posts::with([
        //         'get_post_media' => function ($query) {
        //             $query->where('is_delete', 0);
        //         }
        //     ]
        // )->where('user_id', $post_id)->where('is_delete', 0)->first();


        $post_id = Posts::where('user_id', $post_id)->where('is_delete', 0)->pluck('id')->toArray();


        $ReviewObj = Posts::with([
                'get_post_media' => function ($query) {
                    $query->where('is_delete', 0);
                }
                // 'user_info' => function ($query) {
                //     $query->where('is_delete', 0);
                // }
            ]
        )->orderBy('id', 'desc')->whereIn('id', $post_id)->where('is_delete', 0)->get();

        $post = json_decode($ReviewObj);


        foreach ($post as $key => $value) {
            $user_name = User::where(['id' => $value->user_id])->first();
            $post[$key]->person_name = $user_name->name;
        }

        if (!empty($post)) {
            return response()->json([
                'get_post' => $post
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any post found.'
            ], 201);
        }
    }


    public function get_per_post_single_image($post_id)
    {


        // $post_id = Posts::where('user_id', $post_id)->where('is_delete', 0)->pluck('id')->toArray();


        //   $fetch_state_id = Posts::where('user_id', $post_id)->where('is_delete', 0)->pluck('state_id')->toArray();
        // $check_state_get_user = Posts::whereIn('state_id', $fetch_state_id)->where('user_id', '!=', $post_id)->where('approve_post', 1)->where('is_delete', 0)->pluck('id')->toArray();

        $fetch_state_id = User::where('id', $post_id)->where('is_active', 1)->where('is_delete', 0)->pluck('district_id')->toArray();
        $check_state_get_user = Posts::whereIn('district_id', $fetch_state_id)->where('user_id', '!=', $post_id)->where('is_active', 1)->where('approve_post', 1)->where('is_delete', 0)->pluck('id')->toArray();

        $post_id = Posts::where('user_id', $post_id)->where('is_active', 1)->where('is_delete', 0)->pluck('id')->toArray();

        if (isset($check_state_get_user) && !empty($check_state_get_user)) {
            for ($i = 0; $i < count($check_state_get_user); $i++) {
                array_push($post_id, $check_state_get_user[$i]);
            }
        }

        // echo "<pre>";
        // print_r($post_id);
        // exit;


        $ReviewObj = Posts::with([
                'get_post_media1' => function ($query) {
                    $query->where('is_delete', 0);
                },
//                'user_info' => function ($query) {
//                    $query->where('is_delete', 0);
//                }
            ]
        )->orderBy('id', 'desc')->whereIn('id', $post_id)->where('is_active', 1)->where('is_delete', 0)->get();

        $post = json_decode($ReviewObj);

        // echo "<pre>";
        // print_r($post);
        // exit;


        foreach ($post as $key => $value) {

            $row = User::with('user_role1')->where('id', $value->user_id)->first();

            if (!empty($row->image) && File::exists(public_path('assets/images/userimage/' . $row->image))) {
                $image = url('/public/assets/images/userimage/' . $row->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }
            $post[$key]->user_image = $image;
            $post[$key]->user_name = $row->name;


            if (!empty($value->get_post_media1) && count($value->get_post_media1) > 0) {

                foreach ($value->get_post_media1 as $value1) {
                    $url_array = explode('/', $value1->url);
                    $last_element = end($url_array);
                    if (!empty($last_element) && File::exists(public_path('assets/images/post_image/' . $last_element))) {
                        $image = url('/public/assets/images/post_image/' . $last_element);
                    } else {
                        $image = url('/public/images/no_img.jpg');
                    }
                    $value1->url = $image;

                }
            }


            $user_name = User::where(['id' => $value->user_id])->first();
            $crop_name = Crop::where(['id' => $value->crop_id])->first();
            $product_name = Product::where(['id' => $value->product_id])->first();
            $country_name = Country::where(['id' => $value->country_id])->first();
            $state_name = State::where(['id' => $value->state_id])->first();
            $district_name = City::where(['id' => $value->district_id])->first();

            $post[$key]->person_name = $user_name->name;
            $post[$key]->crop_name =
                isset($crop_name->crop_name) ? $crop_name->crop_name : '';
            $post[$key]->product_name =
                isset($product_name->product_name) ? $product_name->product_name : '';
            $post[$key]->country_name =
                isset($country_name->country_name) ? $country_name->country_name : '';
            $post[$key]->state_name =
                isset($state_name->state_name) ? $state_name->state_name : '';
            $post[$key]->district_name =
                isset($district_name->district_name) ? $district_name->district_name : '';


            if (!empty($value->get_post_media1[0])) {
                $post[$key]->media_type = $value->get_post_media1[0]->media_type;
                $post[$key]->url = $value->get_post_media1[0]->url;
            }
        }

        if (!empty($post)) {
            return response()->json([
                'get_post123' => $post
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any post found.'
            ], 201);
        }
    }



    //   public function get_per_meeting_single_image()
    // {
    //     $ReviewObj = Mettings::with([
    //             'old_get_post_media1' => function ($query) {
    //                 $query->where('is_delete', 0);
    //             }
    //         ]
    //     )->orderBy('id', 'desc')->where('is_delete', 0)->where('is_active', 1)->get();

    //     $post = json_decode($ReviewObj);


    //     foreach ($post as $key => $value) {


    //          $crop_name = Crop::where(['id' => $value->crop_id])->first();
    //         $product_name = Product::where(['id' => $value->product_id])->first();
    //         $country_name = Country::where(['id' => $value->country_id])->first();
    //         $state_name = State::where(['id' => $value->state_id])->first();
    //         $district_name = City::where(['id' => $value->district_id])->first();


    //       $post[$key]->crop_name =
    //         isset($crop_name->crop_name) ? $crop_name->crop_name :'';
    //         $post[$key]->product_name =
    //         isset($product_name->product_name) ? $product_name->product_name :'';
    //         $post[$key]->country_name =
    //         isset( $country_name->country_name) ?  $country_name->country_name :'';
    //         $post[$key]->state_name =
    //         isset( $state_name->state_name) ?  $state_name->state_name :'';
    //         $post[$key]->district_name =
    //         isset( $district_name->district_name) ?  $district_name->district_name :'';

    //         if (!empty($value->old_get_post_media1[0])) {
    //             $post[$key]->media_type = $value->old_get_post_media1[0]->media_type;
    //             $post[$key]->url = $value->old_get_post_media1[0]->url;
    //         }
    //     }

    //     if (!empty($post)) {
    //         return response()->json([
    //             'get_metting' => $post
    //         ], 201);
    //     } else {
    //         return response()->json([
    //             'message' => 'No any post found.'
    //         ], 201);
    //     }
    // }


    public function get_per_meeting_single_image($id)
    {
        $ReviewObj = Mettings::with([
                'old_get_post_media1' => function ($query) {
                    $query->where('is_delete', 0);
                },
                'old_get_post_media1' => function ($query) {
                    $query->where('is_delete', 0);
                },
                'user_join_meeting' => function ($query) {
                    $query->where('is_delete', 0);
                },
                'user_join_meeting.get_user' => function ($query) {
                    $query->where('is_delete', 0);
                },
            ]
        )->orderBy('id', 'desc')->where('is_active', 1)->where('is_delete', 0)->where('is_active', 1)->get();

        $post = json_decode($ReviewObj);


        $row = User::with('user_role1')->where(['id' => 1])->first();

        foreach ($post as $key => $value) {


            if (!empty($row->image) && File::exists(public_path('assets/images/userimage/' . $row->image))) {
                $image = url('/public/assets/images/userimage/' . $row->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }
            $post[$key]->admin_image = $image;
            $post[$key]->admin_name = $row->name;


            $check_user_meeting = User_Meeting::where(['user_id' => $id, 'metting_id' => $value->id, 'is_delete' => 0])->first();

            if (!empty($check_user_meeting)) {
                $post[$key]->status = 1;
            } else {
                $post[$key]->status = 0;
            }

            $post[$key]->user_join_meeting_count = count($value->user_join_meeting);
            $crop_name = Crop::where(['id' => $value->crop_id])->first();
            $product_name = Product::where(['id' => $value->product_id])->first();
            $country_name = Country::where(['id' => $value->country_id])->first();
            $state_name = State::where(['id' => $value->state_id])->first();
            $district_name = City::where(['id' => $value->district_id])->first();


            $post[$key]->crop_name =
                isset($crop_name->crop_name) ? $crop_name->crop_name : '';
            $post[$key]->product_name =
                isset($product_name->product_name) ? $product_name->product_name : '';
            $post[$key]->country_name =
                isset($country_name->country_name) ? $country_name->country_name : '';
            $post[$key]->state_name =
                isset($state_name->state_name) ? $state_name->state_name : '';
            $post[$key]->district_name =
                isset($district_name->district_name) ? $district_name->district_name : '';

            if (!empty($value->old_get_post_media1[0])) {
                $post[$key]->media_type = $value->old_get_post_media1[0]->media_type;
                $post[$key]->url = $value->old_get_post_media1[0]->url;
            }
        }

        if (!empty($post)) {
            return response()->json([
                'get_metting' => $post
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any post found.'
            ], 201);
        }
    }


    public function user_meeting(Request $request)
    {

        $formData = $request->all();

        $rules = [
            'user_id' => 'required',
            'metting_id' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_user_meeting = User_Meeting::where(['user_id' => $formData['user_id'], 'metting_id' => $formData['metting_id'], 'is_delete' => 0])->first();

        if (!empty($check_user_meeting)) {
            return response()->json([
                'status' => 0,
                'success' => 'Already User join meeting.'
            ], 201);
        }
        $reviewObj = new User_Meeting();
        $reviewObj->user_id = $formData['user_id'];
        $reviewObj->metting_id = $formData['metting_id'];

        if ($reviewObj->save()) {
            return response()->json([
                'user_meeting' => $reviewObj,
                'status' => 1,
                'success' => 'Success ! User Join Meeting.'
            ], 201);

        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Error ! User not Join Meeting.'
            ], 201);
        }
    }

    public function add_feedback(Request $request)
    {

        $formData = $request->all();


        $rules = [
            'user_id' => 'required',
            'subject' => 'required',
            'comment' => 'required',
        ];


        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }


        $feedback = new Feedback();
        $feedback->user_id = $formData['user_id'];
        $feedback->subject = $formData['subject'];
        $feedback->comment = $formData['comment'];


        if ($feedback->save()) {
            return response()->json([
                'feed_back' => $feedback,
                'success' => 'Success ! Feedback submitted.'
            ], 201);

        } else {
            return response()->json([
                'message' => 'Error ! Feedback not submitted.'
            ], 201);
        }


    }


    public function get_metting_feedback(Request $request)
    {

        try {

            $id = $request->user_id;

//        echo $request['crop_id'];exit;

            $country_id = $request->country_id;
            $state_id = $request->state_id;

            $district_id = $request->district_id;
            $crop_id = $request->crop_id;

            $ReviewObj = Mettings::with(
                [
                    'old_get_post_media1_filter' => function ($query) {
                        $query->where('is_delete', 0);
                    },
                    'user_join_meeting_filter' => function ($query) {
                        $query->where('is_delete', 0);
                    },
                    'user_join_meeting_filter.get_user_filter' => function ($query) {
                        $query->where('is_delete', 0);
                    },
                ]
            )->orderBy('id', 'desc')->where('is_active', 1)->where('is_delete', 0)->where('is_active', 1);

            // echo "<pre>";
            // print_r($ReviewObj->toArray());
            // exit();


            // $ReviewObj = Mettings::orderBy('id', 'desc')->where('is_delete', 0)->where('is_active', 1)->get();

            $returnResponse = array();
            if (!empty($crop_id)) {
                $ReviewObj = $ReviewObj->where('crop_id', $crop_id);
            }

            if (!empty($country_id)) {
                $ReviewObj = $ReviewObj->where('country_id', $country_id);
            }
            if (!empty($state_id)) {
                $ReviewObj = $ReviewObj->where('state_id', $state_id);
            }
            if (!empty($district_id)) {
                $ReviewObj = $ReviewObj->where('district_id', $district_id);
            }

            $ReviewObj = $ReviewObj->get();


            $post = json_decode($ReviewObj);

            // echo "<pre>";
            // print_r($post);
            // exit();


            $row = User::with('user_role1')->where(['id' => 1])->first();

            foreach ($post as $key => $value) {


                if (!empty($row->image) && File::exists(public_path('assets/images/userimage/' . $row->image))) {
                    $image = url('/public/assets/images/userimage/' . $row->image);
                } else {
                    $image = url('/public/images/no_img.jpg');
                }
                $value->admin_image = $image;
                $value->admin_name = $row->name;


                $check_user_meeting = User_Meeting::where(['user_id' => $id, 'metting_id' => $value->id, 'is_delete' => 0])->first();

                if (!empty($check_user_meeting)) {
                    $value->status = 1;
                } else {
                    $value->status = 0;
                }


                if (!empty($value->old_get_post_media1_filter) && count($value->old_get_post_media1_filter) > 0) {

                    foreach ($value->old_get_post_media1_filter as $value1) {
                        $url_array = explode('/', $value1->url);
                        $last_element = end($url_array);
                        if (!empty($last_element) && File::exists(public_path('assets/images/post_image/' . $last_element))) {
                            $image = url('/public/assets/images/post_image/' . $last_element);
                        } else {
                            $image = url('/public/images/no_img.jpg');
                        }
                        $value1->url = $image;

                    }
                }




                $value->user_join_meeting_count = count($value->user_join_meeting_filter);
                $crop_name = Crop::where(['id' => $value->crop_id])->first();
                $product_name = Product::where(['id' => $value->product_id])->first();
                $country_name = Country::where(['id' => $value->country_id])->first();
                $state_name = State::where(['id' => $value->state_id])->first();
                $district_name = City::where(['id' => $value->district_id])->first();

                $value->crop_name =
                    isset($crop_name->crop_name) ? $crop_name->crop_name : '';
                $value->product_name =
                    isset($product_name->product_name) ? $product_name->product_name : '';
                $value->country_name =
                    isset($country_name->country_name) ? $country_name->country_name : '';
                $value->state_name =
                    isset($state_name->state_name) ? $state_name->state_name : '';
                $value->district_name =
                    isset($district_name->district_name) ? $district_name->district_name : '';

                if (!empty($value->old_get_post_media1_filter[0])) {
                    $value->media_type = $value->old_get_post_media1_filter[0]->media_type;
                    $value->url = $value->old_get_post_media1_filter[0]->url;
                }
            }

            if (!empty($post)) {
                return response()->json([
                    'get_metting_filter' => $post
                ], 201);
            } else {
                return response()->json([
                    'message' => 'No any post found.'
                ], 201);
            }
        } catch (Exception $ex) {
            Bugsnag::notifyException($ex);
        }
    }

}
