<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use App\Event;
use App\Socity;
use App\Member;
use App\Staff;
use Validator;
use Hash;
use Mail;
use DB;
use File;

//use App\Country;
//use App\State;
//use App\City;
//use App\Crop;
//use App\Product;
//use App\Posts;
//use App\Post_Media;

//use App\Banner;


class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function mobile_number(Request $request)
    {
        $rules = [
            'phone' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['phone' => $request->phone])->first();

        $otp = rand(1001, 9999);

        if (!empty($check_exist)) {
            $data = [
                'otp' => $otp,
                'otp_verified' => 0
            ];

            User::where('id', $check_exist->id)->update($data);

            $status = "success";
            $message = 'success your otp is : ' . $otp;

        } else {
            $status = "error";
            $message = 'user is not exiest';

        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ], 201);
    }


    public function verify_otp(Request $request)
    {
        $rules = [
            'phone' => 'required|numeric',
            'otp' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['phone' => $request->phone, 'otp' => $request->otp])->first();

        if (empty($check_exist)) {
            $user = '';
            $status = "error";
            $message = 'your otp is wrong';
        } else {
            $data = [
                'otp' => NULL,
                'otp_verified' => 1
            ];
            User::where('id', $check_exist->id)->update($data);
            $user_row = User::where(['id' => $check_exist->id])->first();

            $user = $user_row;
            $status = "success";
            $message = 'success your otp is verify';
        }

        return response()->json([
            'user' => $user,
            'status' => $status,
            'message' => $message
        ], 201);

    }

    public function get_event()
    {

        $row = Event::where('is_delete', 0)->where('is_active', 1)->get();

        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('images/users/' . $value->image))) {
                $image = url('/public/images/users/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');

            }

            $value->image = $image;
        }

        if (!empty($row)) {
            return response()->json([
                'get_event' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any event found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }


    public function get_event_detail($id)
    {

        $row = Event::where('id', $id)->where('is_delete', 0)->where('is_active', 1)->first();

        if (!empty($row)) {

            if (!empty($row->image) && File::exists(public_path('images/users/' . $row->image))) {
                $image = url('/public/images/users/' . $row->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }
            $row->image = $image;

            if (!empty($row)) {
                return response()->json([
                    'get_event_detail' => $row,
                    'status' => 'success'
                ], 201);
            } else {
                return response()->json([
                    'message' => 'No any event found.',
                    'status' => 'error'
                ], 201);
            }
        } else {
            return response()->json([
                'message' => 'No any event found.',
                'status' => 'error'
            ], 201);

        }

    }

    public function get_contact()
    {

        $row = User::where('is_active', 1)->get();

        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('images/users_thumb/' . $value->image))) {
                $image = url('/public/images/users_thumb/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');

            }

            $value->image = $image;
        }

        if (!empty($row)) {
            return response()->json([
                'get_contact' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any contact found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }

    public function get_society()
    {
        $row = Socity::where('is_delete', 0)->where('is_active', 1)->get();

        if (!empty($row)) {
            return response()->json([
                'get_society' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any society found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }


    public function add_member(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
//            'member_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,bmp',
            'phonenumber' => 'required',
            'occupation' => 'required',
            'dateofbirth' => 'required',
            'hobbies' => 'required',
            'gender' => 'required',
            'bloodgroup' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }


        $member_image = '';

        if ($file = $request->hasFile('member_image')) {
            $base_url = url('/');
            $file = $request->file('member_image');
            $customimagename = time() . '_' . $file->getClientOriginalName();
            $destinationPath = public_path() . '/images/member/';
            $upload = $file->move($destinationPath, $customimagename);
            $member_image = $customimagename;
        }


        $reviewObj = new Member();
        $reviewObj->user_id = $request->user_id;
        $reviewObj->firstname = $request->firstname;
        $reviewObj->lastname = $request->lastname;
        $reviewObj->member_image = $member_image;
        $reviewObj->phonenumber = $request->phonenumber;
        $reviewObj->occupation = $request->occupation;
        $reviewObj->dateofbirth = date('Y-m-d', strtotime($request->dateofbirth));
        $reviewObj->hobbies = $request->hobbies;
        $reviewObj->gender = $request->gender;
        $reviewObj->bloodgroup = $request->bloodgroup;

        if ($reviewObj->save()) {
            return response()->json([
                'member' => $reviewObj,
                'status' => true,
                'message' => 'member add successfully'
            ], 201);

        } else {
            return response()->json([
                'member' => $reviewObj,
                'status' => false,
                'message' => 'member not added'
            ], 201);
        }
    }

    public function get_member_detail($user_id)
    {

        $row = User::where('id', $user_id)->first();

        if (!empty($row)) {
            $get_member = Member::where('user_id', $user_id)->where('is_delete', 0)->where('is_active', 1)->get();
            if (!empty($get_member)) {

                $get_member = json_decode($get_member);

                foreach ($get_member as $key => $value) {
                    if (!empty($value->member_image) && File::exists(public_path('images/member/' . $value->member_image))) {
                        $image = url('/public/images/member/' . $value->member_image);
                    } else {
                        $image = url('/public/images/no_img.jpg');

                    }

                    $value->member_image = $image;
                }

                return response()->json([
                    'get_member_detail' => $get_member,
                    'status' => 'success'
                ], 201);

            } else {
                return response()->json([
                    'message' => 'No any member found.',
                    'status' => 'error'
                ], 201);
            }


        } else {
            return response()->json([
                'message' => 'No any user found.',
                'status' => 'error'
            ], 201);

        }

    }


    public function remove_member($member_id)
    {

        $userObj = Member::findOrFail($member_id);
        $userObj->is_delete = 1;
        $userObj->save();

        if ($userObj->save()) {
            return response()->json([
                'status' => 'success',
                'message' => 'member delete successfully'
            ], 201);

        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'member not deleted'
            ], 201);
        }

    }


    public function add_staff(Request $request)
    {
        $rules = [
            'user_id' => 'required',
            'firstname' => 'required',
            'lastname' => 'required',
//            'staff_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,bmp',
            'phonenumber' => 'required',
            'work' => 'required',
            'age' => 'required',
            'gender' => 'required',
            'bloodgroup' => 'required',
//            'proof_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,bmp',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $staff_image = '';
        if ($file = $request->hasFile('staff_image')) {
            $base_url = url('/');
            $file = $request->file('staff_image');
            $customimagename = time() . '_' . $file->getClientOriginalName();
            $destinationPath = public_path() . '/images/staff/';
            $upload = $file->move($destinationPath, $customimagename);
            $staff_image = $customimagename;
        }

        $proff_image = '';
        if ($file = $request->hasFile('proof_image')) {
            $base_url = url('/');
            $file = $request->file('proof_image');
            $customimagename = time() . '_' . $file->getClientOriginalName();
            $destinationPath = public_path() . '/images/proff/';
            $upload = $file->move($destinationPath, $customimagename);
            $proff_image = $customimagename;
        }


        $reviewObj = new Staff();
        $reviewObj->user_id = $request->user_id;
        $reviewObj->firstname = $request->firstname;
        $reviewObj->lastname = $request->lastname;
        $reviewObj->staff_image = $staff_image;
        $reviewObj->phonenumber = $request->phonenumber;
        $reviewObj->work = $request->work;
        $reviewObj->age = $request->age;
        $reviewObj->gender = $request->gender;
        $reviewObj->bloodgroup = $request->bloodgroup;
        $reviewObj->proof_image = $proff_image;

        if ($reviewObj->save()) {
            return response()->json([
                'member' => $reviewObj,
                'status' => 'success',
                'message' => 'staff add successfully'
            ], 201);

        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'staff not added'
            ], 201);
        }
    }

    public function get_staff_detail($user_id)
    {
        $row = User::where('id', $user_id)->first();
        if (!empty($row)) {
            $get_staff = Staff::where('user_id', $user_id)->where('is_delete', 0)->where('is_active', 1)->get();
            if (!empty($get_staff)) {

                $get_staff = json_decode($get_staff);

                foreach ($get_staff as $key => $value) {
                    if (!empty($value->staff_image) && File::exists(public_path('images/staff/' . $value->staff_image))) {
                        $image = url('/public/images/staff/' . $value->staff_image);
                    } else {
                        $image = url('/public/images/no_img.jpg');

                    }
                    $value->staff_image = $image;


                    if (!empty($value->proof_image) && File::exists(public_path('images/proff/' . $value->proof_image))) {
                        $image1 = url('/public/images/proff/' . $value->proof_image);
                    } else {
                        $image1 = url('/public/images/no_img.jpg');

                    }
                    $value->proof_image = $image1;
                }

                return response()->json([
                    'get_staff_detail' => $get_staff,
                    'status' => 'success'
                ], 201);

            } else {
                return response()->json([
                    'message' => 'No any staff found.',
                    'status' => 'error'
                ], 201);
            }


        } else {
            return response()->json([
                'message' => 'No any user found.',
                'status' => 'error'
            ], 201);

        }

    }


    public function remove_staff($member_id)
    {

        $userObj = Staff::findOrFail($member_id);
        $userObj->is_delete = 1;
        $userObj->save();

        if ($userObj->save()) {
            return response()->json([
                'status' => 'success',
                'message' => 'staff delete successfully'
            ], 201);

        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'staff not deleted'
            ], 201);
        }

    }


    public function create_profile(Request $request)
    {

        $rules = [
            'username' => 'required',
            'email' => 'required',
            'mobile_number' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['mobile_no' => $request->mobile_number, 'is_delete' => 0])->first();

        if (empty($check_exist)) {
            return response()->json([
                'message' => 'Mobile no does not exist.'
            ], 201);
        }

        $data = [
            'name' => $request->username,
            'email' => $request->email,
            'image' => isset($request->image) ? ($request->image) : '',
            'is_active' => 1,
            'role_id' => $request->role_id,
        ];

        User::where('id', $check_exist->id)->update($data);
        $check_exist->assignRole($request->role_id);

        $user_row = User::where(['id' => $check_exist->id, 'is_delete' => 0])->first();
        return response()->json([
            'success' => 'Success ! Profile created.',
            'user' => $user_row
        ], 201);
    }
    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */

    // public function user($user_id){
    //     $row = User::where(['id'=>$user_id])->first();
    //     return response()->json($row);
    // }


    public function login(Request $request)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }
        if ($validator->passes()) {
            $input = $request->all();
            $check_if_exist = User::where(['email' => $input['email']])->where('is_active', 1)->where('is_delete', 0)->first();
            if ($check_if_exist) {
                if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
                    $user = $check_if_exist;
                    $success['login'] = $user;
                    $success['Status'] = 'success';
                    $success['message'] = "login successfully";
                    return response()->json($success);
                } else {
                    return response()->json(['message' => "That doesn't seem right. Please check your password."], 401);
                }
            } else {
                return response()->json(['message' => 'This email id is not registerd.'], 401);
            }
        }
    }


    public function signup(Request $request)
    {
        $formData = $request->all();

        $rules = [
            'name' => 'required',
            'email' => 'required',
            'mobile_no' => 'required',
            'role_id' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required_with:password|same:password',
            'country_id' => 'required',
            'state_id' => 'required',
            'district_id' => 'required',
            'taluka_name' => 'required',
            'village_name' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        if ($validator->passes()) {
            $input = $request->all();

            $check_unique_email = User::where(['email' => $input['email'], 'is_active' => 1, 'is_delete' => 0])->first();

            if (!empty($check_unique_email)) {
                return response()->json(['message' => 'Sorry! That email id is taken! Please use another email id.'], 401);
            }


            $check_unique_mobile = User::where(['mobile_no' => $input['mobile_no'], 'is_active' => 1, 'is_delete' => 0])->first();

            if (!empty($check_unique_mobile)) {
                return response()->json(['message' => 'Sorry! That mobile number is taken! Please use another mobile number.'], 401);
            }

            $userArr = [];
            $userArr['name'] = $input['name'];
            $userArr['email'] = trim(strip_tags($input['email']));
            $userArr['password'] = Hash::make($input['password']);
            $userArr['mobile_no'] = $input['mobile_no'];
            $userArr['role_id'] = $input['role_id'];
            $userArr['country_id'] = $input['country_id'];
            $userArr['state_id'] = $input['state_id'];
            $userArr['district_id'] = $input['district_id'];
            $userArr['taluka_name'] = $input['taluka_name'];
            $userArr['village_name'] = $input['village_name'];

            $userObj = User::create($userArr);

            if ($userObj) {
                $success['signup'] = $userObj;
                $success['Status'] = 'success';
                $success['message'] = 'Signup sucessfully';
            } else {
                $success['Status'] = 'error';
                $success['message'] = 'Some thing missing';
            }
            return response()->json($success);
        }
    }


    public function password_generate(Request $request)
    {
        $rules = [
            'mobile_number' => 'required',
            'password' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['mobile_no' => $request->mobile_number, 'is_delete' => 0])->first();

        if (!empty($check_exist)) {
            $data = [
                'password' => $request->password,
            ];
            User::where('id', $check_exist->id)->update($data);
            return response()->json([
                'password' => $check_exist,
                'success' => 'Password update sucessfully'
            ], 201);
        } else {
//            $user = new User([
//                'mobile_no' => $request->mobile_number,
//                'password' => $request->password,
//            ]);
//            $user_data = $user->save();

            $user = new User();
            $user->mobile_no = $request->mobile_number;
            $user->password = $request->password;
            $user->save();


            return response()->json([
                'password' => $user,
                'success' => 'Password generate sucessfully'
            ], 201);
        }
    }


    public function edit_profile1(Request $request)
    {
        $formData = $request->all();
        // echo "<pre>";
        // print_r($formData);
        // exit();
        $rules = [
            'name' => 'required',
            'role_id' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }
        if ($validator->passes()) {


            $user = User::find($request->id);
            $user->name = $request->name;
            $user->role_id = $request->role_id;
            $user->save();

            $success['edit_profile'] = $user;
            $success['Status'] = 'success';
            $success['message'] = 'Save Profile successfully';
            return response()->json($success);

        }
    }


    public function edit_profile(Request $request)
    {
        $formData = $request->all();
        // echo "<pre>";
        // print_r($formData);
        // exit();
        $rules = [
            'name' => 'required',
            // 'email' => 'required',
            // 'mobile_no' => 'required',
            'role_id' => 'required',
            'country_id' => 'required',
            'state_id' => 'required',
            'district_id' => 'required',
            'taluka_name' => 'required',
            'village_name' => 'required',
        ];


        // if (!empty($request->id)) {
        //     $rules = [
        //         'email' => 'required|unique:users,email,' . $request->id . ',id,is_delete,0',
        //         'mobile_no' => 'required|unique:users,mobile_no,' . $request->id . ',id,is_delete,0',
        //     ];
        // }

        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }
        if ($validator->passes()) {

            $user = User::find($request->id);
            $user->name = $request->name;
            // $user->email = $request->email;
            // $user->mobile_no = $request->mobile_no;
            $user->role_id = $request->role_id;
            $user->country_id = $request->country_id;
            $user->state_id = $request->state_id;
            $user->district_id = $request->district_id;
            $user->taluka_name = $request->taluka_name;
            $user->village_name = $request->village_name;


            if ($file = $request->hasFile('image')) {
                $base_url = url('/');
                $file = $request->file('image');
                $customimagename = time() . '_' . $file->getClientOriginalName();
                $destinationPath = public_path() . '/assets/images/userimage/';
                $upload = $file->move($destinationPath, $customimagename);
                $user->image = $customimagename;
            }

            $user->save();


            if (!empty($user->image) && File::exists(public_path('assets/images/userimage/' . $user->image))) {
                $image = url('/public/assets/images/userimage/' . $user->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }

            $user->image = $image;


            $success['edit_profile'] = $user;
            $success['Status'] = 'success';
            $success['message'] = 'Save Profile successfully';
            return response()->json($success);

        }
    }


    public function forgot_password(Request $request)
    {


        $rules = [
            'email' => 'required|email',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['email' => $request->email, 'is_delete' => 0])->first();

        $otp = rand(1001, 9999);

        if (!empty($check_exist)) {

            $data = ['to_email' => $check_exist->email];


            Mail::send('app_forgot_password', ['data' => $check_exist, 'otp' => $otp], function ($message) use ($data) {
                $message->to($data['to_email'], 'Skyloan')->subject
                ('Otp get Successfully');
                $message->from('test1.cosmonautgroup@gmail.com', 'Skyloan Send Otp Verification');
            });


            $data = [
                'otp' => $otp,
                'otp_verified' => 0
            ];

//            User::where('id', $check_exist->id)->update($data);

            $user = User::where('id', $check_exist->id)->first();
            $user->otp = $otp;
            $user->otp_verified = 0;
            $user->save();


            return response()->json([
                'for_pwd_email' => $user,
                'success' => 'Please check your email id  And Verify a Otp number'
            ], 201);

        } else {
            return response()->json([
                'success' => 'Please enter right email id',
                'message' => 'Please enter right email id'
            ], 201);
        }
    }


    public function password_update(Request $request)
    {
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
            'password_confirmation' => 'required_with:password|same:password'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json(['message' => $validator->errors()], 401);
        }

        $check_exist = User::where(['email' => $request->email, 'is_delete' => 0])->first();

//        echo "<pre>";
//        print_r($check_exist);
//        exit;


        if (!empty($check_exist)) {


            $user = User::where('id', $check_exist->id)->first();
            $user->password = Hash::make($request->password);
            $user->save();


            return response()->json([
                'for_pwd_update' => $user,
                'success' => 'Your Password is successfully update'
            ], 201);

        } else {
            return response()->json([
                'success' => 'Please enter right email id',
                'message' => 'Please enter right email id'
            ], 201);
        }
    }

    public function get_role()
    {
        $rows = DB::table('roles')->where('status', 1)->where('is_delete', 0)->where('id', '!=', '1')->get();


        if (count($rows) > 0) {
            return response()->json([
                'get_role' => $rows
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any role found.'
            ], 201);
        }
    }


    public function user_exiest($user_id)
    {

        $row = User::where('id', $user_id)->where('is_active', 1)->where('is_delete', 0)->first();

        if (!empty($row)) {
            return response()->json([
                'code' => 1,
                'status' => 'success',
                'message' => 'user is exist'
            ], 201);
        } else {
            return response()->json([
                'code' => 0,
                'status' => 'error',
                'message' => 'user is not exist'
            ], 201);
        }

        return response()->json($row);
    }

    public function user($user_id)
    {

        $row = User::with('user_role1')->where(['id' => $user_id])->first();

        if (!empty($row)) {

            $roll = DB::table('roles')->where('is_delete', 0)->where('id', $row->role_id)->first();


            $country_name = Country::where(['id' => $row->country_id])->first();
            $state_name = State::where(['id' => $row->state_id])->first();
            $district_name = City::where(['id' => $row->district_id])->first();


            $row->roll_name = !empty($roll->name) ? $roll->name : '';


            $row->country_name = isset($country_name->country_name) ? $country_name->country_name : '';
            $row->state_name = isset($state_name->state_name) ? $state_name->state_name : '';
            $row->district_name = isset($district_name->district_name) ? $district_name->district_name : '';


            if (!empty($row->image) && File::exists(public_path('assets/images/userimage/' . $row->image))) {
                $image = url('/public/assets/images/userimage/' . $row->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }
            $row->image = $image;

            if (!empty($row)) {
                return response()->json([
                    'get_user' => $row,
                    'status' => 'success'
                ], 201);
            } else {
                return response()->json([
                    'message' => 'No any user found.',
                    'status' => 'error'
                ], 201);
            }
        } else {
            return response()->json([
                'message' => 'No any user found.',
                'status' => 'error'
            ], 201);

        }

        return response()->json($row);
    }

    public function get_user()
    {

        $row = User::with('user_role1')->where('role_id', '!=', 1)->where('is_active', 1)->where('is_delete', 0)->get();


        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('assets/images/userimage/' . $value->image))) {
                $image = url('/public/assets/images/userimage/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');
            }
            $row[$key]->image = $image;
        }


        if (!empty($row)) {
            return response()->json([
                'get_user' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any user found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }


    public function get_crop()
    {

        $row = Crop::where('is_delete', 0)->where('is_active', 1)->get();

        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('assets/images/crop/' . $value->image))) {
                $image = url('/public/assets/images/crop/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');

            }

            $row[$key]->image = $image;
        }

        if (!empty($row)) {
            return response()->json([
                'get_crop' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any crop found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }

    public function get_product()
    {

        $row = Product::where('is_delete', 0)->where('is_active', 1)->get();

        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('assets/images/product/' . $value->image))) {
                $image = url('/public/assets/images/product/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');

            }

            $row[$key]->image = $image;
        }


        if (!empty($row)) {
            return response()->json([
                'get_product' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any product found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }

    public function get_country()
    {

        $row = Country::where('is_delete', 0)->where('is_active', 1)->get();

        if (!empty($row)) {
            return response()->json([
                'get_country' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any country found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }

    public function get_state($country_id)
    {

        $row = State::where('country_id', $country_id)->where('is_delete', 0)->where('is_active', 1)->get();

        if (!empty($row)) {
            return response()->json([
                'get_state' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any state found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }

    public function get_city($state_id)
    {

        $row = City::where('state_id', $state_id)->where('is_delete', 0)->where('is_active', 1)->get();

        if (!empty($row)) {
            return response()->json([
                'get_city' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any city found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }


    public
    function get_banner()
    {

        $row = Banner::where('is_delete', 0)->where('is_active', 1)->get();

        $row = json_decode($row);

        foreach ($row as $key => $value) {
            if (!empty($value->image) && File::exists(public_path('assets/images/banner/' . $value->image))) {
                $image = url('/public/assets/images/banner/' . $value->image);
            } else {
                $image = url('/public/images/no_img.jpg');

            }

            $row[$key]->image = $image;
        }


        if (!empty($row)) {
            return response()->json([
                'get_banner' => $row,
                'status' => 'success'
            ], 201);
        } else {
            return response()->json([
                'message' => 'No any Banner found.',
                'status' => 'error'
            ], 201);
        }

        return response()->json($row);
    }


}
