<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Socity;
use App\Subscription;

class SocityController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin.socity.index');
    }
    public function getData(Request $request){
        try {
            $user = DB::table('users')->select('first_name','last_name','id')->get();
            $socity = Socity::orderBy('id','desc')->get();
            return Datatables::of($socity)
            ->addColumn('action', function ($socity) {
                return "<a href='".route('edit-socity',$socity->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-socity',$socity->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('socity_admin',function($socity){
                $socity_admin = $socity->name;
                return $socity_admin;
            })
            ->rawColumns(['action','status','socity_admin'])
            ->make(true);
        } catch (Exception $ex) {
            Bugsnag::notifyException($ex);
        }
    }

    public function add(Request $request){
            $user = User::pluck('first_name','last_name','id');
            $subscription = Subscription::select('plan_name','id')->get();
            return view('admin.socity.add',compact('user','subscription'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'name'              => 'required',
                'email'             => 'required|email',
                'contact_no'        => 'required',
                'address_address'   => 'required',
                'subscription_id'   => 'required',
            ];
            if(!isset($formData['id'])){
                $rules = [
                    'email'      => 'unique:users'
                ];
            }

            $validator = Validator::make($request->all(), $rules);
                if(isset($formData['id']) && !empty($formData['id'])){
                    $userObj = Socity::find($formData['id']);
                }else{
                    $userObj = new Socity();
                }
                $userObj->society_name    = $formData['name'];
                $userObj->email           = $formData['email'];
                $userObj->contact_no      = $formData['contact_no'];
                $userObj->address         = $formData['address_address'];
                $userObj->subscription_id = $formData['subscription_name'];
                $userObj->is_active       = isset($formData['is_active'])?1:0;
                if($userObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Socity Updated Successfully');
                    }else{
                        $request->session()->flash('success','Socity Added Successfully');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('socity');
    }

    public function edit($id,Request $request){
            $socObj = Socity::findOrFail($id);
            $subscription = Subscription::select('plan_name','id')->get();
            return view('admin.socity.edit',compact('socObj','subscription'));
    }

    public function delete($id)
    {
        $socity = Socity::where('id',$id)->first();

        if ($socity==null) {
            session()->flash('error', 'Socity not found.');
            return redirect()->route('socity');
        }
        Socity::where('id',$id)->delete();
        $socity->roles()->detach();
        session()->flash('success',"Socity successfully deleted.");
        return redirect()->route('socity');
    }

}
