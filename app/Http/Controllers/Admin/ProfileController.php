<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Auth;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where('id',Auth::user()->id)->first();
        if ($user==null) {
            session()->flash('error', 'User not found.');
            return redirect()->route('dashboard');
        }
        return view('admin/profile/index',compact('user'));
    }
   

    /**
     * update users.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $rules = [
            'first_name' => 'required', 
            'last_name' => 'required', 
            'email'     => 'required|email|unique:users,email,'.$request->user_id, 
            'image' => 'image|mimes:jpeg,png,jpg,gif,zip',
        ];

        if (isset($request->change_password) && $request->change_password==1) {
            $rules['old_password'] = 'required';
            $rules['password'] = 'required|confirmed|min:6';
        }

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
                $user = User::where('id',$request->user_id)->first();
                
                $data = [
                    'first_name'     => $request->first_name,
                    'last_name'     => $request->last_name,
                    'email'         => $request->email
                ];
                if (isset($request->change_password) && $request->change_password==1) {
                    if (Hash::check($request->old_password,$user->password)) {
                        if ($request->password==$request->password_confirmation) {
                            $data['password'] = Hash::make($request->password); 
                        }else{
                            session()->flash('error', 'Confirm password missmatch.');
                            return redirect()->route('profile');
                        }
                    } else{
                        $validator->errors()->add('old_password', 'Old password does not match the current password');
                        return redirect()->back()->withErrors($validator)->withInput();
                    }
                }
                if ($file = $request->hasFile('image'))
                {
                    $file = $request->file('image');
                    $customimagename  = time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path().'/images/users/';
                    $upload = $file->move($destinationPath, $customimagename);
                    $data['image'] = $customimagename;
                    if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users/'.$user->image)) {
                        unlink(public_path().'/images/users/'.$user->image);
                    }
                }
                User::where('id',$request->user_id)->update($data);
                $request->session()->flash('success',"Your Profile successfully updated.");
                return redirect()->route('profile');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }
}