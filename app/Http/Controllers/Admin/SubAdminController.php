<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Auth;
use DataTables;
use DB;

class SubAdminController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
public function __construct()
{
    $this->middleware('auth');
}

/**
* Show the application users.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    return view('admin/subadmin/index');
}

/**
* Show the application users.
*
* @return \Illuminate\Http\Response
*/
public function getData(Request $request)
{
    $users = User::getUsers(array('role'=>2));
    return Datatables::of($users)
    ->addColumn('created_at', function ($users) {
        return date('d M Y h:i A', strtotime($users->created_at));
    })
    ->addColumn('image', function ($users) {
        if (isset($users->image) && $users->image!='' && file_exists(public_path().'/images/users/'.$users->image)) {
            $image = url('/public').'/images/users_thumb/'.$users->image;
        }else{
            $image = url('/public').'/images/users/no-image.png';
        }
        return '<img src="'.$image.'" height="50" width="50">';
    })
    ->addColumn('is_vat_tax', function ($users) {
        return (isset($users->is_vat_tax) && intval($users->is_vat_tax) == 1)?'Applied':'Not Applied';
    })
    ->addColumn('is_active', function ($users) {
        if($users->is_active == 1 )
        {
            return "<a href='".route('subadmin-status',$users->id)."'><span class='m-badge  m-badge--success m-badge--wide'>Active</span></a>";
        }else{
            return "<a href='".route('subadmin-status',$users->id)."'><span class='m-badge  m-badge--danger m-badge--wide'>Inactive</span></a>";
        }
    })
    ->addColumn('action', function ($users) {
        return "<a href='".route('edit-subadmin',$users->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-subadmin',$users->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

    })
    ->rawColumns(['image','is_active','action'])
    ->setTotalRecords($users->count())
    ->make(true);
}

/**
* add users.
*
* @return \Illuminate\Http\Response
*/
public function add()
{
    return view('admin/subadmin/add');
}

/**
* create users.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $request)
{
    $rules = [
        'first_name' => 'required',
        'last_name' => 'required',
        'image' => 'image|mimes:jpeg,png,jpg,gif,zip'
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->passes()) {

        $data = [
            'first_name'          => $request->first_name,
            'last_name'          => $request->last_name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password),
            'is_verified'   => isset($request->is_verified)?1:0,
            'is_active'   => isset($request->is_active)?1:0,
            'created_by'  => Auth::user()->id,
        ];

        if ($file = $request->hasFile('image')){

            $file = $request->file('image');
            $customimagename  = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path().'/images/users/';
            $upload = $file->move($destinationPath, $customimagename);
            $data['image'] = $customimagename;
            resizeSmallThumb($data['image']);
        }

        $user = User::create($data);

        if($user){
            $user->assignRole('Sub-Admin');
        }

        $remember_token = str_random(50);
        User::where('id',$user->id)->update(['remember_token'=>$remember_token]);
        //Subadmin Email
        $token_key = $remember_token;
        $request->session()->flash('success',"Subadmin successfully added.");
        return redirect()->route('subadmin');
    }else {
        session()->flash('error', 'Something went wrong.');
        return redirect()->back()->withErrors($validator)->withInput();
    }
}

/**
* edit users.
*
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
    $user = User::where('id',$id)->first();
    if ($user==null) {
        session()->flash('error', 'Subadmin not found.');
        return redirect()->route('subadmin');
    }
    return view('admin/subadmin/edit',compact('user'));
}

/**
* update users.
*
* @return \Illuminate\Http\Response
*/
public function update(Request $request)
{

    $rules = [
        'first_name' => 'required', 
        'last_name' => 'required', 
        'image' => 'image|mimes:jpeg,png,jpg,gif,zip'
    ];

    if (isset($request->change_password) && $request->change_password==1) {
        $rules['old_password'] = 'required';
        $rules['password'] = 'required|confirmed|min:6';
    }

    $validator = Validator::make($request->all(), $rules);
    if ($validator->passes()) {

        $user = User::where('id',$request->user_id)->first();

        $data = [
            'first_name'     => $request->first_name,
            'last_name'     => $request->last_name,
            'email'         => $request->email,
            'is_verified'   => isset($request->is_verified)?1:0,
            'is_active'   => isset($request->is_active)?1:0,
        ];
        if (isset($request->change_password) && $request->change_password==1) {
            if (Hash::check($request->old_password,$user->password)) {
                if ($request->password==$request->password_confirmation) {
                    $data['password'] = Hash::make($request->password); 
                }else{
                    session()->flash('error', 'Confirm password missmatch.');
                    return redirect()->route('profile');
                }
            } else{
                $validator->errors()->add('old_password', 'Old password does not match the current password');
                return redirect()->back()->withErrors($validator)->withInput();
            }
        }
        if ($file = $request->hasFile('image'))
        {
            $file = $request->file('image');
            $customimagename  = time() . '.' . $file->getClientOriginalExtension();
            $destinationPath = public_path().'/images/users/';
            $upload = $file->move($destinationPath, $customimagename);
            $data['image'] = $customimagename;
            if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users/'.$user->image)) {
                unlink(public_path().'/images/users/'.$user->image);
            }
            if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users_thumb/'.$user->image)) {
                unlink(public_path().'/images/users_thumb/'.$user->image);
            }
            resizeSmallThumb($data['image']);
        }
        User::where('id',$request->user_id)->update($data);
        $request->session()->flash('success',"Subadmin successfully updated.");
        return redirect()->route('subadmin');
    }else {
        session()->flash('error', 'Something went wrong.');
        return redirect()->back()->withErrors($validator)->withInput();
    }   
}

/**
* edit status.
*
* @return \Illuminate\Http\Response
*/
public function status($id)
{
    $user = User::where('id',$id)->first();
    if ($user==null) {
        session()->flash('error', 'Subadmin not found.');
        return redirect()->route('subadmin');
    }
    if (isset($user->is_active) && $user->is_active==0) {
        User::where('id',$id)->update(['is_active'=>1]);
        session()->flash('success',"Subadmin successfully Activated.");
    }else{
        User::where('id',$id)->update(['is_active'=>0]);
        session()->flash('success',"Subadmin successfully Deactivated.");
    }
    return redirect()->route('subadmin');
}


/**
* delete providers.
*
* @return \Illuminate\Http\Response
*/
public function delete($id)
{
    $user = User::where('id',$id)->first();

    if ($user==null) {
        session()->flash('error', 'Subadmin not found.');
        return redirect()->route('subadmin');
    }
    if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users/'.$user->image)) {
        unlink(public_path().'/images/users/'.$user->image);
    }
    if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users_thumb/'.$user->image)) {
        unlink(public_path().'/images/users_thumb/'.$user->image);
    }
    User::where('id',$id)->delete();
    $user->roles()->detach();
    session()->flash('success',"Subadmin successfully deleted.");
    return redirect()->route('subadmin');
}
}
