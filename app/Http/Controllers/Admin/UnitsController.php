<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Blocks;
use App\Unit;
use App\Units;
use App\Parking;

class UnitsController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/units/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $units = Unit::getUnit();
            $floor = DB::table('floors')->select('floor','id')->get();
            return Datatables::of($units)
            ->addColumn('action', function ($units) {
                return "<a href='".route('edit-units',$units->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-units',$units->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('block_name',function($units){
                $block_name = $units->name;
                return $block_name;
            })
            ->addColumn('floor',function($units){
                $floor = $units->floor;
                return $floor;
            })
            ->addColumn('unit',function($units){
                $unit = $units->unit_name;
                return $unit;
            })

            ->addColumn('user',function($units){
                $user = $units->first_name;
                return $user;
            })


            ->rawColumns(['action','block_name','floor','unit','user'])
            ->make(true);
    }
    public function getFloor(Request $request) {

        $floor = DB::table("floors")->where("block_id",$request->blockid)->pluck("floor","id");
        return response()->json($floor);

    }
    public function getUnit(Request $request) {

        $unit = DB::table("units")->where('block_id',$request->blockid)->where('floor_id',$request->floorid)->pluck("unit_name","id");
        return response()->json($unit);
    }
    public function add(Request $request){
            $blocks  = Blocks::select('name','id')->get();
            $floor   = DB::table('floors')->select('floor','id')->get();
            $unit    = Units::select('unit_name','id')->get();
            $user    = User::select('first_name','id')->get();
            $bhk     = DB::table('bhk')->select('bhk','id')->get();
            $parking = DB::table('parking')->select('parking_no','id')->get();
            return view('admin/units/add',compact('blocks','floor','unit','user','bhk','parking'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'block'        => 'required',
                'floor'        => 'required',
                'unit'         => 'required',
                'squarfeet'    => 'required',
                'bhk'          => 'required',
                'view'         => 'required',
                'parking_no'   => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
                if(isset($formData['id']) && !empty($formData['id'])){
                    $unitObj = Unit::find($formData['id']);
                }else{
                    $unitObj = new Unit();
                }
                $unitObj->block_id        = ucwords($formData['block']);
                $unitObj->floor_id        = $formData['floor'];
                $unitObj->unit_id         = $formData['unit'];
                $unitObj->squarfeet       = $formData['squarfit'];
                $unitObj->bhk             = $formData['bhk'];
                $unitObj->view            = ucwords($formData['view']);
                $unitObj->parking_no      = $formData['parking_no'];
                $unitObj->assign_user     = $formData['user'];
                if($unitObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Units Updated Success');
                    }else{
                        $request->session()->flash('success','Units Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('units');
    }

    public function edit($id,Request $request){
            $unitsObj = Unit::findOrFail($id);
            $blocks = Blocks::select('name','id')->get();
            $floor  = DB::table('floors')->select('floor','id')->get();
            $unit   = DB::table('units')->select('unit_name','id')->get();
            $user   = User::select('first_name','id')->get();
            $bhk = DB::table('bhk')->select('bhk','id')->get();
            $parking = Parking::select('parking_no','id')->get();
            return view('admin/units/edit',compact('unitsObj','blocks','floor','unit','user','bhk','parking'));
    }

    public function delete($id)
    {
        $units = Unit::where('id',$id)->first();

        if ($units==null) {
            session()->flash('error', 'Units not found.');
            return redirect()->route('units');
        }
        Unit::where('id',$id)->delete();
        $units->roles()->detach();
        session()->flash('success',"Units successfully deleted.");
        return redirect()->route('units');
    }

}
