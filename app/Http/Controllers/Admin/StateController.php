<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\State;
use App\Country;

class StateController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/state/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $state = State::getState();
            return Datatables::of($state)
            ->addColumn('action', function ($state) {
                return "<a href='".route('edit-state',$state->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-state',$state->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('country_name',function($state){
                $country_name = $state->country_name;
                return $country_name;
            })
            ->rawColumns(['action','country_name'])
            ->make(true);
    }
    public function add(Request $request){
            $country = Country::select('country_name','id')->get();
            return view('admin/state/add',compact('country'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'country_name' => 'required',
                'state_name'   => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $stateObj = State::find($formData['id']);
                }else{
                    $stateObj = new State();
                }
                $stateObj->country_id    = $formData['country_name'];
                $stateObj->state_name    = ucwords($formData['state_name']);
                if($stateObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','State Updated Success');
                    }else{
                        $request->session()->flash('success','State Created Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('state');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $stateObj = State::findOrFail($id);
            $country = Country::select('country_name','id')->get();
            return view('admin/state/edit',compact('stateObj','country'));
    }

    public function delete($id)
    {
        $state = State::where('id',$id)->first();

        if ($state==null) {
            session()->flash('error', 'state not found.');
            return redirect()->route('state');
        }
        State::where('id',$id)->delete();
        $state->roles()->detach();
        session()->flash('success',"State successfully deleted.");
        return redirect()->route('state');
    }

}
