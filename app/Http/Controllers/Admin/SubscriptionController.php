<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Subscription;

class SubscriptionController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/subscription/index');
    }
    /*Open User List view*/
    public function getData(Request $request){
            $sub = Subscription::orderBy('id','desc')->get();
            return Datatables::of($sub)
            ->addColumn('action', function ($sub) {
                return "<a href='".route('edit-subscription',$sub->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-subscription',$sub->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->rawColumns(['action'])
            ->make(true);
    }
    /*Load data in Ajax Datatable*/

    public function add(Request $request){
            return view('admin/subscription/add');
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'plan_name'  => 'required',
                'validity'   => 'required',
                'price'      => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {

                if(isset($formData['id']) && !empty($formData['id'])){
                    $subObj = Subscription::find($formData['id']);
                }else{
                    $subObj = new Subscription();
                }

                $subObj->plan_name            = $formData['plan_name'];
                $subObj->validity             = $formData['validity'];
                $subObj->price                = $formData['price'];
                if($subObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Subscription Updated Success');
                    }else{
                        $request->session()->flash('success','Subscription Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect('admin/subscription');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $subObj = Subscription::findOrFail($id);
            return view('admin/subscription/edit',compact('subObj'));
    }

    public function delete($id)
    {
        $sub = Subscription::where('id',$id)->first();

        if ($sub==null) {
            session()->flash('error', 'Subscription not found.');
            return redirect()->route('subscription');
        }
        Subscription::where('id',$id)->delete();
        $sub->roles()->detach();
        session()->flash('success',"Subscription successfully deleted.");
        return redirect()->route('subscription');
    }

}
