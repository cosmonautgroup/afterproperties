<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Units;
use App\Blocks;
use App\Unit;
use App\Parking;
use App\Socity;
use App\Maintenance;

class MaintenanceController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/maintenance/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $main = Maintenance::getMaintenance();
            return Datatables::of($main)
            ->addColumn('action', function ($main) {
                return "<a href='".route('edit-maintenance',$main->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-maintenance',$main->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('socity_name',function($main){
                $socity_name = $main->society_name;
                 return $socity_name;
            })
            ->addColumn('block_name',function($main){
                $block_name = $main->blockname;
               /* echo "<pre>";
                print_r($block_name);
                exit();*/
                return $block_name;
            })
            ->rawColumns(['action','socity_name','block_name'])
            ->make(true);
    }
    public function getBlock(Request $request) {

        $block = DB::table("blocks")->where("socity_id",$request->socityid)->pluck("name","id");
        return response()->json($block);

    }
    public function add(Request $request){
            $socity  = Socity::select('society_name','id')->get();
            $blocks  = Blocks::select('name','id')->get();
            return view('admin.maintenance.add',compact('blocks','socity'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'socity'        => 'required',
                'block'         => 'required',
                'ammount'       => 'required',
                'yearly'        => 'required',
                'unit_type'     => 'required',
                'payment_cycle' => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $mainObj = Maintenance::find($formData['id']);
                }else{
                    $mainObj = new Maintenance();
                }
                $mainObj->socity_id            = $formData['socity'];
                $mainObj->block_id             = $formData['block'];
                $mainObj->ammount              = $formData['ammount'];
                $mainObj->unit_resident_type   = $formData['unit_type'];
                $mainObj->payment_cycle        = $formData['payment_cycle'];
                if($mainObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Maintenance Updated Success');
                    }else{
                        $request->session()->flash('success','Maintenance Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('maintenance');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
        try {
            $mainObj = Maintenance::findOrFail($id);
            $blocks = Blocks::select('name','id')->get();
            $socity = Socity::select('society_name','id')->get();
            return view('admin.maintenance.edit',compact('mainObj','blocks','socity'));
        } catch (Exception $ex) {
            Bugsnag::notifyException($ex);
        }
    }

    public function delete($id)
    {
        $maintenance = Maintenance::where('id',$id)->first();

        if ($maintenance==null) {
            session()->flash('error', 'Maintenance not found.');
            return redirect()->route('maintenance');
        }
        Maintenance::where('id',$id)->delete();
        $maintenance->roles()->detach();
        session()->flash('success',"Maintenance successfully deleted.");
        return redirect()->route('maintenance');
    }

}
