<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\State;
use App\Country;
use App\City;

class CityController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/city/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $city     = City::getCity();
            return Datatables::of($city)
            ->addColumn('action', function ($city) {
                return "<a href='".route('edit-city',$city->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-city',$city->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('country_name',function($city){
                $country_name = $city->country_name;
                return $country_name;
            })
            ->addColumn('state_name',function($city){
                $state_name = $city->state_name;
                return $state_name;
            })
            ->rawColumns(['action','country_name','state_name'])
            ->make(true);
    }
    public function getState(Request $request) {

        $states = DB::table("state")->where("country_id",$request->countryid)->pluck("state_name","id");
        return response()->json($states);

    }
    public function add(Request $request){
            $country = Country::select('country_name','id')->get();
            $state = State::select('state_name','id')->get();
            return view('admin/city/add',compact('country','state'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'country_name' => 'required',
                'city_name'    => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $cityObj = City::find($formData['id']);
                }else{
                    $cityObj = new City();
                }
                $cityObj->country_id    = $formData['country_name'];
                $cityObj->state_id      = $formData['state_name'];
                $cityObj->city_name     = ucwords($formData['city_name']);
                if($cityObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','City Updated Success');
                    }else{
                        $request->session()->flash('success','City Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('city');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $cityObj = City::findOrFail($id);
            $country = Country::select('country_name','id')->get();
            $state   = State::select('state_name','id')->get();
            return view('admin/city/edit',compact('cityObj','country','state'));
    }

    public function delete($id)
    {
        $city = City::where('id',$id)->first();

        if ($city==null) {
            session()->flash('error', 'City not found.');
            return redirect()->route('city');
        }
        City::where('id',$id)->delete();
        $city->roles()->detach();
        session()->flash('success',"City successfully deleted.");
        return redirect()->route('city');
    }

}
