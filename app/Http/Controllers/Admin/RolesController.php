<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Validator;
use Auth;
use DB;
use DataTables;


class RolesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::get();
        return view('admin/roles/index',compact('roles'));
    }

     /**
     * Show the application houses.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        $roles = Role::get();
        return Datatables::of($roles)
            ->addColumn('created_at', function ($roles) {
                return date('d M Y h:i A', strtotime($roles->created_at));
            })
            ->addColumn('action', function ($roles) {
                if (isset($roles->id) && $roles->id !=1) { 
                    return "<a href='".route('menu-permission',$roles->id)."' class='btn btn-sm btn-success'><i class='fa fa-lock'></i> Permission</a>";
                } else{
                    return "";
                }
            })
            ->rawColumns(['action'])
            ->setTotalRecords($roles->count())
            ->make(true);
    }

    /**
     * add roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('admin/roles/add');
    }

    /**
     * create roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'name'  => 'required|unique:roles,name', 
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {    
            
            $data = ['name'  => $request->name ];
           
            $role = Role::create($data);
            $request->session()->flash('success',"Role successfully added.");
            return redirect()->route('roles');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }

    /**
     * edit roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::findById($id);
        if ($role==null) {
            session()->flash('error', 'Role not found.');
            return redirect()->route('roles');
        }
        return view('admin/roles/edit',compact('role'));
    }

    /**
     * update roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name'     => 'required|unique:roles,name,'.$request->role_id, 
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $data = ['name'  => $request->name ];
            Role::where('id',$request->role_id)->update($data);
            $request->session()->flash('success',"Role successfully updated.");
            return redirect()->route('roles');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }


    /**
     * delete roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $role = Role::where('id',$id)->first();
        if ($role==null) {
            session()->flash('error', 'Role not found.');
            return redirect()->route('roles');
        }
        Role::where('id',$id)->delete();
        session()->flash('success',"Role successfully deleted.");
        return redirect()->route('roles');
    }

    /**
     * edit roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function permission($id)
    {
        $role = Role::findById($id);
        if ($role==null) {
            session()->flash('error', 'Role not found.');
            return redirect()->route('roles');
        }
        $permissions = Permission::where('is_parent',0)->orderBy('order_by','asc')->get();
        return view('admin/roles/permission',compact('role','permissions'));
    }

    /**
     * update roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function createPermission(Request $request)
    {  
        $permissions = $request->permission;
        if (!empty($permissions)) {
            $role = Role::find($request->role_id);
            DB::table('role_has_permissions')->where(['role_id'=>$request->role_id])->delete();
            foreach ($permissions as $key => $permission) {
                $permission_id = isset($permission)?$permission:0;
                if (intval($permission_id) > 0) {
                    if (isset($permission_id) && intval($permission_id) > 0) {
                        $permissionData = Permission::where('id',$permission_id)->first();
                        if (isset($permissionData->is_parent) && intval($permissionData->is_parent) == 0) {
                            $childPermissionData = Permission::where('is_parent',$permission_id)->get();
                            if (!empty($childPermissionData)) {
                                foreach ($childPermissionData as $childPermission) {
                                    $roleHasPer = DB::table('role_has_permissions')->where(['permission_id'=>$childPermission->id,'role_id'=>$request->role_id])->first();
                                    if ($roleHasPer==null) {
                                        DB::table('role_has_permissions')->insert(['permission_id'=>$childPermission->id,'role_id'=>$request->role_id]);
                                        $permission = Permission::find($childPermission->id);
                                        $role->givePermissionTo($permission->name);
                                    }
                                }
                            }
                        }else{
                            $roleHasPer = DB::table('role_has_permissions')->where(['permission_id'=>$permissionData->is_parent,'role_id'=>$request->role_id])->first();
                            if ($roleHasPer==null) {
                                DB::table('role_has_permissions')->insert(['permission_id'=>$permissionData->is_parent,'role_id'=>$request->role_id]);
                                $permission = Permission::find($permissionData->is_parent);
                                $role->givePermissionTo($permission->name);
                            }    
                        }
                        $roleHasPer = DB::table('role_has_permissions')->where(['permission_id'=>$permission_id,'role_id'=>$request->role_id])->first();
                        if ($roleHasPer==null) {
                            DB::table('role_has_permissions')->insert(['permission_id'=>$permission_id,'role_id'=>$request->role_id]);
                            $permission = Permission::find($permission_id);
                            $role->givePermissionTo($permission->name);
                        }    
                    }
                }
            }
            $request->session()->flash('success',"Permission successfully updated.");
        }else{
            $request->session()->flash('error',"No Permission Selected.");
        }
        return redirect()->route('menu-permission',$request->role_id);
    }


}
