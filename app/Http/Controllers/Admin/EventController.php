<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Event;

class EventController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/event/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $event = Event::getEvent();
            return Datatables::of($event)
            ->addColumn('action', function ($event) {
                return "<a href='".route('edit-event',$event->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-event',$event->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('image', function ($event) {

                if (isset($event->image) && $event->image!='' && file_exists(public_path().'/images/users/'.$event->image)) {
                    $image = url('/public').'/images/users/'.$event->image;
                }else{

                    $image = url('/public').'/images/no-image.png';
                }
                return '<img src="'.$image.'" height="50" width="50">';
            })
            ->rawColumns(['action','image'])
            ->make(true);
    }
    public function add(Request $request){
            return view('admin/event/add');
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'name'      => 'required',
                'detail'    => 'required',
                'venue_name'=> 'required',
                'address'   => 'required',
                'date'      => 'required',
                'time'      => 'required',
                'end_date'  => 'required',
                'end_time'  => 'required',
                'rules'     => 'required',
                'radio'     => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $eventObj = Event::find($formData['id']);
                }else{
                    $eventObj = new Event();
                }
                $eventObj->event_name   = ucwords($formData['name']);
                $eventObj->event_detail = ucwords($formData['detail']);
                $eventObj->venue_name   = ucwords($formData['venue_name']);
                $eventObj->address      = ucwords($formData['address']);
                $eventObj->cost_ticket  = $formData['radio'];
                $eventObj->date         = date('y-m-d',strtotime($formData['date']));
                $eventObj->time         = date('h:m:s',strtotime($formData['time']));
                $eventObj->end_date     = date('y-m-d',strtotime($formData['end_date']));
                $eventObj->end_time     = date('h:m:s',strtotime($formData['end_time']));
                $eventObj->event_rule   = ucwords($formData['rules']);
                $eventObj->cost         = $formData['cost'];
                $eventObj->contact_name = ucwords($formData['contact_name']);
                $eventObj->phone_no     = $formData['phone_no'];
                $eventObj->email        = $formData['email'];
                $eventObj->website_url  = $formData['website_url'];
                if ($file = $request->hasFile('image')){
                    $file = $request->file('image');
                    $customimagename  = time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path().'/images/users/';
                    $upload = $file->move($destinationPath, $customimagename);
                    $eventObj->image = $customimagename;
                }
                if($eventObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Event Updated Success');
                    }else{
                        $request->session()->flash('success','Event Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('event');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $eventObj = Event::findOrFail($id);
            return view('admin/event/edit',compact('eventObj'));
    }

    public function delete($id)
    {
        $event = Event::where('id',$id)->first();

        if ($event==null) {
            session()->flash('error', 'event not found.');
            return redirect()->route('event');
        }
        Event::where('id',$id)->delete();
        $event->roles()->detach();
        session()->flash('success',"event successfully deleted.");
        return redirect()->route('event');
    }

}
