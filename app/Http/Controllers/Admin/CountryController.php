<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Country;

class CountryController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/country/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $country = Country::getCountry();
            return Datatables::of($country)
            ->addColumn('action', function ($country) {
                return "<a href='".route('edit-country',$country->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-country',$country->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->rawColumns(['action'])
            ->make(true);
    }
    public function add(Request $request){
            return view('admin/country/add');
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'country_name'   => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $countryObj = Country::find($formData['id']);
                }else{
                    $countryObj = new Country();
                }
                $countryObj->country_name    = ucwords($formData['country_name']);
                if($countryObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Country Updated Success');
                    }else{
                        $request->session()->flash('success','Country Added Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('country');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $countryObj = Country::findOrFail($id);
            return view('admin/country/edit',compact('countryObj'));
    }

    public function delete($id)
    {
        $country = Country::where('id',$id)->first();

        if ($country==null) {
            session()->flash('error', 'Country not found.');
            return redirect()->route('country');
        }
        Country::where('id',$id)->delete();
        $country->roles()->detach();
        session()->flash('success',"Country successfully deleted.");
        return redirect()->route('country');
    }

}
