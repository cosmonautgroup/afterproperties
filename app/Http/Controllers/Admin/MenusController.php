<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Validator;
use Auth;
use DB;
use DataTables;


class MenusController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/menus/index');
    }

     /**
     * Show the application houses.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        $menus = Permission::get();
        return Datatables::of($menus)
            ->addColumn('created_at', function ($menus) {
                return date('d M Y h:i A', strtotime($menus->created_at));
            })
            ->addColumn('parent', function ($menus) {
                if (isset($menus->is_parent) && intval($menus->is_parent) > 0) {
                  $parent = Permission::where('id',$menus->is_parent)->first();
                  return isset($parent->name)?$parent->name:'' ;
                }else{
                    return "";
                }
            })
            ->addColumn('action', function ($menus) {
                return "<a href='".route('edit-menu',$menus->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-menu',$menus->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i> </a>";
            })
            ->rawColumns(['action'])
            ->setTotalRecords($menus->count())
            ->make(true);
    }
    /**
     * add menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        $menus = Permission::where('is_parent',0)->get();
        return view('admin/menus/add',compact('menus'));
    }

    /**
     * create menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'name'  => 'required|unique:permissions,name', 
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {    
            
            $menus = Permission::where('is_parent',0)->count();
            $order_by = intval(isset($menus)?$menus+1:0);

            $data = ['name'  => $request->name,'is_parent'  => intval(isset($request->is_parent)?$request->is_parent:0),'font_icon'=>isset($request->font_icon)?$request->font_icon:'fa fa-list' ,'url'=>isset($request->url)?$request->url:'admin/dashboard','order_by'=>$order_by];
           
            $menu = Permission::create($data);
            $roles = Auth::user()->getRoleNames(); // Returns a collection
            if (isset($roles[0]) && $roles[0]!='') {
                $role = Role::findByName($roles[0]);
                $role->givePermissionTo($request->name);
            }
            $request->session()->flash('success',"Menu successfully added.");
            return redirect()->route('menus');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }

    /**
     * edit menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = Permission::where('id',$id)->first();
        if ($menu==null) {
            session()->flash('error', 'Menu not found.');
            return redirect()->route('menus');
        }
        $menus = Permission::where('is_parent',0)->get();
        return view('admin/menus/edit',compact('menu','menus'));
    }

    /**
     * update menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name'     => 'required|unique:permissions,name,'.$request->menu_id, 
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            $data = ['name'  => $request->name,'is_parent'  => intval(isset($request->is_parent)?$request->is_parent:0),'font_icon'=>isset($request->font_icon)?$request->font_icon:'fa fa-list','url'=>isset($request->url)?$request->url:'admin/dashboard' ];
            Permission::where('id',$request->menu_id)->update($data);
            $request->session()->flash('success',"Menu successfully updated.");
            return redirect()->route('menus');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }   
    }


    /**
     * delete menus.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $menu = Permission::where('id',$id)->first();
        if ($menu==null) {
            session()->flash('error', 'Menu not found.');
            return redirect()->route('menus');
        }
        $associatedMenu = Permission::where('is_parent',$id)->first();
        if ($associatedMenu!=null) {
            session()->flash('error', 'This Menu is associated with another Menu.');
            return redirect()->route('menus');
        }
        Permission::where('id',$id)->delete();
        DB::table('role_has_permissions')->where(['permission_id'=>$id])->delete();
        session()->flash('success',"Menu successfuly deleted.");
        return redirect()->route('menus');
    }
}
