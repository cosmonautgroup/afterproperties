<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Units;
use App\Blocks;
use App\Unit;
use App\Socity;
use App\Parking;

class ParkingController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/parking/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $parking = Parking::getParking();
            return Datatables::of($parking)
            ->addColumn('action', function ($parking) {
                return "<a href='".route('edit-parking',$parking->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-parking',$parking->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('socity',function($parking){
                $socity = $parking->society_name;
                return $socity;
            })

            ->rawColumns(['action','socity'])
            ->make(true);
    }
     public function getFloor(Request $request) {

        $floor = DB::table("floors")->where("block_id",$request->blockid)->pluck("floor","id");
        return response()->json($floor);

    }
    public function getUnit(Request $request) {

        $unit = DB::table("units")->where('block_id',$request->blockid)->where('floor_id',$request->floorid)->pluck("unit_name","id");
        /*echo "<pre>";
        print_r($request->blockid);
        print_r($request->floorid);
        exit();*/
        return response()->json($unit);
    }
    public function add(Request $request){
            $blocks = Blocks::select('name','id')->get();
            $floor  = DB::table('floors')->select('floor','id')->get();
            $unit   = Units::select('unit_name','id')->get();
            $socity   = Socity::select('society_name','id')->get();
            return view('admin/parking/add',compact('blocks','floor','unit','socity'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'socity'        => 'required',
                'parking_floor' => 'required',
                'parking_no'    => 'required',
                'block'         => 'required',
                'floor'         => 'required',
                'unit'          => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
                if(isset($formData['id']) && !empty($formData['id'])){
                    $parkingObj = Parking::find($formData['id']);
                }else{
                    $parkingObj = new Parking();
                }
                $parkingObj->socity_id       = $formData['socity'];
                $parkingObj->floor           = $formData['parking_floor'];
                $parkingObj->parking_no      = $formData['parking_no'];
                $parkingObj->block_id        = $formData['block'];
                $parkingObj->floor_id        = $formData['floor'];
                $parkingObj->unit_id         = $formData['unit'];
                    if($parkingObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Parking Updated Success');
                    }else{
                        $request->session()->flash('success','Parking Addes Success');
                    }
                }else{
                    $request->session()->flash('error','Something Went Wrong');
                }
                return redirect()->route('parking');
    }

    public function edit($id,Request $request){
            $parkingObj = Parking::findOrFail($id);
            $blocks = Blocks::select('name','id')->get();
            $floor  = DB::table('floors')->select('floor','id')->get();
            $unit   = DB::table('units')->select('unit_name','id')->get();
            $socity = Socity::select('society_name','id')->get();
            return view('admin/parking/edit',compact('parkingObj','blocks','floor','unit','socity'));
    }

    public function delete($id)
    {
        $parking = Parking::where('id',$id)->first();

        if ($parking==null) {
            session()->flash('error', 'parking not found.');
            return redirect()->route('parking');
        }
        Parking::where('id',$id)->delete();
        $parking->roles()->detach();
        session()->flash('success',"Parking successfully deleted.");
        return redirect()->route('parking');
    }

}
