<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use DataTables;
use App\Posts;
use DB;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/posts/index');
    }

    public function getData(){
        $posts = Posts::getPost();

        return DataTables::of($posts)
        ->addColumn('action',function($posts){
            return "<a href='".route('view-posts',$posts->id)."' class='btn btn-sm btn-info'><i class='la la-eye'></i></a> <a href='".route('edit-posts',$posts->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-posts',$posts->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
        })
        ->rawColumns(['action'])
        ->setTotalRecords($posts->count())
        ->make(true);

    }

    public function add(){
        $category = DB::table('category')->select('category_name','id')->get();
        $user    = DB::table('users')->select('first_name','last_name','id')->get();
        return view('admin.posts.add',compact('category','user'));
    }
}
