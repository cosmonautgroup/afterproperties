<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Auth;
use DataTables;
use DB;
use App\Settings;

class SettingsController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
public function __construct()
{
    $this->middleware('auth');
}

/**
* Show the application users.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
    $settings = Settings::getSettings();
    return view('admin/settings/index',compact('settings'));
}

/**
* Show the application users.
*
* @return \Illuminate\Http\Response
*/

public function create(Request $request)
{
    $rules = [
        'meta_title'       => 'required',
        'meta_keyword'     => 'required',
        'meta_description' => 'required',
        'site_title'       => 'required',
        'copyright_text'   => 'required',
        'contactus_email'  => 'required',
        'address'          => 'required',
    ];

    $validator = Validator::make($request->all(), $rules);
    if ($validator->passes()) {
        $settings = Settings::getSettings();
        $data = [
            'meta_title'        => $request->meta_title,
            'meta_keyword'      => $request->meta_keyword,
            'meta_description'  => $request->meta_description,
            'site_title'        => $request->site_title,
            'copyright_text'    => $request->copyright_text,
            'contactus_email'   => $request->contactus_email,
            'address'           => $request->address,
            'facebook_link'     => $request->facebook_link,
            'twitter_link'      => $request->twitter_link,
            'instagram_link'    => $request->instagram_link
        ];
        if(!empty($settings)){
            Settings::where('id',1)->update($data);
        }else{
            Settings::create($data);
        }
        $request->session()->flash('success',"Settings successfully updated.");
        return redirect()->route('settings');
    }else {
        session()->flash('error', 'Something went wrong.');
        return redirect()->back()->withErrors($validator)->withInput();
    }
}

}
