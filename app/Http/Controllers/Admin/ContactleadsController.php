<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\Contactleads;
use Validator;
use Auth;
use DB;
use DataTables;

class ContactleadsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application users.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/contactleads/index');
    }
      /**
     * Show the application users.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request){

        $contactleads = Contactleads::getleads();
        return Datatables::of($contactleads)
            ->setTotalRecords($contactleads->count())
            ->make(true);
    }

}
