<?php
namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Blocks;
use App\Socity;
use App\Units;

class BlockController extends Controller
{
/*Open User List view*/
public function index(){
            return view('admin/blocks/index');
    }
/*Open User List view*/
    public function getData(Request $request){
        try {
            $blocks = Blocks::orderBy('id','desc')->get();
            return Datatables::of($blocks)
            ->addColumn('action', function ($block) {
                return "<a href='".route('edit-block',$block->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-block',$block->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->rawColumns(['action'])
            ->make(true);
        } catch (Exception $ex) {
            Bugsnag::notifyException($ex);
        }
    }
/*Load data in Ajax Datatable*/
public function add(Request $request){
        $socity = Socity::select('society_name','id')->get();
        $block  = Blocks::select('name','id')->get();
        return view('admin/blocks/add',compact('socity','block'));
}
public function create(Request $request){
   /* echo "string";
    exit();*/
        $formData = $request->all();

        $rules = [
            'name[]'     => 'required',
            'units'    => 'required',
            'floor'    => 'required',
            'unit_name'=> 'required',
            'socity'   => 'required',
        ];
                $validator = Validator::make($request->all(), $rules);
                for($k=0;$k<count($formData['name']);$k++){

                    if(isset($formData['id']) && !empty($formData['id'])){
                        $blocksObj = Blocks::find($formData['id']);
                    }else{
                        $blocksObj = new Blocks();
                    }
                    $blocksObj->name                  = $formData['name'][$k];
                    $blocksObj->socity_id             = $formData['socity_name'];
                    $blocksObj->floor                 = $formData['floor'][$k];
                    $blocksObj->unit_no               = $formData['units'][$k];
                    $blocksObj->save();

                    for($i=1;$i<=$formData['floor'][$k];$i++){
                    $block = array('floor' =>$i,
                                   'socity_id' => $formData['socity_name'],
                                   'block_id'  => $blocksObj->id,
                             );
                     $blocks = DB::table('floors')->insert($block);

                     for($j=1; $j<=$formData['units'][$k]; $j++){

                      $unitnum =  array ('unit_name'=>$i."0".$j,
                                        'block_id'=>$blocksObj->id,
                                        'socity_id'=>$blocksObj->socity_id,
                                        'floor_id'=>$i,
                                  );
                     $unit = DB::table('units')->insert($unitnum);
                    }
                }

                }
            if(isset($formData['id']) && !empty($formData['id'])){
                $request->session()->flash('success','Blocks Updated Success');
            }else{
                $request->session()->flash('success','Blocks Aded Success');
             }
             return redirect()->route('block');
}

public function edit($id,Request $request){
        $blocksObj = DB::table('blocks')
                   ->select('blocks.*','socity.society_name as socityname')
                   ->leftjoin('socity','socity.id','blocks.id')
                   ->where('blocks.socity_id',$id)
                   ->get();
       $socity = Socity::select('society_name','id')->get();
       $blockObj = Blocks::getBlocksById($request->id);
        return view('admin.blocks.edit',compact('blocksObj','socity','blockObj'));
}
public function update(Request $request){
        $formData = $request->all();
        $rules = [
            'name'     => 'required',
            'units'    => 'required',
            'floor'    => 'required',
            'unit_name'=> 'required',
            'socity'   => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        $block = Blocks::getBlocksById($request->id);
            if($validator->passes()){

            if(isset($formData['name']) && is_array($formData['name']) && count($formData['name']) > 0){
                foreach($formData['name'] as $key => $value){
                   if(isset($formData['id']) && !empty($formData['id'])){
                        $blocksObj = Blocks::getBlocksById($request->id);
                    }
                    if(!empty($value)){
                    $data = [
                        'id'                    => $formData['id'],
                        'name'                  => $formData['name'][$key],
                        'socity_id'             => $formData['socity'],
                        'unit_no'               => $formData['units'][$key],
                        'floor'                 => $formData['floor'][$key],
                    ];
                    Blocks::where('id',$request->id)->update($data);
                   }
                    for($i=1;$i<=$formData['floor'][$key];$i++){
                    $block = array('floor' =>$i,
                                   'socity_id' => $formData['socity'],
                                   'block_id'  => $blocksObj->id,
                             );
                     $blocks = DB::table('floors')->where('block_id',
                        $request->block_id)->update($block);
                }
            }
        }
            $request->session()->flash('success','Blocks Updated Success');
            return redirect()->route('blocks');
            }else{
                return redirect()->back()->withErrors($validator)->withInput();
            }
}
  public function delete($id)
    {
        $block = Blocks::where('id',$id)->first();

        if ($block==null) {
            session()->flash('error', 'Block not found.');
            return redirect()->route('block');
        }
        Blocks::where('id',$id)->delete();
        $block->roles()->detach();
        session()->flash('success',"Block successfully deleted.");
        return redirect()->route('block');
    }

}