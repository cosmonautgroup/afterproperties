<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use App\Cms;
use Validator;
use Auth;
use DataTables;
use App\User;



class CmsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin/cms/index');
    }

     /**
     * Show the application houses.
     *
     * @return \Illuminate\Http\Response
     */
    public function getData(Request $request)
    {
        $cms = Cms::getCmsPages();
        return Datatables::of($cms)
            ->addColumn('created_at', function ($cms) {
                return date('d M Y h:i A', strtotime($cms->created_at));
            })
            ->addColumn('status', function ($cms) {
                $status = status_action(route('cms-status',$cms->id),isset($cms->is_active)?$cms->is_active:0);
                return $status;
            })
            ->addColumn('action', function ($cms) {
                return "<a href='".route('view-cms',$cms->id)."' class='btn btn-sm btn-info'><i class='la la-eye'></i></a> <a href='".route('edit-cms',$cms->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-cms',$cms->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
                return $action;
            })
            ->rawColumns(['is_active','action','status'])
            ->setTotalRecords($cms->count())
            ->make(true);
    }

    /**
     * add zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('admin/cms/add');
    }

    /**
     * create zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $rules = [
            'page_title' => ['required',Rule::unique('cms_page')->where(function ($query) use ($request) {
                return $query->where('is_delete',0);
            })],
            'page_slug'  => 'required',
            'page_content'  => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $data = [
                'page_title'  => $request->page_title,
                'page_slug' => $request->page_slug,
                'page_content' => $request->page_content,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
                'is_active'     => isset($request->is_active)?1:0
            ];
            $cms = Cms::create($data);

            $request->session()->flash('success',"CMS Page successfully added.");
            return redirect()->route('cms-page');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * edit zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cms = Cms::getCmsPagesById($id);
        if ($cms==null) {
            session()->flash('error', 'Page not found.');
            return redirect()->route('cms');
        }
        return view('admin/cms/edit',compact('cms'));
    }

    /**
     * update zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'page_title' => ['required',
                Rule::unique('cms_page')->where(function ($query) use ($request) {
                    return $query->where('is_delete',0);
                })->ignore($request->id)
            ],
            'page_slug'  => 'required',
            'page_content'  => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);

        if ($validator->passes()) {
            $data = [
                'page_title'  => $request->page_title,
                'page_slug' => $request->page_slug,
                'page_content' => $request->page_content,
                'meta_keywords' => $request->meta_keywords,
                'meta_description' => $request->meta_description,
                'is_active'     => isset($request->is_active)?1:0
            ];
            $cms = Cms::where('id',$request->id)->update($data);

            $request->session()->flash('success',"CMS Page successfully updated.");
            return redirect()->route('cms-page');
        }else {
            session()->flash('error', 'Something went wrong.');
            return redirect()->back()->withErrors($validator)->withInput();
        }
    }

    /**
     * edit status.
     *
     * @return \Illuminate\Http\Response
     */
    public function status($id)
    {
        $cms = Cms::getCmsPagesById($id);
        if ($cms==null) {
            session()->flash('error', 'Page not found.');
            return redirect()->route('cms-page');
        }
        if (isset($cms->is_active) && $cms->is_active==0) {
            Cms::where('id',$id)->update(['is_active'=>1]);
            session()->flash('success',"Page successfully Activated.");
        }else{
            Cms::where('id',$id)->update(['is_active'=>0]);
            session()->flash('success',"Page successfully Deactivated.");
        }
        return redirect()->route('cms-page');
    }


    /**
     * delete zones.
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $cms = Cms::getCmsPagesById($id);

        if ($cms==null) {
            session()->flash('error', 'Page not found.');
            return redirect()->route('cms-page');
        }
        Cms::where('id',$id)->update(['is_delete'=>1]);
        session()->flash('success',"Page successfully deleted.");
        return redirect()->route('cms-page');
    }

    public function view($id)
    {
        $cms = Cms::getCmsPagesById(['id'=>$id]);
        if ($cms==null) {
            session()->flash('error', 'Cms not found.');
            return redirect()->route('cms');
        }
        return view('admin/cms/view',compact('cms'));
    }
}
