<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Hash;
use Lang;
use DB;
use Validator;
use App\Amenites;
use App\Socity;

class AmenitesController extends Controller
{
	/*Open User List view*/
    public function index(){
            return view('admin/amenites/index');
    }
    public function getData(Request $request){
            $formData = $request->all();
            $ame = Amenites::getAmenites();
            return Datatables::of($ame)
            ->addColumn('action', function ($ame) {
                return "<a href='".route('edit-amenites',$ame->id)."' class='btn btn-sm btn-primary'><i class='la la-pencil'></i></a> <a href='".route('delete-amenites',$ame->id)."' onclick='return confirm(\"Confirm deletion?\")' class='btn btn-sm btn-danger'><i class='fa fa-trash'></i></a>";

            })
            ->addColumn('image', function ($ame) {

                if (isset($ame->image) && $ame->image!='' && file_exists(public_path().'/images/users/'.$ame->image)) {
                    $image = url('/public').'/images/users/'.$ame->image;
                }else{

                    $image = url('/public').'/images/no-image.png';
                }
                return '<img src="'.$image.'" height="50" width="50">';
            })
            ->addColumn('socity_id',function($ame){
                $socity_id = $ame->society_name;
                    return $socity_id;
            })
            ->rawColumns(['action','image','socity_id'])
            ->make(true);
    }
    public function add(Request $request){
            $socity = Socity::select('society_name','id')->get();
            return view('admin/amenites/add',compact('socity'));
    }

    public function create(Request $request){
            $formData = $request->all();
            $rules = [
                'socity'    => 'required',
                'name'      => 'required',
                'time'      => 'required',
                'end_time'  => 'required',
                'avilable'  => 'required',
                'rules'     => 'required',
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->passes()) {
                if(isset($formData['id']) && !empty($formData['id'])){
                    $ameObj = Amenites::find($formData['id']);
                }else{
                    $ameObj = new Amenites();
                }
                $ameObj->socity_id       = $formData['socity'];
                $ameObj->amenites_name   = ucwords($formData['name']);
                $ameObj->time            = date('h:m:s',strtotime($formData['time']));
                $ameObj->end_time        = date('h:m:s',strtotime($formData['end_time']));
                $ameObj->avilable        = $formData['avilable'];
                $ameObj->amenites_rule   = ucwords($formData['rules']);
                if ($file = $request->hasFile('image')){
                    $file = $request->file('image');
                    $customimagename  = time() . '.' . $file->getClientOriginalExtension();
                    $destinationPath = public_path().'/images/users/';
                    $upload = $file->move($destinationPath, $customimagename);
                    $ameObj->image = $customimagename;
                }
                if($ameObj->save()){
                    if(isset($formData['id']) && !empty($formData['id'])){
                        $request->session()->flash('success','Amenites Updated Success');
                    }else{
                        $request->session()->flash('success','Amenites Added Success');
                    }
                }else{
                    $request->session()->flash('error','Somthing Went Wrong');
                }
                return redirect()->route('amenites');

            }else {
                return redirect()->back()->withErrors($validator)->withInput();
            }
    }

    public function edit($id,Request $request){
            $socity = Socity::select('society_name','id')->get();
            $ameObj = Amenites::findOrFail($id);
            return view('admin/amenites/edit',compact('ameObj','socity'));
    }

    public function delete($id)
    {
        $ame = Amenites::where('id',$id)->first();

        if ($ame==null) {
            session()->flash('error', 'Amenites not found.');
            return redirect()->route('amenites');
        }
        Amenites::where('id',$id)->delete();
        $ame->roles()->detach();
        session()->flash('success',"Amenites successfully deleted.");
        return redirect()->route('amenites');
    }

}
