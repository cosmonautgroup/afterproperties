<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Auth;
use DataTables;
use App\ClientHouse;
use App\ClientHouseService;
use App\Bill;
use DB;
use App\Zone;
use App\Logs;


class LogsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin/logs/index');
    }

    public function getData(Request $request)
    {

        $logs = Logs::getLogs($request->all());

        return Datatables::of($logs)
            ->addColumn('date', function ($logs) {
                return date('d M Y', strtotime($logs->log_date_time));
            })
            ->addColumn('time', function ($logs) {
                return date('h:i A', strtotime($logs->log_date_time));
            })
            ->addColumn('action', function ($logs) {
               $html = $logs->message;
               return $html;
            })
            ->rawColumns(['action'])
            ->setTotalRecords($logs->count())
            ->make(true);
    }
}
