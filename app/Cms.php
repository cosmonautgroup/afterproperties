<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use DB;
use Auth;

class Cms extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $table = 'cms_page';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'page_title', 'page_slug','page_content','meta_keywords','meta_description','is_active','is_delete'
    ];
    public static function getCmsPages($data=[]){
        $response = Self::select('cms_page.*');
        $response = $response->where('is_delete','0');
        $response = $response->get();
        return $response;
    }

    public static function getCmsPagesById($id){
        $response = Self::select('cms_page.*');
        $response = $response->where('id',$id);
        $response = $response->where('is_delete','0');
        $response = $response->first();
        return $response;
    }
}
