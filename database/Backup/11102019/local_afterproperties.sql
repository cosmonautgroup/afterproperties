-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 11, 2019 at 05:27 AM
-- Server version: 5.7.21
-- PHP Version: 7.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `local_afterproperties`
--

-- --------------------------------------------------------

--
-- Table structure for table `amenites`
--

DROP TABLE IF EXISTS `amenites`;
CREATE TABLE IF NOT EXISTS `amenites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socity_id` int(11) DEFAULT NULL,
  `amenites_name` varchar(255) DEFAULT NULL,
  `time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `avilable` varchar(255) DEFAULT NULL,
  `amenites_rule` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amenites`
--

INSERT INTO `amenites` (`id`, `socity_id`, `amenites_name`, `time`, `end_time`, `avilable`, `amenites_rule`, `image`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 1, 'Test Amenites', '11:09:00', '11:09:00', 'All', 'Nothing', '1568007687.png', 1, 0, '2019-09-09 00:11:27', '2019-09-09 00:11:27'),
(3, 1, 'Amenites test', '11:09:30', '11:09:30', 'Saturday-Sunday', 'nothing', '1568007829.png', 1, 0, '2019-09-09 00:13:49', '2019-09-09 00:13:49'),
(4, 1, 'Swimming Pull', '11:09:00', '11:09:00', 'Only Sunday', 'Nothing', '1568008297.jpeg', 1, 0, '2019-09-09 00:20:22', '2019-09-09 00:21:37');

-- --------------------------------------------------------

--
-- Table structure for table `bhk`
--

DROP TABLE IF EXISTS `bhk`;
CREATE TABLE IF NOT EXISTS `bhk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bhk` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `id_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bhk`
--

INSERT INTO `bhk` (`id`, `bhk`, `is_active`, `id_delete`, `created_at`, `updated_at`) VALUES
(1, '1BHK', 1, 0, '2019-09-02 06:31:21', '2019-09-02 06:31:21'),
(2, '2BHK', 1, 0, '2019-09-02 06:31:21', '2019-09-02 06:31:21'),
(3, '3BHK', 1, 0, '2019-09-02 06:31:40', '2019-09-02 06:31:40'),
(4, '4BHK', 1, 0, '2019-09-02 06:31:40', '2019-09-02 06:31:40'),
(5, '5BHK', 1, 0, '2019-09-02 06:31:48', '2019-09-02 06:31:48');

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

DROP TABLE IF EXISTS `blocks`;
CREATE TABLE IF NOT EXISTS `blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `socity_id` int(11) DEFAULT NULL,
  `floor` int(11) DEFAULT NULL,
  `floors` int(11) DEFAULT NULL,
  `unit_no` int(11) DEFAULT NULL,
  `unit_num_start_with` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `name`, `socity_id`, `floor`, `floors`, `unit_no`, `unit_num_start_with`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'A', 1, 2, NULL, 4, NULL, 1, 0, '2019-09-05 07:06:10', '2019-09-05 07:06:10'),
(2, 'B', 1, 2, NULL, 3, NULL, 1, 0, '2019-09-05 07:06:10', '2019-09-05 07:06:10');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
CREATE TABLE IF NOT EXISTS `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `country_id`, `state_id`, `city_name`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 1, 1, 'Ahmedabad', 1, 0, '2019-09-11 04:05:31', '2019-09-11 04:05:31');

-- --------------------------------------------------------

--
-- Table structure for table `cms_page`
--

DROP TABLE IF EXISTS `cms_page`;
CREATE TABLE IF NOT EXISTS `cms_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(55) NOT NULL,
  `page_slug` varchar(55) NOT NULL,
  `page_content` text NOT NULL,
  `meta_keywords` text,
  `meta_description` text,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `contact_leads`
--

DROP TABLE IF EXISTS `contact_leads`;
CREATE TABLE IF NOT EXISTS `contact_leads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(55) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `description` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact_leads`
--

INSERT INTO `contact_leads` (`id`, `name`, `subject`, `email`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test', NULL, 'test@gmail.com', 'test', '2019-10-07 10:57:08', '2019-10-07 10:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
CREATE TABLE IF NOT EXISTS `country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_name` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`id`, `country_name`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'India', 1, 0, '2019-09-11 03:01:13', '2019-09-11 03:01:33'),
(3, 'U.S.A', 1, 0, '2019-09-11 05:20:34', '2019-09-11 05:20:34');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_name` varchar(255) DEFAULT NULL,
  `event_detail` varchar(255) DEFAULT NULL,
  `venue_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `time` time DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `event_rule` varchar(255) DEFAULT NULL,
  `cost_ticket` varchar(255) DEFAULT NULL,
  `cost` int(11) DEFAULT NULL,
  `contact_name` varchar(255) DEFAULT NULL,
  `phone_no` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website_url` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `event_name`, `event_detail`, `venue_name`, `address`, `date`, `time`, `end_date`, `end_time`, `event_rule`, `cost_ticket`, `cost`, `contact_name`, `phone_no`, `email`, `website_url`, `image`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'BirthDay Event', 'Happy Birthday......', NULL, NULL, '2019-09-10', '07:09:00', '2019-09-10', '12:09:00', 'Only accept blessing as a gift', NULL, NULL, '', NULL, NULL, NULL, '1567663280.jpeg', 1, 0, '2019-09-05 00:31:20', '2019-09-05 00:35:39'),
(2, 'Events', 'events', NULL, NULL, '2019-09-11', '11:09:00', '2019-09-17', '11:09:00', 'Nothing', NULL, NULL, '', NULL, NULL, NULL, '1567665025.jpeg', 1, 0, '2019-09-05 01:00:25', '2019-09-05 01:00:25'),
(3, 'Event Test', 'Event Test Detail', 'Ahmedabad', 'Ahmedabad', '2019-09-10', '05:09:30', '2019-09-11', '05:09:30', 'Nothing', NULL, NULL, '', NULL, NULL, NULL, NULL, 1, 0, '2019-09-11 07:03:18', '2019-09-11 07:03:18'),
(4, 'Test', 'Test', 'Test', 'Test', '2019-09-19', '07:09:30', '2019-09-17', '07:09:30', 'Test', 'No Ticket', NULL, '', NULL, NULL, NULL, NULL, 1, 0, '2019-09-11 08:22:45', '2019-09-11 08:22:45'),
(5, 'Test', 'Test', 'Test', 'Test', '2019-09-11', '07:09:00', '2019-09-17', '07:09:00', 'Test', 'External Ticket', NULL, '', NULL, NULL, NULL, NULL, 1, 0, '2019-09-11 08:24:11', '2019-09-11 08:24:11'),
(6, 'Test', 'Test', 'Test', 'Test', '2019-09-04', '11:09:00', '2019-09-18', '11:09:00', 'Test', 'No Ticket', NULL, 'Test', 'test', 'test@gmail.com', 'www.test.com', NULL, 1, 0, '2019-09-13 00:36:14', '2019-09-13 00:40:13');

-- --------------------------------------------------------

--
-- Table structure for table `floors`
--

DROP TABLE IF EXISTS `floors`;
CREATE TABLE IF NOT EXISTS `floors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `floor` varchar(11) DEFAULT NULL,
  `socity_id` int(11) DEFAULT NULL,
  `block_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `floors`
--

INSERT INTO `floors` (`id`, `floor`, `socity_id`, `block_id`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, '1', 1, 1, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(2, '2', 1, 1, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(3, '1', 1, 2, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(4, '2', 1, 2, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  `action` int(11) DEFAULT NULL,
  `message` text,
  `log_date_time` datetime DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `role_id`, `user_id`, `username`, `role_name`, `action`, `message`, `log_date_time`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 09:20:14', '2019-10-04 09:20:14', '2019-10-04 09:20:14'),
(2, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 11:49:31', '2019-10-04 11:49:31', '2019-10-04 11:49:31'),
(3, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:19:14', '2019-10-04 12:19:14', '2019-10-04 12:19:14'),
(4, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:49:41', '2019-10-04 12:49:41', '2019-10-04 12:49:41'),
(5, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:51:01', '2019-10-04 12:51:01', '2019-10-04 12:51:01'),
(6, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:53:06', '2019-10-04 12:53:06', '2019-10-04 12:53:06'),
(7, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:53:57', '2019-10-04 12:53:57', '2019-10-04 12:53:57'),
(8, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 12:59:37', '2019-10-04 12:59:37', '2019-10-04 12:59:37'),
(9, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 13:09:05', '2019-10-04 13:09:05', '2019-10-04 13:09:05'),
(10, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-04 13:09:13', '2019-10-04 13:09:13', '2019-10-04 13:09:13'),
(11, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 05:17:22', '2019-10-07 05:17:22', '2019-10-07 05:17:22'),
(12, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 05:21:37', '2019-10-07 05:21:37', '2019-10-07 05:21:37'),
(13, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 05:52:09', '2019-10-07 05:52:09', '2019-10-07 05:52:09'),
(14, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 08:31:32', '2019-10-07 08:31:32', '2019-10-07 08:31:32'),
(15, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 08:32:27', '2019-10-07 08:32:27', '2019-10-07 08:32:27'),
(16, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 08:33:01', '2019-10-07 08:33:01', '2019-10-07 08:33:01'),
(17, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 08:34:26', '2019-10-07 08:34:26', '2019-10-07 08:34:26'),
(18, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-07 11:06:35', '2019-10-07 11:06:35', '2019-10-07 11:06:35'),
(19, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-09 06:45:37', '2019-10-09 06:45:37', '2019-10-09 06:45:37'),
(20, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-10 10:11:08', '2019-10-10 10:11:08', '2019-10-10 10:11:08'),
(21, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-11 05:09:09', '2019-10-11 05:09:09', '2019-10-11 05:09:09'),
(22, 1, 1, NULL, 'Admin', 1, 'Admin <b>\'  \'</b> Logged in', '2019-10-11 05:25:15', '2019-10-11 05:25:15', '2019-10-11 05:25:15');

-- --------------------------------------------------------

--
-- Table structure for table `maintenance`
--

DROP TABLE IF EXISTS `maintenance`;
CREATE TABLE IF NOT EXISTS `maintenance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socity_id` int(11) DEFAULT NULL,
  `block_id` int(11) DEFAULT NULL,
  `ammount` int(11) DEFAULT NULL,
  `unit_resident_type` varchar(255) DEFAULT NULL,
  `payment_cycle` varchar(255) DEFAULT NULL,
  `yearly` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maintenance`
--

INSERT INTO `maintenance` (`id`, `socity_id`, `block_id`, `ammount`, `unit_resident_type`, `payment_cycle`, `yearly`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 5000, 'Owner', 'First Month Of Year', NULL, 1, 0, '2019-09-04 04:29:17', '2019-09-04 04:29:17'),
(2, 1, 2, 6000, 'Rentals', 'Last Month Of Year', NULL, 1, 0, '2019-09-04 04:29:35', '2019-09-04 04:29:35'),
(3, 1, 1, 1100, 'Rentals', 'First Month Of Year', NULL, 1, 0, '2019-09-05 07:08:22', '2019-09-05 07:08:22'),
(4, 1, 1, 6000, 'Rentals', 'First Month Of Year', NULL, 1, 0, '2019-09-09 00:17:20', '2019-09-09 00:17:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8 NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_12_18_114555_create_permission_tables', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

DROP TABLE IF EXISTS `model_has_permissions`;
CREATE TABLE IF NOT EXISTS `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8 NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

DROP TABLE IF EXISTS `model_has_roles`;
CREATE TABLE IF NOT EXISTS `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) CHARACTER SET utf8 NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\User', 1),
(2, 'App\\User', 2),
(2, 'App\\User', 4),
(3, 'App\\User', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('059bc635b7829c181651b66f4740532cbc0f6fef030f901090f93161f4ccaf26e94a1263c1e42514', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-25 06:32:36', '2019-01-25 06:32:36', '2020-01-25 12:02:36'),
('1291728793a4a4cd97af499024f87bf867d1072b6ca0c81628640788f6666c74c7385b2b4462afcd', 334, 1, 'Personal Access Token', '[]', 0, '2019-03-03 23:30:43', '2019-03-03 23:30:43', '2020-03-04 05:00:43'),
('151792e058ef425866c411cd9e021ac5ae3e827b3862a8641fa140d8b2490377dbc4a8f1de17c743', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-25 06:32:55', '2019-01-25 06:32:55', '2020-01-25 12:02:55'),
('23bf4da6090680c2d935d018fb020d87628cdb00f42720720170c8c80e2a6e64b0c1288e495eaec0', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-05 01:21:51', '2019-02-05 01:21:51', '2020-02-05 06:51:51'),
('25610aa9fbdccccddfe75f60eeb3c077c3a4c6a691caeabf72c30b25b05a51cc56a7d109073ec5b1', 4, 1, 'Personal Access Token', '[]', 0, '2019-02-14 00:00:59', '2019-02-14 00:00:59', '2020-02-14 05:30:59'),
('2d764885af253b3bcd1fdd919184e29b601e71e37e58f8348cd99c2b1f8d1d655c7f24148a33bcd6', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 08:05:27', '2019-02-04 08:05:27', '2020-02-04 13:35:27'),
('338757dfc773b1bf406d0cc02b49a95f55f3e485b46c30069fc63d8384b2ce0fc1b5dd02de0740fe', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 07:48:48', '2019-02-04 07:48:48', '2020-02-04 13:18:48'),
('3418185b5a38af647d6ba11095f87ffbaaf1d5f19a48e1a720c9001b8c418c478fbcb58184a7b5cf', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 07:55:42', '2019-02-04 07:55:42', '2020-02-04 13:25:42'),
('34a9b62d4672844a546e7b608abb8cd6c516559f2cb9aed7f03ddb7b1ca0a9956c2f6c8778dff30d', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:52:46', '2019-02-04 06:52:46', '2020-02-04 12:22:46'),
('3bfb4ebff3c8e842e7fdf1b2a994b98b95dd907beacfabb1653e2dd806cd9792c35b22c3bedfbe64', 3, 1, 'Personal Access Token', '[]', 0, '2019-01-30 00:06:34', '2019-01-30 00:06:34', '2020-01-30 05:36:34'),
('3c5acd6cd4f08c53fd78b5add1b2f520f1e0c2cdbb9367263def3e92c81801029f97961eca680914', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:58:54', '2019-02-04 06:58:54', '2020-02-04 12:28:54'),
('3caa8e261728153d232930aa1ec9f4a4396e75cbca8970767abb23e8b94b106cd58357c2dedd739d', 4, 1, 'Personal Access Token', '[]', 0, '2019-02-04 02:57:25', '2019-02-04 02:57:25', '2020-02-04 08:27:25'),
('3ce01419484d6577c2200524ba6d20fc66ac2e2f8bb3dd29769bbccf8dd8f053760f2382b3176343', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-05 01:22:17', '2019-02-05 01:22:17', '2020-02-05 06:52:17'),
('3cfd67a35a0afc5d25e826f899a451cafa2742a8a3f7c92c069948ab481da6538d78ee68abbfc23f', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:27:51', '2019-02-04 03:27:51', '2020-02-04 08:57:51'),
('3f198cdf013e732b2c0f464c54ed7c95ec67cece6218a10c1c31a1f7573f42003635146009b4ac75', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 01:21:49', '2019-01-28 01:21:49', '2020-01-28 06:51:49'),
('406c7e1c9d9c639b8a264974637b2e95b47a4860982b22342d382b002e6c1156dcd1ac19bfec126e', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-25 06:33:22', '2019-01-25 06:33:22', '2020-01-25 12:03:22'),
('41a843319b735c837f28abcbe2400f3fa14d9aea001a5c844d959237cc895a9effcfbd1191cfd990', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-25 06:34:07', '2019-01-25 06:34:07', '2020-01-25 12:04:07'),
('42c68b385be6fffd65305836a98a0b097f7b73c782775f7a1786f40b915cbd6534f8f924ef6f5d67', 326, 1, 'Personal Access Token', '[]', 0, '2019-02-14 01:21:34', '2019-02-14 01:21:34', '2020-02-14 06:51:34'),
('448f762cd8e4780e8e99ecea3c6ee079a8f9c3c9310a03418711f07ec5724b9d8e211b09cf340665', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:33:28', '2019-02-04 04:33:28', '2020-02-04 10:03:28'),
('45aba215c558f27113a03645abaa1e98466db5f45638bf4f69684ab50a863da205c86784d15175c5', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-10 23:58:49', '2019-02-10 23:58:49', '2020-02-10 16:58:49'),
('4fc991709c44cdfe422d4eb1afc933edfaea00aa0ec778dbd5493b6591dbd937a15050c91b29008d', 3, 1, 'Personal Access Token', '[]', 0, '2019-01-30 00:07:30', '2019-01-30 00:07:30', '2020-01-30 05:37:30'),
('4ff2d994e23ec4717c6215ce56a9adf774b953226901baacf77a58b2162db942f4c77d5d51c58fe1', 326, 1, 'Personal Access Token', '[]', 0, '2019-02-14 01:22:48', '2019-02-14 01:22:48', '2020-02-14 06:52:48'),
('50e15ee61bbd0e64f6ab9a01dd851d9ce23f26a618d7b0fadc17951a3953bf3855d7714ffb145127', 2, 1, 'Personal Access Token', '[]', 0, '2019-02-13 23:48:21', '2019-02-13 23:48:21', '2020-02-14 05:18:21'),
('516912f1a397b2e78c41bdd4bea3209963ac0140e7b3551ac0584bb25dd9a9c206b0b4afc43e339e', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 08:11:41', '2019-02-04 08:11:41', '2020-02-04 13:41:41'),
('51b7d33bf4fdbba9625a479810e7f12ca39f7005fb9b68b0b7a2f6a122e9f4378164b6862cd37916', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-29 23:51:55', '2019-01-29 23:51:55', '2020-01-30 05:21:55'),
('536fb3379868499a0c57c24e1f112e6ba36abecc3bbb53e263fe93ac9dd09b7353f211236e540c15', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 08:34:45', '2019-01-28 08:34:45', '2020-01-28 14:04:45'),
('54d81bc019702dd4e87c5ee1a0c200dc3a844c2a394815af3bbf5d0e4b1c0e6928ebea731d352f06', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 08:25:38', '2019-02-04 08:25:38', '2020-02-04 13:55:38'),
('58703324d2c6b760ea7ffdfd07d023f0a7fd286ab2028f95015dcae7cd272411d541b4feec50cf8f', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:01:21', '2019-02-04 04:01:21', '2020-02-04 09:31:21'),
('58902da953caa4ae53ba69e0c5807275ba7533b81775b02c884bc1d50f642c46319b8d84e63e7067', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:50:43', '2019-02-04 03:50:43', '2020-02-04 09:20:43'),
('606f302bddd0854e70da53a421514eb03fc8fbc8cfee4bf718e0bc1ea4ff4d153af4189061e4780c', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:14:57', '2019-02-04 04:14:57', '2020-02-04 09:44:57'),
('623323bd83e7c904505911f340289654931c94212b462fd832ae53c2ce8a8018ae9c36fa48b920c4', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:36:30', '2019-02-04 03:36:30', '2020-02-04 09:06:30'),
('624a5752da17b58b6fcbb2958ec90ad56cdd16d2569fa4eb70cd72f8afd7145884d716083452eb1d', 1, 1, 'MyApp', '[]', 0, '2019-01-25 06:28:29', '2019-01-25 06:28:29', '2020-01-25 11:58:29'),
('643d27657ce63112a74eecb4d33d5ae2859652e6c5e5e448d7af7277f2e6bb7cfd50402d75657371', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:49:44', '2019-02-04 03:49:44', '2020-02-04 09:19:44'),
('67f49a8d0116918149960eed953c2d46f335cc9fce83d95b6687a3573cf97deaf288333b9ef1fd0d', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:10:46', '2019-02-04 04:10:46', '2020-02-04 09:40:46'),
('69d7632a182282fe88d103d9be83af8d9e9ca277372dae878bc419b0d384e929e95591479bb4cbed', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:17:50', '2019-02-04 03:17:50', '2020-02-04 08:47:50'),
('6cb3c86b471441c3fd6f85f25f690798889385cd5dd0b770ec2ef33f3d88506cff9ff5efe423a5c3', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-05 01:29:10', '2019-02-05 01:29:10', '2020-02-05 06:59:10'),
('6cfedf0ccf9c90debcdcbfb82228b157f7f4813fe181309d7d628998be8b6a530efda7774c048ef3', 326, 1, 'Personal Access Token', '[]', 0, '2019-02-13 23:49:16', '2019-02-13 23:49:16', '2020-02-14 05:19:16'),
('7059c907fb7b7d3dddcbce62adae39d8a268dc41b985d0e40ef2e909c2eea779ecfa7cf26bed76ea', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 07:40:23', '2019-02-04 07:40:23', '2020-02-04 13:10:23'),
('7994ba539f20150d42aafb8067f0bc56f1f2432466af1aa01447e7b2983a5dbb159ef3bff421c3da', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:47:52', '2019-02-04 03:47:52', '2020-02-04 09:17:52'),
('8059851631476025a11e67dbf205b771124859fc98be4fb8fbefa79f49f3835210f6788573de1201', 326, 1, 'Personal Access Token', '[]', 0, '2019-02-14 01:48:01', '2019-02-14 01:48:01', '2020-02-14 07:18:01'),
('81f9a1c84e8487f1d80b89dedbe2076850cf229fcaee85ed99e9030593d7a676a0d9d87123ceb618', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-25 06:32:04', '2019-01-25 06:32:04', '2020-01-25 12:02:04'),
('8405c72690f7db484f7360e2e562aba96bfaf678226d04cf882872e8bbd88d067399b967bc42bd30', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:45:00', '2019-02-04 06:45:00', '2020-02-04 12:15:00'),
('85210a148b778123d4c9b74e4c931c403272e807ed5c57e64b9baf4506355c5891c63f76ab59ab84', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 02:57:18', '2019-02-04 02:57:18', '2020-02-04 08:27:18'),
('87acbea03efb80eb084a4dfb89453bf36c46e35a48bb7b5ae5390d0523c7cb2108d3e6e9380b0715', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:38:47', '2019-02-04 04:38:47', '2020-02-04 10:08:47'),
('8aa37b71900482e1400b613088a45b9ee4815edbb426056a229d7af0e7e6c72bfdb20c118a67c47c', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:43:24', '2019-02-04 04:43:24', '2020-02-04 10:13:24'),
('94713c04b039e57b3d60f6c8a3c7e64325612d9bb82aa900d820d152fcab41245d4f298cd4b25e61', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:44:31', '2019-02-04 06:44:31', '2020-02-04 12:14:31'),
('9a5b6bad291765c6fbcd800b58ddcdb53f3ebca824b56e990aba4e26ce785129c8f35aaff756f471', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:46:24', '2019-02-04 06:46:24', '2020-02-04 12:16:24'),
('a297f906eec6af28e2a1c492cb8a027b226dac09de3050bd3b3223177f5738fdb98ae04a03854eb5', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 01:24:02', '2019-01-28 01:24:02', '2020-01-28 06:54:02'),
('a37573568e907d9cc009b5d483b0b2949ffbcbe7e2902c3bcbb369022b3b692a4be457a485032536', 4, 1, 'Personal Access Token', '[]', 0, '2019-02-14 00:01:57', '2019-02-14 00:01:57', '2020-02-14 05:31:57'),
('a456824b3410b4500785cd5934e74071f8f1a098f3ef93b018b31d48f205bd564886e831d6af3ab4', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:40:50', '2019-02-04 06:40:50', '2020-02-04 12:10:50'),
('a620803e40c2474c80535158ff0ece28313aac4d2a712e8307d7119bc4048e8ccfa28416abc98ba6', 326, 1, 'Personal Access Token', '[]', 0, '2019-02-14 00:00:33', '2019-02-14 00:00:33', '2020-02-14 05:30:33'),
('ae1ab218338d71ca2b64444d93383100fb7f5b2a12a4b225dff99c514ca04322f711c2e3f6a58887', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:58:36', '2019-02-04 03:58:36', '2020-02-04 09:28:36'),
('af930b87fabf4948fec0a86991c0a73b12310e504168fcb8bd81bb18e02796d168f9d002340ebc75', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 06:10:56', '2019-02-04 06:10:56', '2020-02-04 11:40:56'),
('b4a589b3c9a8767c69751021779857622c6f406bcc129d21e5f0eb44369974e68eb19a4b8c78710d', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-05 01:30:57', '2019-02-05 01:30:57', '2020-02-05 07:00:57'),
('b8441827bb65d4495f46def38586f7fdb2e3bcbdc85959b7cacfb50f80b502ac9f2f2bc3b29adec4', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:53:48', '2019-02-04 03:53:48', '2020-02-04 09:23:48'),
('bf6c138d2fdf8359f36605edc7bc40bfdb7bce205908b818466f259763f4e0e67e664daa7f0ec580', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:42:43', '2019-02-04 03:42:43', '2020-02-04 09:12:43'),
('c28b60143cc53f3e31865480b4f8b2c16eccabaffe314c5e07743fe3518ce416e97185445b5538e1', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:16:18', '2019-02-04 03:16:18', '2020-02-04 08:46:18'),
('c29c72d1e56ac581b3bd9ba8ef8e5ccae8d972bafb7eb343cb8fb778a0d0a70ed44427c85cec8f7e', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:17:31', '2019-02-04 03:17:31', '2020-02-04 08:47:31'),
('c3639b4bb46a6d5f6c4d6939fbbbd7344a6d8b706b1cb44db695af49e52c7fcca1b3a3485ec708dd', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:22:34', '2019-02-04 04:22:34', '2020-02-04 09:52:34'),
('c4da519b24480f0714998349884b6e5fe4d3453075a5391df00f922e73303559116b5ce39c52a8cf', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 08:06:51', '2019-01-28 08:06:51', '2020-01-28 13:36:51'),
('c9f975721696bcf0aba011d86b129ba7dda5ee965d877e2767c542f372aa53f88615d8c07a81e126', 2, 1, 'Personal Access Token', '[]', 0, '2019-02-14 01:20:09', '2019-02-14 01:20:09', '2020-02-14 06:50:09'),
('cf7eafac108b5d35378535597824f167aa655d4ea086ce12f3577e123fad7a85001112408e17c20d', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:57:12', '2019-02-04 03:57:12', '2020-02-04 09:27:12'),
('cfc8fe7b72d628c251c6e21ec304837128c35bbba950ae7a4ea94683f3e9fc56958dc7da0cd2e019', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:41:18', '2019-02-04 04:41:18', '2020-02-04 10:11:18'),
('d026876917c23d2e3dc4695ef8b78d5fccef7c02b9562cc683cc86dc811dfdc1d5b487b24518660d', 2, 1, 'Personal Access Token', '[]', 0, '2019-02-13 23:48:17', '2019-02-13 23:48:17', '2020-02-14 05:18:17'),
('e08a5baa9a094131c7d840446eb80230e28e9617ae022837f392b8b52b0eace4b779f8cec2210d13', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:41:04', '2019-02-04 03:41:04', '2020-02-04 09:11:04'),
('e28a342c1d09164ae8bee448f3a9884d7a296d8878820b029fe1ac8891563ab0e4569e8fd3fff07b', 3, 1, 'Personal Access Token', '[]', 0, '2019-01-30 00:06:10', '2019-01-30 00:06:10', '2020-01-30 05:36:10'),
('e33bf99937d908990d92c8c761f539d5e541eec8d9b2c99bdfd6adf873c922482de73bd013723fe0', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 01:23:27', '2019-01-28 01:23:27', '2020-01-28 06:53:27'),
('e723506cfdd6842a813e1a70a758c67fc66d8de9d38278ade32e93ce4285696f0c63a46001ef8648', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 08:03:34', '2019-02-04 08:03:34', '2020-02-04 13:33:34'),
('ea62094556090ae6d389cf87c69619ce2dcba7e8c89a039724da3fa19267beb967b3eb45e4b409f1', 1, 1, 'Personal Access Token', '[]', 0, '2019-01-28 08:34:57', '2019-01-28 08:34:57', '2020-01-28 14:04:57'),
('ee9f296bb36bb5dd233c0d987b4e32c390d543adc83bf7bcf4fee8e060584aae09d92f9ab47d4d9f', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:52:33', '2019-02-04 03:52:33', '2020-02-04 09:22:33'),
('ff3ab5865d8159c3334831f79d7da8ee478f362e8db448130773afba6e8a3cef5e08916f7e9f289c', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 04:03:58', '2019-02-04 04:03:58', '2020-02-04 09:33:58'),
('ffb71106c88db7af8ed36a38324efc0fa9ec6938fc6464c9ba16fc2addaa257ac845d3ae1fcd7eb0', 3, 1, 'Personal Access Token', '[]', 0, '2019-02-04 03:35:55', '2019-02-04 03:35:55', '2020-02-04 09:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Eutility Personal Access Client', 'EgdhGRhdQkmb5033MEaCFT4PpC8XGnIgmse2gt75', 'http://localhost', 1, 0, 0, '2019-01-25 06:17:55', '2019-01-25 06:17:55'),
(2, NULL, 'Eutility Password Grant Client', 'j2SfgVk5drTuhwoio6TuJCVR9fmc9T1ELp1wGsfL', 'http://localhost', 0, 1, 0, '2019-01-25 06:17:55', '2019-01-25 06:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-01-25 06:17:55', '2019-01-25 06:17:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `parking`
--

DROP TABLE IF EXISTS `parking`;
CREATE TABLE IF NOT EXISTS `parking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `socity_id` int(11) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `parking_no` int(11) DEFAULT NULL,
  `block_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parking`
--

INSERT INTO `parking` (`id`, `socity_id`, `floor`, `parking_no`, `block_id`, `floor_id`, `unit_id`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 1, 'basment', 101, NULL, NULL, NULL, 1, 0, '2019-09-03 00:58:48', '2019-09-03 00:58:48'),
(4, 2, 'basement', 100, 3, 9, 27, 1, 0, '2019-09-03 01:42:39', '2019-09-03 01:57:44'),
(6, 2, 'On-Street Parking', 103, 2, 3, 9, 1, 0, '2019-09-11 01:29:24', '2019-09-11 01:31:57');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) CHARACTER SET utf8 NOT NULL,
  `token` varchar(191) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `url` text CHARACTER SET utf8,
  `is_parent` bigint(20) NOT NULL DEFAULT '0',
  `font_icon` text CHARACTER SET utf8,
  `order_by` int(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `url`, `is_parent`, `font_icon`, `order_by`, `created_at`, `updated_at`) VALUES
(1, 'Logs and Error', 'web', 'admin/logs', 0, 'fa fa-history', 11, '2019-02-19 06:31:38', '2019-02-19 06:31:38'),
(2, 'Permission', 'web', 'admin/dashboard', 0, 'fa fa-lock', 2, '2018-12-18 18:27:19', '2019-01-19 00:55:27'),
(3, 'Roles', 'web', 'admin/roles', 2, 'fa fa-list', 0, '2018-12-18 18:27:25', '2019-10-03 00:49:09'),
(4, 'Menus', 'web', 'admin/menus', 2, 'fa fa-user', 0, '2018-12-18 18:27:33', '2019-10-03 00:49:26'),
(5, 'Users Management', 'web', 'admin/dashboard', 0, 'fa fa-user', 3, '2018-12-18 18:27:42', '2019-10-03 01:06:42'),
(6, 'Sub Admins', 'web', 'admin/authors', 5, 'fa fa-users', 0, '2018-12-18 18:27:57', '2019-10-10 23:44:26'),
(7, 'Society Admin', 'web', 'admin/users', 5, 'fa fa-users', 0, '2018-12-18 18:28:06', '2019-10-10 23:44:42'),
(8, 'Home', 'web', 'admin/dashboard', 0, 'fa fa-home', 1, '2018-12-18 19:26:54', '2019-10-03 00:48:24'),
(10, 'Settings', 'web', 'admin/settings', 0, 'fa fa-cog', 10, '2019-10-04 03:55:46', '2019-10-04 03:55:46'),
(11, 'Posts', 'web', 'admin/posts', 0, 'fa fa-list', 6, '2019-10-04 03:56:53', '2019-10-04 03:56:53'),
(12, 'Events', 'web', 'admin/events', 0, 'fa fa-list', 7, '2019-10-04 03:57:34', '2019-10-04 03:57:34'),
(13, 'Category', 'web', 'admin/category', 0, 'fa fa-list', 5, '2019-10-04 07:18:25', '2019-10-04 07:18:25'),
(14, 'News Letter', 'web', 'admin/news-letter', 0, 'fa fa-list', 9, '2019-10-04 07:18:48', '2019-10-07 04:20:33'),
(15, 'CMS Page', 'web', 'admin/cms', 0, 'fa fa-list', 8, '2019-10-07 01:22:55', '2019-10-07 01:22:55'),
(16, 'Society Block Admin', 'web', 'admin/block-admins', 5, 'fa fa-users', 11, '2019-10-10 23:45:18', '2019-10-10 23:45:18');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `media_type` int(11) NOT NULL DEFAULT '0' COMMENT '0 => Image , 1 => Video',
  `media` varchar(255) DEFAULT NULL,
  `discription` longtext,
  `visibility` int(11) NOT NULL DEFAULT '1' COMMENT '0 => Private , 1 => Public',
  `enable_review` int(11) NOT NULL DEFAULT '1' COMMENT '0 => Disable, 1 => Enabled',
  `is_drafted` int(11) NOT NULL DEFAULT '0' COMMENT '0 => Draft , 1 => Publish',
  `post_title` varchar(255) DEFAULT NULL,
  `post_slug` varchar(255) DEFAULT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_comments`
--

DROP TABLE IF EXISTS `post_comments`;
CREATE TABLE IF NOT EXISTS `post_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `comment` longtext,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `post_likes`
--

DROP TABLE IF EXISTS `post_likes`;
CREATE TABLE IF NOT EXISTS `post_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `guard_name` varchar(191) CHARACTER SET utf8 NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'web', '2018-12-17 23:17:14', '2018-12-20 12:37:48'),
(2, 'Sub-Admin', 'web', '2018-12-18 21:43:58', '2019-10-11 05:01:28'),
(3, 'Society-Admin', 'web', '2018-12-17 23:17:32', '2019-10-11 05:01:32'),
(4, 'Society-Block-Admin', 'web', '2019-10-11 04:54:15', '2019-10-11 05:01:41');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

DROP TABLE IF EXISTS `role_has_permissions`;
CREATE TABLE IF NOT EXISTS `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(8, 2),
(8, 3),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meta_title` varchar(55) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` text,
  `site_title` varchar(55) DEFAULT NULL,
  `site_logo` varchar(55) DEFAULT NULL,
  `favicon` varchar(55) DEFAULT NULL,
  `copyright_text` varchar(55) DEFAULT NULL,
  `contactus_email` varchar(55) DEFAULT NULL,
  `facebook_link` text,
  `twitter_link` text,
  `instagram_link` text,
  `address` text,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `meta_title`, `meta_keyword`, `meta_description`, `site_title`, `site_logo`, `favicon`, `copyright_text`, `contactus_email`, `facebook_link`, `twitter_link`, `instagram_link`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Example Title', 'Example Keyword', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'The List Are You On It', '1569495497.png', '1569495506.png', '2018 © TheListAreYouOnIt', 'test.cosmonautgroup@gmail.com', 'https://facebook.com/example', 'https://twitter.com/example', 'https://instagram.com/example', 'Test', '2019-06-05 18:57:45', '2019-10-09 09:45:54');

-- --------------------------------------------------------

--
-- Table structure for table `socity`
--

DROP TABLE IF EXISTS `socity`;
CREATE TABLE IF NOT EXISTS `socity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subscription_id` int(11) DEFAULT NULL,
  `society_name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `contact_no` varchar(255) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `socity`
--

INSERT INTO `socity` (`id`, `subscription_id`, `society_name`, `address`, `latitude`, `longitude`, `email`, `contact_no`, `created_by`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Test', 'Test', NULL, NULL, 'test@gmail.com', '1234567890', '50', 1, 0, '2019-09-05 07:04:53', '2019-09-11 00:42:41'),
(2, 3, 'Test Soc', 'Ahmedabad, Gujarat, India', NULL, NULL, 'testsoc@gmail.com', '1234567890', NULL, 1, 0, '2019-09-11 00:49:46', '2019-09-11 00:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `state`
--

DROP TABLE IF EXISTS `state`;
CREATE TABLE IF NOT EXISTS `state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `state_name` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `state`
--

INSERT INTO `state` (`id`, `country_id`, `state_name`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 1, 'Gujrat', 1, 0, '2019-09-11 03:27:33', '2019-09-11 03:27:33');

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

DROP TABLE IF EXISTS `subscription`;
CREATE TABLE IF NOT EXISTS `subscription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_name` varchar(255) DEFAULT NULL,
  `validity` varchar(255) DEFAULT NULL,
  `price` int(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `plan_name`, `validity`, `price`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(1, 'test', '1 month', 500, 1, 0, '2019-08-22 06:40:59', '2019-08-22 06:52:52'),
(2, 'tests', '1 month', 120, 1, 0, '2019-08-23 01:55:45', '2019-08-23 01:55:45'),
(3, 'test subscription', '2 month', 120, 1, 0, '2019-09-02 03:52:23', '2019-09-02 03:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `tags_name` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

DROP TABLE IF EXISTS `unit`;
CREATE TABLE IF NOT EXISTS `unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `block_id` int(11) DEFAULT NULL,
  `floor_id` int(11) DEFAULT NULL,
  `unit_id` int(11) DEFAULT NULL,
  `bhk` int(11) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `squarfeet` varchar(255) DEFAULT NULL,
  `parking_no` int(11) DEFAULT NULL,
  `assign_user` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `block_id`, `floor_id`, `unit_id`, `bhk`, `view`, `squarfeet`, `parking_no`, `assign_user`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(4, 1, 1, 2, 1, 'Garden', '120', 2, '53', 1, 0, '2019-09-11 01:03:20', '2019-09-11 01:19:44'),
(5, 2, 4, 12, 1, 'Test', '130', 2, '54', 1, 0, '2019-09-11 01:06:36', '2019-09-17 01:14:21');

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

DROP TABLE IF EXISTS `units`;
CREATE TABLE IF NOT EXISTS `units` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_name` int(11) DEFAULT NULL,
  `socity_id` int(11) DEFAULT NULL,
  `unit_detail` varchar(255) DEFAULT NULL,
  `parking_no` int(11) DEFAULT NULL,
  `assign_user` varchar(255) DEFAULT NULL,
  `block_id` varchar(11) DEFAULT NULL,
  `floor_id` varchar(11) DEFAULT NULL,
  `unit_number` int(11) DEFAULT NULL,
  `squarfeet` double DEFAULT NULL,
  `bhk` double DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_delete` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `unit_name`, `socity_id`, `unit_detail`, `parking_no`, `assign_user`, `block_id`, `floor_id`, `unit_number`, `squarfeet`, `bhk`, `view`, `is_active`, `is_delete`, `created_at`, `updated_at`) VALUES
(2, 102, 1, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(3, 103, 1, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(4, 104, 1, NULL, NULL, NULL, '1', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(5, 201, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(6, 202, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(7, 203, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(8, 204, 1, NULL, NULL, NULL, '1', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(9, 101, 1, NULL, NULL, NULL, '2', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(10, 102, 1, NULL, NULL, NULL, '2', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(11, 103, 1, NULL, NULL, NULL, '2', '1', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:10', '2019-09-05 12:36:10'),
(12, 201, 1, NULL, NULL, NULL, '2', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:11', '2019-09-05 12:36:11'),
(13, 202, 1, NULL, NULL, NULL, '2', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:11', '2019-09-05 12:36:11'),
(14, 203, 1, NULL, NULL, NULL, '2', '2', NULL, NULL, NULL, NULL, 1, 0, '2019-09-05 12:36:11', '2019-09-05 12:36:11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `last_name` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `city` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `address` text CHARACTER SET utf8,
  `image` text CHARACTER SET utf8,
  `role_id` int(11) DEFAULT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `is_verified` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` bigint(20) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `phone`, `city`, `address`, `image`, `role_id`, `remember_token`, `is_active`, `is_verified`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Super', 'Admin', 'sharad.cosmonautgroup@gmail.com', '2019-01-23 13:00:00', '$2y$10$lFkVL4a7FM2EjcB1u2iAruuSuNnp.tfiOCY1xhzLTBjt1j50c7UZu', NULL, NULL, NULL, '1570181408.jpg', 1, 'YeSdYMFFPJPkFswnBWrPXGLgaobapxiij3szurwIJSOUAMIz8BfSHMKZyDIo', 1, 1, 1, '2018-12-17 12:22:40', '2019-10-07 01:48:43'),
(2, 'Test', 'Author', 'author@gmail.com', NULL, '$2y$10$aCdlk9mBKjU4CP5YqIPleOd9G3wF7MKVKS9HebXjtzipOWLzFJsFa', NULL, NULL, NULL, '1570182218.jpg', NULL, '343M10LMKmDJI9iXuAivSMna852TG6IzPOuXSlJ5UyigpTHXVvF9U3s0VsLQ', 1, 1, 1, '2019-10-04 04:13:38', '2019-10-04 07:21:39'),
(3, 'Test', 'User', 'user@gmail.com', NULL, '$2y$10$Tem/oiALDpfrn.0LgoEsw.K3uuY7UJUco.ncb6b7VKmQ9sm5vkLLW', NULL, NULL, NULL, '1570184989.jpg', NULL, 'NSuDI6cM9R8GzBiV4Xs9fo0ieoJIssp2yB7KfIrFbaiEBTcisa', 1, 1, 1, '2019-10-04 04:59:50', '2019-10-04 05:13:54'),
(4, 'Super', 'Author', 'test1.cosmonautgroup@gmail.com', NULL, '$2y$10$VJBkrxJo/qZV9a6CIorJ0.wGr/JDR8fAJIAk3j9xfc1WGrXr67SHG', NULL, NULL, NULL, NULL, NULL, 'bI9iE830LQcOL5RJjqQ1knaY0l65beRHrOL7h5EzxyAOCvpK3jo9MneM4daT', 1, 1, 0, '2019-10-07 02:59:34', '2019-10-07 03:00:17');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
