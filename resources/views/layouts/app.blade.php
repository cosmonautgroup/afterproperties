<!DOCTYPE html>
<html lang="en">
   <!-- begin::Head -->
   <head>
      <meta charset="utf-8" />
      <title>{{ config('app.name', 'Afterproperties') }} |  @yield('title')</title>
      <meta name="description" content="Latest updates and statistic charts">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
      <!--begin::Web font -->
      <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
      <script>
         WebFont.load({
               google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
               active: function() {
                   sessionStorage.fonts = true;
               }
             });
      </script>
      <link href="{{ asset('public/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('public/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('public/assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('public/css/main.css') }}" rel="stylesheet" type="text/css" />
      <link rel="shortcut icon" href="{{url('public/images/list-logo.png')}}" />
      @yield('css')
   </head>
   <!-- begin::Body -->
   <body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
      <!-- begin:: Page -->
      <div class="m-grid m-grid--hor m-grid--root m-page">
         <!-- BEGIN: Header -->
         <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
            <div class="m-container m-container--fluid m-container--full-height">
               <div class="m-stack m-stack--ver m-stack--desktop">
                  <!-- BEGIN: Brand -->
                  <div class="m-stack__item m-brand  m-brand--skin-dark ">
                     <div class="m-stack m-stack--ver m-stack--general">
                        <div class="m-stack__item m-stack__item--middle m-brand__logo">
                           <a href="{{url('/')}}" class="m-brand__logo-wrapper">
                              <img src="{{url('public/images/list-logo.png')}}" width="110"> 
                           </a>
                        </div>
                        <div class="m-stack__item m-stack__item--middle m-brand__tools">
                           <a href="javascript:;" id="m_aside_left_minimize_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                           <span></span>
                           </a>
                           <!-- END -->
                           <a href="javascript:;" id="m_aside_left_offcanvas_toggle" class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                           <span></span>
                           </a>
                          
                           <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                           <i class="flaticon-more"></i>
                           </a>
                        </div>
                     </div>
                  </div>
                  <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">
                     <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                     <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                        <div class="m-stack__item m-topbar__nav-wrapper">
                           <ul class="m-topbar__nav m-nav m-nav--inline">
                              <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                 m-dropdown-toggle="click">
                                 <a href="#" class="m-nav__link m-dropdown__toggle">
                                 <span class="m-topbar__userpic">
                                 <?php if (isset(Auth::user()->image) && Auth::user()->image!='' && file_exists(public_path().'/images/users/'.Auth::user()->image)) { 
                                    $image = url('/public').'/images/users/'.Auth::user()->image;
                                    }else{
                                    $image = url('/public').'/images/users/no-image.png';
                                    } ?> 
                                 <img src="{{ $image }}" class="m--img-rounded m--marginless" alt="" />
                                 </span>
                                 <span class="m-topbar__username m--hide">{{ Auth::user()->name }}</span>
                                 </a>
                                 <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                       <div class="m-dropdown__header m--align-center" style="background: url(public/assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                          <div class="m-card-user m-card-user--skin-dark">
                                             <div class="m-card-user__pic">
                                                <?php if (isset(Auth::user()->image) && Auth::user()->image!='' && file_exists(public_path().'/images/users/'.Auth::user()->image)) { 
                                                   $image = url('/public').'/images/users/'.Auth::user()->image;
                                                   }else{
                                                   $image = url('/public').'/images/users/no-image.png';
                                                   } ?>
                                                <img src="{{ $image }}" class="m--img-rounded m--marginless" alt="" />
                                             </div>
                                             <div class="m-card-user__details">
                                                <span class="m-card-user__name m--font-weight-500">{{ Auth::user()->name }}</span>
                                                <a href="" class="m-card-user__email m--font-weight-300 m-link">{{ Auth::user()->email }}</a>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="m-dropdown__body">
                                          <div class="m-dropdown__content">
                                             <ul class="m-nav m-nav--skin-light">
                                                <li class="m-nav__section m--hide">
                                                   <span class="m-nav__section-text">Section</span>
                                                </li>
                                                <li class="m-nav__item">
                                                   <a href="{{ route('profile') }}" class="m-nav__link">
                                                   <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                   <span class="m-nav__link-title">
                                                   <span class="m-nav__link-wrap">
                                                   <span class="m-nav__link-text">My Profile</span>
                                                   <span class="m-nav__link-badge"><span class="m-badge m-badge--success"></span></span>
                                                   </span>
                                                   </span>
                                                   </a>
                                                </li>
                                                <li class="m-nav__separator m-nav__separator--fit">
                                                </li>
                                                <li class="m-nav__item">
                                                   <a href="{{ route('logout') }}" class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </header>
         <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
            <div id="m_aside_left" class="m-grid__item  m-aside-left  m-aside-left--skin-dark ">
               <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark " m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                  <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                     <?php $permissions = \Spatie\Permission\Models\Permission::where('is_parent',0)->orderBy('order_by','asc')->get();
                        if (!empty($permissions)) {
                          foreach ($permissions as $permission) {
                            $ind_id = preg_replace('/\s+/', '_', $permission->name);
                        
                              if(auth()->user()->can($permission->name)){
                                $sub_permissions = \Spatie\Permission\Models\Permission::where('is_parent',$permission->id)->get(); 
                                  $icon = "flaticon-layers";
                                  if(isset(($permission->font_icon)) && !empty($permission->font_icon)){
                                    $icon = ($permission->font_icon);
                                  }
                              if(count($sub_permissions) > 0){ 
                                  
                              ?>
                     <li class="m-menu__item m-menu__item--submenu parent" id="{{$ind_id}}" aria-haspopup="true" m-menu-submenu-toggle="hover">
                        <a href="javascript:;" class="m-menu__link m-menu__toggle"><i class="m-menu__link-icon {{$icon}}"></i><span class="m-menu__link-text">{{ $permission->name }}</span><i class="m-menu__ver-arrow la la-angle-right"></i></a>
                        <div class="m-menu__submenu " m-hidden-height="80" style="display: none; overflow: hidden;">
                           <span class="m-menu__arrow"></span>
                           <ul class="m-menu__subnav">
                              <?php foreach ($sub_permissions as $sub_permission) {
                                 if(auth()->user()->can($sub_permission->name)){ 
                                 
                                   $sub_ind_id = preg_replace('/\s+/', '_', $sub_permission->name);
                                 
                                   ?>
                              <li class="m-menu__item sub" id="{{$sub_ind_id}}" aria-haspopup="true"><a href="{{ url($sub_permission->url) }}" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text">{{ $sub_permission->name }}</span></a></li>
                              <?php  }} ?>
                           </ul>
                        </div>
                     </li>
                     <?php }else{?>
                     <li class="m-menu__item parent" id="{{$ind_id}}" aria-haspopup="true"><a href="{{ url($permission->url) }}" class="m-menu__link "><i class="m-menu__link-icon {{$icon}}"></i><span class="m-menu__link-text">{{ $permission->name }}</span></a></li>
                     <?php }
                        }
                        } 
                        } ?>
                  </ul>
               </div>
            </div>
            @yield('content')
         </div>
         <footer class="m-grid__item   m-footer ">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
               <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                  <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                     <span class="m-footer__copyright">
                     ©{{ date('Y') }} All Rights Reserved.  <a href="{{ url('/') }}">{{ config('app.name', 'The List Are You On It') }}!</a>
                     </span>
                  </div>
               </div>
            </div>
         </footer>
      </div>
      <div id="m_scroll_top" class="m-scroll-top">
         <i class="la la-arrow-up"></i>
      </div>
      <script src="{{ asset('public/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/demo/default/custom/crud/datatables/basic/paginations.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
      @yield('script')
   </body>
</html>