<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8" />
      <title>{{ config('app.name','AfterProperties') }} | @yield('title') </title>
      <meta name="description" content="Latest updates and statistic charts">
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
      <!--begin::Web font -->
      <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
      <script>
         WebFont.load({
               google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
               active: function() {
                   sessionStorage.fonts = true;
               }
             });
           
      </script>
      <!--end::Web font -->
      <!--begin::Global Theme Styles -->
      <link href="{{ asset('public/assets/vendors/base/vendors.bundle.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="{{ asset('public/assets/vendors/base/vendors.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />-->
      <link href="{{ asset('public/assets/demo/default/base/style.bundle.css') }}" rel="stylesheet" type="text/css" />
      <link href="{{ asset('public/css/main.css') }}" rel="stylesheet" type="text/css" />
      <!--RTL version:<link href="{{ asset('public/assets/demo/default/base/style.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />-->
      <!--end::Global Theme Styles -->
      <link rel="shortcut icon" href="{{ asset('public/images/list-logo.png') }}" />
      <style>
         .error{
         color: red !important;;
         }
      </style>
   </head>
   <!-- end::Head -->
   <!-- begin::Body -->
   <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

      @yield('content')
      <!--begin::Global Theme Bundle -->
      <script src="{{ asset('public/assets/vendors/base/vendors.bundle.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/assets/demo/default/base/scripts.bundle.js') }}" type="text/javascript"></script>
      <script src="{{ asset('public/js/jquery.min.js') }}"></script>
      <script src="{{ asset('public/js/jquery.validate.min.js') }}"></script>
      @yield('javascript')
   </body>
</html>