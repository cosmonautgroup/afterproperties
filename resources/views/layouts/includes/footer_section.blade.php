<footer>
   <div class="main-footer">
      <div class="container">
         <div class="footer-imgs-section">
            <div class="row">
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                  <a href="javascript:void(0)" class="footer-img">
                  <img src="{{url('/public')}}/front_assets/images/LT.jpg">
                  </a>
               </div>
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                  <a href="javascript:void(0)" class="footer-img">
                  <img src="{{url('/public')}}/front_assets/images/LT.jpg">
                  </a>
               </div>
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                  <a href="javascript:void(0)" class="footer-img">
                  <img src="{{url('/public')}}/front_assets/images/LT.jpg">
                  </a>
               </div>
               <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3">
                  <a href="javascript:void(0)" class="footer-img">
                  <img src="{{url('/public')}}/front_assets/images/ics.jpg">
                  </a>
               </div>
            </div>
         </div>
      </div>
      <div class="footer-signup">
         <div class="footer-signup-title">
            <h3>Sign Up For Our Newsletter</h3>
         </div>
         <form>
            <ul>
               <li>
                  <label>
                  Email
                  </label>
                  <input type="email" name="email-name" placeholder="JohnDoe@example.com">
               </li>
               <li>
                  <label>
                  First Name
                  </label>
                  <input type="text" name="first-name" placeholder="First name">
               </li>
               <li>
                  <label>
                  Last Name
                  </label>
                  <input type="text" name="last-name" placeholder="Last name">
               </li>
               <li>
                  <div class="subscribe-input">
                     <input type="submit" class="subscribe-btn" value="Subscribe">
                  </div>
               </li>
            </ul>
         </form>
      </div>
      <div class="footer-copyright">
         <div class="copyright-text">
            COPYRIGHT THE LIST ARE YOU ON IT © 2019. POWERED BY <a href="javascript:void(0)">UVISION SOFTWARE</a>
         </div>
         <div class="bottom-to-top">
            <a href="javascript:void(0)">
            <i class="fa fa-angle-up cb-circle"></i>
            </a>
         </div>
      </div>
   </div>
</footer>