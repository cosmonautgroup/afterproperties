<head>
   <meta charset="UTF-8"/>
   <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
   <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"/>
   <title>{{ config('app.name', 'Afterproperties') }} |  @yield('title')</title>
   <meta name="keywords" content=""/>
   <meta name="description" content=""/>
   <meta name="author" content=""/>
   <meta name="csrf-token" content="">
   <!-- OG Meta S -->
   <meta property="og:url" content=""/>
   <meta property="og:type" content="" />
   <meta property="og:title" content=""/>
   <meta property="og:description" content=""/>
   <meta property="og:image" content=""/>
   <!-- OG Meta E -->
   <!-- Twitter Meta S -->
   <meta name="twitter:card" content=""/>
   <meta name="twitter:title" content=""/>
   <meta name="twitter:url" content=""/>
   <meta name="twitter:description" content=""/>
   <meta name="twitter:image" content=""/>
   <!-- Twitter Meta E -->
   <!-- Favicon Icon S -->
   <!-- <link rel="icon" href="{{url('/public')}}/front_assets/images/favicon.ico" type="image/x-icon"/>
      <link rel="apple-touch-icon" sizes="144x144" href="{{url('/public')}}/front_assets/images/apple-touch-icon-144.png"/>
      <link rel="apple-touch-icon" sizes="114x114" href="{{url('/public')}}/front_assets/images/apple-touch-icon-114.png"/>
      <link rel="apple-touch-icon" sizes="72x72" href="{{url('/public')}}/front_assets/images/apple-touch-icon-72.png"/>
      <link rel="apple-touch-icon" sizes="57x57" href="{{url('/public')}}/front_assets/images/apple-touch-icon-57.png"/> -->
   <!-- Favicon Icon E -->
   <!-- Canonical Link S -->
   <link rel="canonical" href="{{URL::current()}}"/>
   <!-- Canonical Link E -->
   <!-- Style Sheet Link S -->
   <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800|Open+Sans:300,400,600,700&display=swap" rel="stylesheet">
   <link rel="stylesheet" href="{{url('/public')}}/front_assets/css/main.css"/>
   <!-- Style Sheet Link E -->
   <link rel="shortcut icon" href="{{url('public/images/list-logo.png')}}" />
   <!-- Java Script Link S -->

   <?php
      $current_url = URL::current();
      $found = 0;
      if(strpos($current_url, "index.php") !== false){
          $found = 1;
      }
      $redirect_url = url('/');
      $redirect_url = str_replace('index.php', '', $redirect_url)

   ?>
   <script type="text/javascript">
      var found = "{{$found}}";
      var redirect_url = "{{ $redirect_url }}";
      if(found == 1){
         window.location.href = redirect_url;
      }
      
   </script>

   <script type="text/javascript" src="{{url('/public')}}/front_assets/libraries/jquery-3.2.1/js/jquery-3.2.1.min.js"></script>
   <!-- Java Script Link E -->
   
   @yield('css')

</head>