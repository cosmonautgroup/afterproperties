<header>
   <div class="main-top-menu">
      <div class="left-side-menu">
         <a href="javascript:void(0)" onclick="openMenu()">
         <i class="fa fa-bars"></i>
         </a>
      </div>
      <div class="right-side">
         <ul>
            <li>
               <a href="javascript:void(0)" onclick="openSearchBar()">
               <i class="fa fa-search"></i>
               </a>
               <a href="javascript:void(0)" onclick="openSignup()">
               <i class="fa fa-user"></i>
               </a>
            </li>
         </ul>
      </div>
   </div>
   <div class="main-header-banner">
      <div class="container logo-box">
         <div class="logo">
            <a href="javascript:void(0)">
            <img src="{{url('/public')}}/front_assets/images/logo-1-1.png" alt="logo">
            </a>
         </div>
      </div>
   </div>
   <nav>
      <div class="container">
         <ul class="exo-menu">
            <li>
               <a class="menu-item" href="index.php">
               Home
               </a>
            </li>
            <li class="mega-drop-down">
               <a href="calendar.php" class="menu-item">
               Event
               </a>
               <div class="animated fadeIn mega-menu">
                  <div class="mega-menu-wrap">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 text-left">
                           <h4>
                              Events
                           </h4>
                        </div>
                        <div class="col-md-6 col-sm-6 text-right d-flex align-items-center justify-content-end">
                           <a href="#" class="see-all">
                           see all
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              <a href="trick-track.php">Trick or Track</a>
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/compund.jpg">
                           <h2>
                              <a href="javascript:void(0)">SustainabiliTEA</a>
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              <a href="javascript:void(0)">Throwback Thursdays At The Graham Rooftop Kickoff</a>
                           </h2>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <li class="mega-drop-down">
               <a href="coming-soon.php" class="menu-item">
               Coming Soon
               </a>
               <div class="animated fadeIn mega-menu">
                  <div class="mega-menu-wrap">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 text-left">
                           <h4>
                              Events
                           </h4>
                        </div>
                        <div class="col-md-6 col-sm-6 text-right d-flex align-items-center justify-content-end">
                           <a href="#" class="see-all">
                           see all
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              Trick or Track
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/compund.jpg">
                           <h2>
                              SustainabiliTEA
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              Throwback Thursdays At The Graham Rooftop Kickoff
                           </h2>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <li class="mega-drop-down">
               <a href="openings.php" class="menu-item">
               Opening
               </a>
               <div class="animated fadeIn mega-menu">
                  <div class="mega-menu-wrap">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 text-left">
                           <h4>
                              Events
                           </h4>
                        </div>
                        <div class="col-md-6 col-sm-6 text-right d-flex align-items-center justify-content-end">
                           <a href="#" class="see-all">
                           see all
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              Trick or Track
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/compund.jpg">
                           <h2>
                              SustainabiliTEA
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              Throwback Thursdays At The Graham Rooftop Kickoff
                           </h2>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <li class="mega-drop-down">
               <a href="wtop.php" class="menu-item">
               Wtop
               </a>
               <div class="animated fadeIn mega-menu">
                  <div class="mega-menu-wrap">
                     <div class="row">
                        <div class="col-md-6 col-sm-6 text-left">
                           <h4>
                              Events
                           </h4>
                        </div>
                        <div class="col-md-6 col-sm-6 text-right d-flex align-items-center justify-content-end">
                           <a href="#" class="see-all">
                           see all
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              <a href="nycci-tells-us-where-to-wine-dine-and-celebrate-the-4th-of-july.php">NYCCI TELLS US WHERE TO WINE, DINE, AND CELEBRATE THE 4TH OF JULY</a>
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/compund.jpg">
                           <h2>
                              SustainabiliTEA
                           </h2>
                        </div>
                        <div class="col-md-4 col-sm-4 text-center">
                           <img class="img-responsive" src="{{url('/public')}}/front_assets/images/natural.jpg">
                           <h2>
                              Throwback Thursdays At The Graham Rooftop Kickoff
                           </h2>
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <li>
               <a href="buzz.php" class="menu-item">
               Buzz
               </a>
            </li>
            <li>
               <a href="mirebox.php" class="menu-item">
               Mirebox
               </a>
            </li>
            <li class="drop-down">
               <a href="#" class="menu-item">
               Radio & Podcast
               </a>
               <!--Drop Down-->
               <ul class="drop-down-ul animated fadeIn">
                  <li class="flyout-right">
                     <a href="#">Flyout Right</a><!--Flyout Right-->
                  </li>
                  <li class="flyout-left">
                     <a href="#">Flyout Left</a><!--Flyout Left-->
                  </li>
                  <!-- <li><a href="#">No Flyout</a></li> -->
               </ul>
               <!--//End drop down-->
            </li>
            <li>
               <a href="#" class="menu-item">
               <i class="fa fa-user"></i>
               Newsletter Signup
               </a>
            </li>
            <li>
               <a href="#" class="menu-item">
               <i class="fa fa-search"></i>
               </a>
            </li>
         </ul>
      </div>
   </nav>
</header>
<div id="MenuBar" class="overlay" >
   <a href="javascript:void(0)" class="closebtn" onclick="closeMenu()">&times;</a>
   <div class="overlay-content">
      <ul>
         <li>
            <a href="#">Home</a>
         </li>
         <li>
            <a href="#">Events</a>  
         </li>
         <li>
            <a href="#">Coming Soon</a>
         </li>
         <li>
            <a href="#">WTOP</a>
         </li>
         <li>
            <a href="#">Buzz</a>
         </li>
         <li>
            <a href="#">Mirepoix</a>    
         </li>
         <li>
            <a href="#">Radio & Podcast</a>
         </li>
      </ul>
   </div>
</div>
<div id="SearchBar" class="overlay">
   <div class="main-searchbar">
      <a href="javascript:void(0)" class="closebtn" onclick="closeSearchBar()">&times;</a>
      <div class="search-modal-inner">
         <form>
            <input type="text" name="" placeholder="Search...">
         </form>
      </div>
   </div>
</div>
<div id="SignupModal" class="overlay">
   <div class="main-signup">
      <a href="javascript:void(0)" class="closebtn" onclick="closeSignup()">&times;</a>
      <div class="signup-modal-inner">
         <div class="signup-login-title">
            <a href="javascript:void(0)" class="tab-links login-title" onclick="openSignupLogin(event, 'login-form')" id="defaultOpen">Log In</a>
            <a href="javascript:void(0)" class="tab-links register-title" onclick="openSignupLogin(event, 'register-form')">Register</a>
         </div>
         <form class="login-form signuptabcontent" id="login-form">
            <div class="form-input">
               <input type="text" placeholder="Username" class="login-username">
               <input type="password" placeholder="Password" class="login-signup-password">
            </div>
            <div class="recaptcha-text">
               Could not connect to the reCAPTCHA service. Please check your internet connection and reload to get a reCAPTCHA challenge.
            </div>
            <div class="submit-btn-main">
               <input type="submit" name="login-submit" class="submit-btn-form" value="Log in">
            </div>
            <div class="remember-me">
               <label>Remember Me</label>
               <input type="checkbox" name="login-checkbox" class="login-checkbox">
            </div>
            <div class="lost-password">
               <a href="javascript:void(0)">Lost your password?</a>
            </div>
         </form>
         <form class="register-form signuptabcontent" id="register-form">
            <div class="form-input">
               <input type="text" placeholder="Username" class="register-username">
               <input type="email" placeholder="E-mail" class="register-email">
            </div>
            <div class="recaptcha-text">
               Could not connect to the reCAPTCHA service. Please check your internet connection and reload to get a reCAPTCHA challenge.
            </div>
            <div class="submit-btn-main">
               <input type="submit" name="login-submit" class="submit-btn-form" value="Log in">
            </div>
            <div class="mail-password-text">
               <label>A password will be e-mailed to you.</label>
            </div>
         </form>
      </div>
   </div>
</div>