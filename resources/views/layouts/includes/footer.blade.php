<!-- Java Script S -->
<script src="{{url('/public')}}/front_assets/libraries/bootstrap-4.0.0/js/popper.min.js"></script>
<script src="{{url('/public')}}/front_assets/libraries/bootstrap-4.0.0/js/bootstrap.min.js"></script>
<script src="{{url('/public')}}/front_assets/libraries/OwlCarousel2-2.2.1/js/owl.carousel.min.js"></script>  
<script src="{{url('/public')}}/front_assets/libraries/back-top/js/back-top.js"></script>
<script src="{{url('/public')}}/front_assets/libraries/wow/js/wow.min.js"></script>
<script src="{{url('/public')}}/front_assets/js/custom.js"></script>
<!-- Java Script E -->
<script>
  
function openMenu() {
    document.getElementById("MenuBar").style.width = "100%";
}

function closeMenu() {
    document.getElementById("MenuBar").style.width = "0%";
}

function openSearchBar() {
    document.getElementById("SearchBar").style.height = "100%";
}

function closeSearchBar() {
    document.getElementById("SearchBar").style.height = "0%";
}

function openSignup() {
    document.getElementById("SignupModal").style.height = "100%";
}

function closeSignup() {
    document.getElementById("SignupModal").style.height = "0%";
}

function openSignupLogin(evt, calName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("signuptabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(calName).style.display = "block";
    evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

$('body').on('click', '#defaultOpen', function() {
    $(this).addClass('active');
    $('.register-title').removeClass('active');

});

$('body').on('click', '.register-title', function() {
    $(this).addClass('active');
    $('#defaultOpen').removeClass('active');

});


   
</script>

@yield('javascript')