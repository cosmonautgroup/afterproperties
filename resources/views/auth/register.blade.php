@extends('layouts.login_app')
@section('title')
SignUp
@endsection
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
   <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
      <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside m-auto">
         <div class="m-stack m-stack--hor m-stack--desktop">
            <div class="m-stack__item m-stack__item--fluid">
               <div class="m-login__wrapper">
                  <div class="m-login__logo">
                     <a href="{{ url('/') }}">
                     <img src="{{ asset('public/images/list-logo.png') }}">
                     </a>
                  </div>
                  <div class="m-login__signin">
                     <div class="m-login__head">
                        <h3 class="m-login__title">Sign Up</h3>
                        <div class="m-login__desc">Enter your details to create your account:</div>
                     </div>
                     @if (session('error'))
                     <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                     </div>
                     @endif
                     @if (session('success'))
                     <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                     </div>
                     @endif
                     <form class="m-login__form m-form" method="POST" action="{{ route('register') }}" id="frmSignup">
                        @csrf
                        <div class="form-group m-form__group">
                           <input id="first_name" type="text" placeholder="First Name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }} m-input" name="first_name" value="{{ old('first_name') }}" required autofocus/>
                           @if ($errors->has('first_name'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('first_name') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group">
                           <input id="last_name" type="text" placeholder="Last Name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }} m-input" name="last_name" value="{{ old('last_name') }}" required autofocus/>
                           @if ($errors->has('last_name'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('last_name') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group">
                           <input id="email" type="text" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" required autofocus/>
                           @if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('email') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group">
                           <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('password') }}" required autofocus/>
                           @if ($errors->has('password'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group">
                           <input id="password_confirmation" type="password" placeholder="Confirm Password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" value="{{ old('password_confirmation') }}" required autofocus/>
                           @if ($errors->has('password_confirmation'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password_confirmation') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group mar-top-15 i-agree-term-height">
                           <input id="tc" name="tc" type="checkbox"/> I agree to the Term of Use and Privacy Policy
                           @if ($errors->has('tc'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('tc') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="m-login__form-action">
                           <button type="submit" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign Up</button>
                           <a href="{{ route('login') }}" id="m_login_forget_password" class="m-link">Sign In</a>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
   $(document).ready(function(){
       $("#frmSignup").validate({
       rules: {
         first_name: {
           required: true,
           lettersonly: true
         },
         last_name: {
           required: true,
           lettersonly: true
         },
         password: {
           required: true,
           minlength: 6
         },
         password_confirmation: {
           required: true,
           equalTo: "#password"
         },
         email: {
           required: true,
           email: true
         },
         tc:{
           required: true
         }
       },
         messages:{
           first_name: {
               lettersonly: "please enter characters only",
           },
           last_name: {
               lettersonly: "please enter characters only",
           },
           tc: {
               required : "Please accept term and conditions"
           },
         }
     });
   
     jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/.test(value);
     });
   });
</script>
@endsection