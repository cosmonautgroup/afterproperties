@extends('layouts.login_app')
@section('title')
Reset Password
@endsection
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page auther-sign-page">
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
   <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
      <div class="m-stack m-stack--hor m-stack--desktop">
         <div class="m-stack__item m-stack__item--fluid" style="display: table-cell; vertical-align: middle;">
            <div class="m-login__wrapper">
               <div class="m-login__logo">
                  <a href="{{ url('admin/dashboard') }}">
                  <img src="{{ asset('public/images/list-logo.png') }}">
                  </a>
               </div>
               <div class="m-login__signin">
                  <div class="m-login__head">
                     <h3 class="m-login__title">Reset Password</h3>
                  </div>
               </div>
               @if (session('error'))
               <div class="alert alert-danger" role="alert">
                  {{ session('error') }}
               </div>
               @endif
               @if (session('success'))
               <div class="alert alert-success" role="alert">
                  {{ session('success') }}
               </div>
               @endif
               <form class="m-login__form m-form" method="POST" action="{{ route('resetPassword') }}" id="frmReset">
                  @csrf
                  <div class="form-group m-form__group" style="height: auto;">
                     <div class="form-group m-form__group">
                        <input id="old_password" type="password" placeholder="Old Password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }} m-input" name="old_password" required/>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group m-form__group">
                        <input id="password" type="password" placeholder="New Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} m-input" name="password" required/>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                     </div>
                     <div class="form-group m-form__group">
                        <input id="password_confirmation" type="password" placeholder="Confirm Password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }} m-input" name="password_confirmation" required />
                        @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                     </div>
                     <input id="user_id" type="hidden" class="form-control" name="user_id" value="{{ isset($id)?$id:'' }}" required />
                     <div class="m-login__form-action">
                        <button type="submit" id="m_login_signup_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">{{ __('Reset Password') }}</button>
                        <a href="{{url('')}}" class="m-link">Login</a>
                     </div>
               </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
   $(document).ready(function(){
       $("#frmReset").validate({
       rules: {
         password: {
           required: true,
           minlength: 6
         },
         password_confirmation: {
           required: true,
           equalTo: "#password"
         },
         old_password: {
           required: true
         }
       }
     });
   });
</script>
@endsection