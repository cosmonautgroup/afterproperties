@extends('layouts.login_app')
@section('title')
Login
@endsection
@section('content')
<div class="m-grid m-grid--hor m-grid--root m-page">
   <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
      <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside m-auto">
         <div class="m-stack m-stack--hor m-stack--desktop">
            <div class="m-stack__item m-stack__item--fluid">
               <div class="m-login__wrapper">
                  <div class="m-login__logo" style="padding-bottom: 40px;">
                     <a href="{{ url('/') }}">
                     <img src="{{ url('public/images/list-logo.png') }}">
                     </a>
                  </div>
                  <div class="m-login__signin">
                     <div class="m-login__head">
                        <h3 class="m-login__title">Sign In</h3>
                     </div>
                     @if (session('error'))
                     <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                     </div>
                     @endif
                     @if (session('success'))
                     <div class="alert alert-success" role="alert">
                        {{ session('success') }}
                     </div>
                     @endif
                     <form class="m-login__form m-form" method="POST" action="{{ route('login') }}" id="frmLogin" autocomplete="off">
                        @csrf
                        <div class="form-group m-form__group">
                           <input id="email" type="text" placeholder="Email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} m-input" name="email" value="{{ old('email') }}" required autofocus/>
                           @if ($errors->has('email'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('email') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="form-group m-form__group">
                           <input id="password" type="password" placeholder="Password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="{{ old('email') }}" required autofocus/>
                           @if ($errors->has('password'))
                           <span class="invalid-feedback" role="alert">
                           <strong>{{ $errors->first('password') }}</strong>
                           </span>
                           @endif
                        </div>
                        <div class="row m-login__form-sub">
                           <div class="col m--align-left">
                              <label class="m-checkbox m-checkbox--focus">
                              <input type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }} > Remember me
                              <span></span>
                              </label>
                           </div>
                        </div>
                        <div class="row m-login__form-sub">
                           <div class="col">
                              <a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link">Forget Password ?</a>
                           </div>
                        </div>
                        <div class="m-login__form-action">
                           <button type="submit" id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">Sign In</button>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('javascript')
<script>
   $(document).ready(function(){
       $("#frmLogin").validate({
       rules: {
         password: {
           required: true
         },
         email: {
           required: true,
           emailExt: true
         }
       }
     });
       jQuery.validator.addMethod("emailExt", function(value, element, param) {
         return value.match(/^[a-zA-Z0-9_\.%\+\-]+@[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,}$/);
     },'Enter a valid email address');
   });
</script>
@endsection