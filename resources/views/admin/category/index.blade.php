@extends('layouts.app')
@section('title')
Category
@endsection
@section('content')
<style type="text/css">
   tfoot {
   display: table-header-group !important;
   }
</style>
<!-- page content -->
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Category</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Category</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="m-portlet m-portlet--mobile">
         <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
               <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                     Category
                  </h3>
               </div>
            </div>
            <div class="m-portlet__head-tools">
               <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                     <a href="{{ route('add-category') }}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                     <span>
                     <i class="la la-plus"></i>
                     <span>Add Category</span>
                     </span>
                     </a>
                  </li>
                  <li class="m-portlet__nav-item"></li>
               </ul>
            </div>
         </div>
         <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="datatable">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Category Name</th>
                     <th>Menu</th>
                     <th>Status</th>
                     <th width="200px">Action</th>
                  </tr>
               </thead>
               <tfoot>
                  <tr>
                     <th><input type="text" class="form-control form-control-sm"></th>
                     <th><input type="text" class="form-control form-control-sm"></th>
                     <th><input type="text" class="form-control form-control-sm"></th>
                     <th><input type="text" class="form-control form-control-sm"></th>
                     <th></th>
                  </tr>
               </tfoot>
            </table>
         </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
   </div>
</div>
@endsection
@section('script')
<script>
   $(function() {
     var table = $('#datatable').DataTable({
       processing: true,
       serverSide: true,
       ajax: {
           "url" : "{{ route('get-category') }}",
           "type": "POST",
           "data" : {
            "_token": "{{ csrf_token() }}"
           }
       },
       "lengthMenu": [
             [10, 15, 20, 50, 100, -1],
             [10, 15, 20, 50, 100, "All"]
         ],
       columns:[
                 { data: 'id', name: 'id' },
                 { data: 'category_name', name: 'category_name' },
                 { data: 'is_menu', name: 'is_menu' },
                 { data: 'is_active', name: 'is_active' },
                 { data: 'action', name: 'action' },
               ]
     });
     table.columns().every( function () {
       var that = this;
       $('input', this.footer()).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that.search(this.value).draw();
           }
       });
     });
   });
</script>
@endsection