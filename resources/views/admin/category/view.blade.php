@extends('layouts.app')
@section('title')
View Category
@endsection
@section('content')
<!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Category</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                  <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                      <i class="m-nav__link-icon la la-home"></i>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                      <span class="m-nav__link-text">View Category</span>
                    </a>
                  </li>
                   <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                      <span class="m-nav__link-text">{{$category->category_name}}</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div>
              </div>
            </div>
          </div>

          <!-- END: Subheader -->
          <div class="m-content">
            @if (session('error'))
                  <div class="alert alert-danger" role="alert">
                      {{ session('error') }}
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session('success') }}
                  </div>
              @endif
            <div class="row">
              <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                          <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                          View Category
                        </h3>
                      </div>
                    </div>
                  </div>

                  <!--begin::Form-->
                  <form class="m-form">
                    <div class="m-portlet__body">
                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Category Name:</label>
                          <div class="col-lg-3">
                            {{ $category->category_name }}
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                  <!--end::Form-->
                </div>

                <!--end::Portlet-->
              </div>
            </div>
          </div>
        </div>

@endsection
