@extends('layouts.app')
@section('title')
Add Category
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Category</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Category</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Category
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-category') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Category Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Category Name" name="category_name" maxlength="20" id="category_name" value="{{ old('category_name') }}">
                              @if ($errors->has('category_name'))
                              <span class="help-block">{{ $errors->first('category_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Menu:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_menu" id="is_menu" value="1" @if(old('is_menu') == 1) checked="checked" @endif>
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Active:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_active" id="is_active" value="1" @if(old('is_active') == 1) checked="checked" @endif> 
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('category') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script>
   $(document).ready(function(){
     $("#frmUser").validate({
             rules: {
               category_name: {
                 required: true,
               }
             },
             messages:{
               category_name: {
                   required: "please Enter Your Category Name"
               }
             },
             highlight: function(e) {
                 $(e).closest(".form-label-group").addClass("has-error")
             },
             success: function(e) {
                 e.closest(".form-label-group").removeClass("has-error"), e.remove()
             },
             submitHandler: function(e) {
                 e.submit()
             }
       });
     });
</script>
@endsection