@extends('layouts.app')
@section('title')
Edit CMS Page
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">CMS Page</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{route('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="{{route('cms-page')}}" class="m-nav__link">
                  <span class="m-nav__link-text">CMS Page</span>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Edit CMS Page</span>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">{{$cms->page_title}}</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Edit CMS Page
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="submit_form" method="POST" action="{{ route('update-cms') }}" >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Page Title:<span style="color: red">*</span></label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Page Title" name="page_title" id="page_title" value="{{ $cms->page_title }}">
                              @if ($errors->has('page_title'))
                              <span class="help-block">{{ $errors->first('page_title') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Page Slug:<span style="color: red">*</span></label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Page Slug" name="page_slug" id="page_slug" value="{{ $cms->page_slug }}" readonly="readonly">
                              @if ($errors->has('page_slug'))
                              <span class="help-block">{{ $errors->first('page_slug') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Page Content:<span style="color: red">*</span> </label>
                           <div class="col-lg-8">
                              <textarea name="page_content" class="form-control" rows="10" cols="20" placeholder="Page Content">{{ $cms->page_content }}</textarea>
                              @if ($errors->has('page_content'))
                              <span class="help-block">{{ $errors->first('page_content') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Meta Keywords:</label>
                           <div class="col-lg-6">
                              <textarea name="meta_keywords" class="form-control" rows="5" placeholder="Meta Keywords">{{ $cms->meta_keywords }}</textarea>
                              @if ($errors->has('meta_keywords'))
                              <span class="help-block">{{ $errors->first('meta_keywords') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Meta Description:</label>
                           <div class="col-lg-6">
                              <textarea name="meta_description" class="form-control" rows="5" placeholder="Meta Description">{{ $cms->meta_description }}</textarea>
                              @if ($errors->has('meta_description'))
                              <span class="help-block">{{ $errors->first('meta_description') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Active:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_active" id="is_active" value="1" @if (isset($cms->is_active) && $cms->is_active==1) checked="checked" @endif> 
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <input type="hidden" class="form-control" id="id" name="id" value="{{ $cms->id }}" >
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a href="{{ route('cms-page') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
<script>
   $(document).ready(function(){
     CKEDITOR.replace('page_content');
     /*Create Page Slug using Page Title*/
     $('#page_title').on('blur',function(){
       var title = $('#page_title').val();
       var slug = title.replace(" ","_");
       slug = slug.toLowerCase();
       $('#page_slug').val(slug);
     })
     /*Create Page Slug using Page Title*/
     $("#submit_form").validate({
       ignore: [],
         debug: false,
         rules: {
             page_title: {
               required: true,
             },
             page_slug: {
               required: true,
             },
             page_content: {
               required:true,
             }
           },
           messages: {
             page_title:{
               required: "Please enter your page title",
             },
             page_slug:{
               required: "Please enter your page slug",
             },
             page_content:{
               required: "Please enter your page content",
             },
         },
           highlight: function(e) {
               $(e).closest(".form-label-group").addClass("has-error")
           },
           success: function(e) {
               e.closest(".form-label-group").removeClass("has-error"), e.remove()
           },
           submitHandler: function(e) {
               e.submit()
           }
       });
       jQuery.validator.addMethod("lettersonly", function(value, element) {
          return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/.test(value);
       });
     });
</script>
@endsection