@extends('layouts.app')
@section('title')
Add Maintenance
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Maintenance</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Maintenance</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Maintenance
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-maintenance') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body" id="form_main">
                     <div class="row" >
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;">
                           <label >Society Name</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="socity" name="socity" >
                                 <option value="">Select Society Name</option>
                                    @foreach ($socity as $val)
                                 <option value="{{ $val->id }}">{{ $val->society_name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('socity'))
                              <span class="m-form__help error">
                              <strong>{{ $errors->first('socity') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label >Block Name</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="block" name="block" >
                                 <option value="">Select Block Name</option>
                                    @foreach ($blocks as $val)
                                 <option value="{{ $val->id }}">{{ $val->name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('block'))
                              <span class="m-form__help error">
                              <strong>{{ $errors->first('block') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;">
                           <label>Maintenance Amoount(In Rs)</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter Parking No" name="ammount" maxlength="20" id="ammount" value="{{ old('ammount') }}">
                              @if ($errors->has('ammount'))
                              <span class="help-block">{{ $errors->first('ammount') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label>Payment Type</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter yearly" name="yearly" maxlength="20" id="yearly" value="Yearly">
                              @if ($errors->has('yearly'))
                              <span class="help-block">{{ $errors->first('yearly') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;">
                           <label>Unit Resident Type<span class="error">*</span></label>
                           <div class="input-class">
                              <select  name="unit_type" class="form-control" autocomplete="off" id="unit_type" >
                                 <option value="{{ old('unit_type') }}">Select Your Resident Type</option>
                                 <option>Rentals</option>
                                 <option>Owner</option>
                              </select>
                              @if ($errors->has('unit_type'))
                              <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('unit_type') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label>Payment Cycle</label>
                           <div class="input-class">
                              <select  name="payment_cycle" class="form-control" autocomplete="off" id="payment_cycle" >
                                 <option value="{{ old('payment_cycle') }}">Select Payment Cycle</option>
                                 <option>First Month Of Year</option>
                                 <option>Last Month Of Year</option>
                                 <option>Monthly</option>
                              </select>
                              @if ($errors->has('payment_cycle'))
                              <span class="m-form__help error">{{ $errors->first('payment_cycle') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('maintenance') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
    $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
            socity: {
              required: true,
            },
              block: {
                required: true,
              },
              ammount: {
                required: true,
              },
              unit_type: {
                required: true,
              },
              unit: {
                required: true,
              },
              payment_cycle: {
                required: true,
              },
            },
          messages: {
            socity: {
              required: "Please select your socity",
            },
              block:{
                required: "Please select your block name",
              },
              ammount: {
                required: "Please enter your ammount",
              },
              unit_type:{
                required: "Please select your unit resident type",
              },
              unit: {
                required: "Please select your category",
              },
             payment_cycle: {
                required: "Please select your payment cycle",
             },
          },
        });
   });
</script>
<script>
   $(document).ready(function() {
   /*Start block*/
   $('#socity').change(function(){
   var socityId = $(this).val();
   if(socityId)
   {
       $.ajax({
       type:"GET",
       url:"{{url('admin/maintenance/block')}}?socityid="+socityId,
       success:function(res){
         if(res){
           $("#block").empty();
           $("#block").append('<option>Select Block</option>');
           $.each(res,function(key,value){
           $("#block").append('<option value="'+key+'">'+value+'</option>');
           });
         }
         else{
             $("#block").empty();
         }
       }
     });
   }
   else
   {
       $("#block").empty();
   }
   });
   /*End block*/
   });
</script>
@endsection