@extends('layouts.app')
@section('title')
Add Event
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Event</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Event</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Event
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-event') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3">Event Image</label>
                           <div class="col-lg-6">
                              <input type="file" class="m-input form-control" accept="image/*" name="image" id="image">
                              @if ($errors->has('image'))
                              <span class="help-block">{{ $errors->first('image') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Event Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Name Name" name="name" maxlength="20" id="name" value="{{ old('name') }}">
                              @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Event Details:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Event Details" name="detail" maxlength="20" id="detail" value="{{ old('detail') }}">
                              @if ($errors->has('detail'))
                              <span class="help-block">{{ $errors->first('detail') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Venue Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Venue Name" name="venue_name" maxlength="20" id="venue_name" value="{{ old('venue_name') }}">
                              @if ($errors->has('venue_name'))
                              <span class="help-block">{{ $errors->first('venue_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Address:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Address" name="address" maxlength="20" id="address" value="{{ old('address') }}">
                              @if ($errors->has('address'))
                              <span class="help-block">{{ $errors->first('address') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;margin-right: -31px;margin-left: 14px;">
                           <label>Event Date</label>
                           <div class="input-class" style="margin-right: 48px;">
                              <input type="text" class="form-control m-input date" data-provide="datepicker" placeholder="Enter Event Date" name="date" maxlength="20" id="date" value="{{ old('date') }}">
                              @if ($errors->has('date'))
                              <span class="m-form__help error">{{ $errors->first('date') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label>Event End Date</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input date" data-provide="datepicker" placeholder="Enter Event End Date" name="end_date" maxlength="20" id="end_date" value="{{ old('end_date') }}">
                              @if ($errors->has('end_date'))
                              <span class="m-form__help error">{{ $errors->first('end_date') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;margin-right: -31px;margin-left: 14px;">
                           <label">Event Time</label>
                           <div class="input-class" style="margin-right: 48px;">
                              <input type="text" class="form-control m-input timepicker" placeholder="Enter Event Time" name="time" maxlength="20" id="time" value="{{ old('time') }}">
                              @if ($errors->has('time'))
                              <span class="m-form__help error">{{ $errors->first('time') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label>Event End Time</label>
                           <div class="input-class" style="margin-right: 48px;">
                              <input type="text" class="form-control m-input timepicker" placeholder="Enter Event End Time" name="end_time" maxlength="20" id="end_time" value="{{ old('end_time') }}">
                              @if ($errors->has('end_time'))
                              <span class="m-form__help error">{{ $errors->first('end_time') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Event Rules:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Event Rules" name="rules" maxlength="20" id="rules" value="{{ old('rules') }}">
                              @if ($errors->has('rules'))
                              <span class="help-block">{{ $errors->first('rules') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group">
                           <label style="margin-left: 12px;">Event Cost And Tickets:</label>
                        </div>
                        <div class="form-group m-form__group" style="margin-top: -13px;margin-left: 95px;">
                           <input type="radio" name="radio" id="no_ticket" value="No Ticket">
                           <lable>No Tickets</lable>
                           <input type="radio" name="radio" id="timely_ticket" value="Timely Ticket">
                           <lable>Timely Tickets</lable>
                           <input type="radio" name="radio" id="external_ticket" value="External Ticket">
                           <lable>External Tickets</lable>
                        </div>
                     </div>
                     <div class="form-group m-form__group row" style="margin-top: -5px;margin-right: -83px;">
                        <label class="col-md-3 form-control-label" style="margin-left: 243px;">Cost</label>
                        <div class="input-class col-md-3" style="margin-top: -6px;">
                           <input type="text" class="form-control m-input" placeholder="Enter Event Cost" name="cost" maxlength="20" id="cost" value="{{ old('cost') }}" style="height: 33px;margin-left: -209px;">
                           @if ($errors->has('cost'))
                           <span class="m-form__help error">{{ $errors->first('cost') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group">
                           <label>Organizer Contect Info:</label>
                        </div>
                        <div class="form-group m-form__group row" style="margin-left: 38px;">
                           <label class="col-md-5 form-control-label" style="margin-top: 12px;">Contact Name:</label>
                           <div class="input-class col-md-7">
                              <input type="text" class="form-control m-input" placeholder="Enter Event Contact Name" name="contact_name" maxlength="20" id="contact_name" value="{{ old('contact_name') }}">
                              @if ($errors->has('contact_name'))
                              <span class="m-form__help error">{{ $errors->first('contact_name') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group row" style="margin-left: 206px;">
                           <label class="col-md-5 form-control-label" style="margin-top: 12px;margin-left: -25px;">Phone No</label>
                           <div class="input-class col-md-7">
                              <input type="text" class="form-control m-input" placeholder="Enter Phone No" name="phone_no" maxlength="20" id="phone_no" value="{{ old('phone_no') }}" style="margin-left: 26px;">
                              @if ($errors->has('phone_no'))
                              <span class="m-form__help error">{{ $errors->first('phone_no') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group row" style="margin-left: 206px;">
                           <label class="col-md-5 form-control-label" style="margin-top: 12px;">Email</label>
                           <div class="input-class col-md-7">
                              <input type="text" class="form-control m-input" placeholder="Enter Email" name="email" maxlength="20" id="email" value="{{ old('email') }}" style="margin-left: 10px;">
                              @if ($errors->has('email'))
                              <span class="m-form__help error">{{ $errors->first('email') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group row" style="margin-left: 206px;">
                           <label class="col-md-5 form-control-label" style="margin-top: 12px;">Website Url</label>
                           <div class="input-class col-md-7">
                              <input type="text" class="form-control m-input" placeholder="Enter Website Url" name="website_url" maxlength="20" id="website_url" value="{{ old('website_url') }}">
                              @if ($errors->has('website_url'))
                              <span class="m-form__help error">{{ $errors->first('website_url') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('event') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   // Class definition
   
   var KTBootstrapTimepicker = function () {
           var demos = function () {
            $('#time').timepicker({
               minuteStep: 1,
               showSeconds: true,
               showMeridian: true
           });
            $('#end_time').timepicker({
               minuteStep: 1,
               showSeconds: true,
               showMeridian: true
           });
       }
       return {
           init: function() {
               demos();
           }
       };
   }();
   jQuery(document).ready(function() {
       KTBootstrapTimepicker.init();
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
          	name: {
          		required: true,
          	},
              detail: {
                required: true,
              },
              venue_name:{
               required:true
              },
              address:{
               required:true
              },
              date: {
              	required: true,
              },
              end_date: {
              	required: true,
              },
              time: {
              	required: true,
              },
              end_time: {
              	required: true,
              },
              rules: {
              	required: true,
              },
            },
          messages: {
          	name: {
          		required: "Please enter your event name",
          	},
              detail:{
                required: "Please enter your event detail",
              },
              venue_name:{
               required: "Please Enter Your Venue Name"
              },
              address:{
               required: "Please Enter Your Address"
              },
              date: {
              	required: "Please select your event start date",
              },
              end_date: {
              	required: "Please select your event end date",
              },
              time:{
              	required: "Please select your event start time",
              },
              end_time:{
              	required: "Please select your event end time",
              },
              rules: {
              	required: "Please enter your event rules",
              },
          },
        });
   });
</script>
@endsection