@extends('layouts.app')
@section('title')
Logs
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
  <div class="m-subheader ">
    <div class="d-flex align-items-center">
      <div class="mr-auto">
        <h3 class="m-subheader__title m-subheader__title--separator">Logs and Error</h3>
        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
          <li class="m-nav__item m-nav__item--home">
            <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
            <i class="m-nav__link-icon la la-home"></i>
            </a>
          </li>
          <li class="m-nav__separator">-</li>
          <li class="m-nav__item">
            <a href="javascript:;" class="m-nav__link">
            <span class="m-nav__link-text">Logs and Error</span>
            </a>
          </li>
        </ul>
      </div>
      <div>
      </div>
    </div>
  </div>
  <!-- END: Subheader -->
  <div class="m-content">
    @if (session('error'))
    <div class="alert alert-danger" role="alert">
      {{ session('error') }}
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success" role="alert">
      {{ session('success') }}
    </div>
    @endif
    <div class="m-portlet m-portlet--mobile">
      <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
          <div class="m-portlet__head-title">
            <h3 class="m-portlet__head-text">
              Logs and Error
            </h3>
          </div>
        </div>
      </div>
      <div class="m-portlet__body">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="logs_datatable">
          <thead>
            <tr>
              <th>#ID</th>
              <th>Date</th>
              <th>Time</th>
              <th>Action</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
  </div>
</div>
@endsection
@section('script')
<script>
  $(function() {
    getDatatable();
    function getDatatable(){
      return  $('#logs_datatable').DataTable({
            processing: true,
            serverSide: true,
            "bDestroy": true,
            ajax: {
                "url" : "{{ route('get-logs') }}",
                "type": "POST",
                "data" : {
                 "_token": "{{ csrf_token() }}"
                }
            },
            "lengthMenu": [
                [10, 15, 20, 50, 100, -1],
                [10, 15, 20, 50, 100, "All"]
            ],
            order : ['0','desc'],
            columns:[
              { data: 'id', name: 'id' },
              { data: 'date', name: 'date' },
              { data: 'time', name: 'time' },
              { data: 'action', name: 'action' },
            ]
        });
      }
  });
</script>
@endsection