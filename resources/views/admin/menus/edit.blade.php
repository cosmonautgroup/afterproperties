@extends('layouts.app')
@section('title')
Edit Menu
@endsection
@section('content')
<!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Menus</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                  <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                      <i class="m-nav__link-icon la la-home"></i>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="{{url('admin/menus')}}" class="m-nav__link">
                      <span class="m-nav__link-text">Menu</span>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                      <span class="m-nav__link-text">Edit Menu</span>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                      <span class="m-nav__link-text">{{$menu->name}}</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div>
              </div>
            </div>
          </div>

          <!-- END: Subheader -->
          <div class="m-content">
            @if (session('error'))
                  <div class="alert alert-danger" role="alert">
                      {{ session('error') }}
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session('success') }}
                  </div>
              @endif
            <div class="row">
              <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                          <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                          Edit Menu
                        </h3>
                      </div>
                    </div>
                  </div>

                  <!--begin::Form-->
                  <form class="m-form" id="frmMenu" method="POST" action="{{ route('update-menu') }}" >
                    @csrf
                    <div class="m-portlet__body">

                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Parent Menu:</label>
                          <div class="col-lg-6">
                            <select class="form-control m-input" id="is_parent" name="is_parent" >
                              <option>Select</option>
                              <?php if (!empty($menus)) {
                                foreach ($menus as $menuR) { ?>
                                  <option value="{{ $menuR->id }}" {{(isset($menu->is_parent) && $menu->is_parent==$menuR->id)?'selected="selected"':''}}>{{ $menuR->name }}</option>
                              <?php }
                              } ?>
                            </select>
                            @if ($errors->has('is_parent'))
                                <span class="help-block">
                                <strong>{{ $errors->first('is_parent') }}</strong>
                                </span>
                            @endif
                          </div>
                        </div>
                      </div>


                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Name:*</label>
                          <div class="col-lg-6">
                            <input type="text" class="form-control m-input" placeholder="Enter Name" name="name" id="name"  value="{{ $menu->name }}">
                            @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span> 
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Font Icon:</label>
                          <div class="col-lg-6">
                            <input type="text" class="form-control m-input" placeholder="Enter Font Icon" name="font_icon" id="font_icon"  value="{{ $menu->font_icon }}">
                            @if ($errors->has('font_icon'))
                              <span class="help-block">{{ $errors->first('font_icon') }}</span> 
                            @endif
                          </div>
                        </div>
                      </div>

                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Url:</label>
                          <div class="col-lg-6">
                            <input type="text" class="form-control m-input" placeholder="Enter Url" name="url" id="url"  value="{{ $menu->url }}">
                            @if ($errors->has('url'))
                              <span class="help-block">{{ $errors->first('url') }}</span> 
                            @endif
                          </div>
                        </div>
                      </div>
                      <input type="hidden" class="form-control" id="menu_id" name="menu_id" value="{{ $menu->id }}" >
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                      <div class="m-form__actions m-form__actions">
                        <div class="row">
                          <div class="col-lg-3"></div>
                          <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a  href="{{ route('menus') }}" class="btn btn-danger">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

                  <!--end::Form-->
                </div>

                <!--end::Portlet-->
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('script')
  <script>
  $(document).ready(function(){
    $("#frmMenu").validate({
      rules: {
        name: {
          required: true
        }
      },
      messages:{
        name:{
          required:"Please Enter Your Menu Name"
        }
      }
    });
    });
  </script>
@endsection