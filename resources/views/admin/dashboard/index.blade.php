@extends('layouts.app')
@section('title')
Dashboard
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Dashboard</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Home</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="m-portlet m-portlet--mobile">
         <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
               <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                     Statistics
                  </h3>
               </div>
            </div>
            <div class="m-portlet__head-tools">
               <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item"></li>
               </ul>
            </div>
         </div>
         <div class="m-portlet__body">
            <!--begin: Datatable -->
           
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
@endsection