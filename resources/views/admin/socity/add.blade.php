@extends('layouts.app')
@section('title')
Add Socity
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Socity</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Socity</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Socity
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-socity') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Socity Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Socity Name" name="name" maxlength="20" id="name" value="{{ old('name') }}">
                              @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Email:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Email" name="email" id="email" value="{{ old('email') }}">
                              @if ($errors->has('email'))
                              <span class="help-block">{{ $errors->first('email') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Subscription Name:</label>
                           <div class="col-lg-6 input-class">
                              <select class="form-control m-input" id="subscription_name" name="subscription_name" >
                                 <option>Select Subscription Name</option>
                                 <?php if (!empty($subscription)) {
                                    foreach ($subscription as $val) { ?>
                                 <option value="{{ $val->id }}">{{ $val->plan_name }}</option>
                                 <?php }
                                    } ?>
                              </select>
                              @if ($errors->has('subscription_name'))
                              <span class="help-block">
                              <strong>{{ $errors->first('subscription_name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Socity Address:*</label>
                           <div class="col-lg-6 input-class">
                              <input type="text" class="form-control map-input" placeholder="Enter Socity Address" name="address_address" id="address-input" value="{{ old('address_address') }}">
                              @if ($errors->has('address_address'))
                              <span id="address_error" style="display: none;" class="error address_error" for="address">Select a valid  address</span><span class="m-form__help error">{{ $errors->first('address') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="address_div" style="display: none"></div>
                     <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                     <input type="hidden" name="address_longitude" id="address-longitude" value="0" />
                     <div id="address-map-container" style="width:45%;height:278px; ">
                        <div style="width: 100%; height: 100%" id="address-map"></div>
                     </div>
                     <input id="manual_address" name="manual_address" value="{{ old('manual_address') }}" type="hidden">
                     <input id="state" name="state" value="{{ old('state') }}" type="hidden">
                     <input id="postcode" name="postcode" value="{{ old('postcode') }}" type="hidden">
                     <input id="country" name="country" value="{{ old('country') }}" type="hidden">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Contact No:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Contact No" name="contact_no" id="contact_no" value="{{ old('contact_no') }}">
                              @if ($errors->has('contact_no'))
                              <span class="help-block">{{ $errors->first('contact_no') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Status:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_active" id="is_active" value="1" @if(old('is_active') == 1) checked="checked" @endif>
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('socity') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
              name: {
                required: true,
              },
              email: {
                required: true,
                email:true,
              },
              subscription_name:{
                required:true
              },
              address_address:{
                required:true
              },
              contact_no:{
                required:true,
              },
              socity_admin:{
                required: true,
              },
            },
          messages: {
              name:{
                required: "Please enter your socity name",
              },
              email:{
                required: "Please enter your email",
              },
              subscription_name:{
                required:"Please Select Your Subscription Name"
              },
              address_address:{
                required:"Please Enter Your Address"
              },
              contact_no:{
                required: "Please enter your contact number",
              },
              socity_admin:{
                required: "Please select your socity admin",
              },
          },
        });
   });
   function initialize() {
    $("body").on("keyup keyup","#address",function(){
   var count = $(".address_div").parent().find('.street-address').length;
   var input = $(this).val();
   if($.trim(input) == "" || $.trim(input) == null){
       $('#latitude').val('');
       $('#longitude').val('');
   }
   if(count < 1){
       if($.trim(input) != ""){
           $(".address_error").show();
       }else{
           $(".address_error").hide();
       }
       $('#latitude').val('');
       $('#longitude').val('');
   }
   });
    const locationInputs = document.getElementsByClassName("map-input");
    const autocompletes = [];
    const geocoder = new google.maps.Geocoder;
    for (let i = 0; i < locationInputs.length; i++) {
        const input = locationInputs[i];
        const fieldKey = input.id.replace("-input", "");
        const isEdit = document.getElementById(fieldKey + "-latitude").value != '' && document.getElementById(fieldKey + "-longitude").value != '';
        const latitude = parseFloat(document.getElementById(fieldKey + "-latitude").value) || 20.5937;
        const longitude = parseFloat(document.getElementById(fieldKey + "-longitude").value) || 78.9629;
        const map = new google.maps.Map(document.getElementById(fieldKey + '-map'), {
            center: {lat: latitude, lng: longitude},
            zoom: 13
        });
        const marker = new google.maps.Marker({
            map: map,
            position: {lat: latitude, lng: longitude},
        });
        marker.setVisible(isEdit);
        const autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.key = fieldKey;
        autocompletes.push({input: input, map: map, marker: marker, autocomplete: autocomplete});
    }
    for (let i = 0; i < autocompletes.length; i++) {
        const input = autocompletes[i].input;
        const autocomplete = autocompletes[i].autocomplete;
        const map = autocompletes[i].map;
        const marker = autocompletes[i].marker;
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            marker.setVisible(false);
            const place = autocomplete.getPlace();
            geocoder.geocode({'placeId': place.place_id}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    const lat = results[0].geometry.location.lat();
                    const lng = results[0].geometry.location.lng();
                    setLocationCoordinates(autocomplete.key, lat, lng);
                }
            });
            if (!place.geometry) {
                window.alert("No details available for input: '" + place.name + "'");
                input.value = "";
                return;
            }
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
        });
      /*  map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 20.5937, long: 78.9629},
          zoom: 12
        });*/
            /*function init_map(){
             google.maps.event.addListener(map, "click", function(overlay, latLng,results,lat,lng,key)
            {
                   const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
             });
            }*/
          /*  google.maps.event.addDomListener(window, 'load', init_map);*/
    }
   }
   function setLocationCoordinates(key, lat, lng) {
    const latitudeField = document.getElementById(key + "-" + "latitude");
    const longitudeField = document.getElementById(key + "-" + "longitude");
    latitudeField.value = lat;
    longitudeField.value = lng;
   }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtRFp40-0JkjSjKBdmoa1RbNefd1WnDsI&libraries=places&callback=initialize"
   async defer></script>
@endsection