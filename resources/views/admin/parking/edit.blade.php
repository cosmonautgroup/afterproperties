@extends('layouts.app')
@section('title')
Edit Parking
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Parking</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Parking</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Edit Parking
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-parking') }}" enctype='multipart/form-data' >
                  @csrf
                  <input type="hidden" name="id" id="id" value="{{$parkingObj->id}}">
                  <div class="m-portlet__body" id="form_main">
                     <div class="row">
                        <div class="form-group m-form__group col-md-3" style="margin-bottom: -1px;margin-top: 15px;">
                           <label >Socity Name</label>
                           <div class="input-class">
                              <select  name="socity" class="form-control" autocomplete="off" id="socity" >
                              @if(!empty($socity))
                              @foreach($socity as $val)
                              <option value="{{$val->id}}" @if($val->id == $parkingObj->socity_id) selected @endif>{{$val->society_name}} </option>
                              @endforeach
                              @endif
                              </select>
                              @if ($errors->has('socity'))
                              <span class="help-block">
                              <strong>{{ $errors->first('socity') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label >Block Name</label>
                           <div class="input-class">
                              <select  name="block" class="form-control" autocomplete="off" id="block" >
                              @if(!empty($blocks))
                              @foreach($blocks as $val)
                              <option value="{{$val->id}}" @if($val->id == $parkingObj->block_id) selected @endif>{{$val->name}} </option>
                              @endforeach
                              @endif
                              </select>
                              @if ($errors->has('block'))
                              <span class="help-block">
                              <strong>{{ $errors->first('block') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label >Floor</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="floor" name="floor" >
                                 <option>Select Floor Name</option>
                                 @if(!empty($floor))
                                 @foreach($floor as $val)
                                 <option value="{{$val->id}}" @if($val->id == $parkingObj->floor_id) selected @endif>{{$val->floor}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('floor'))
                              <span class="help-block">
                              <strong>{{ $errors->first('floor') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label >Unit</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="unit" name="unit" >
                                 <option>Select Unit</option>
                                 @if(!empty($unit))
                                 @foreach($unit as $val)
                                 <option value="{{$val->id}}" @if($val->id == $parkingObj->unit_id) selected @endif>{{$val->unit_name}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('unit'))
                              <span class="help-block">
                              <strong>{{ $errors->first('unit') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Parking Floor</label>
                           <div class="col-lg-6">
                              <select  name="parking_floor" class="form-control" autocomplete="off" id="parking_floor" >
                                 <option>{{$parkingObj->floor}}</option>
                                 <option>Select Parking Floor</option>
                                 <option value="onstreet"> On-Street Parking</option>
                                 <option value="offstreet">Off Street Parking</option>
                                 <option value="indoor">Indoor Parking</option>
                                 <option value="parkade">Parkade Parking</option>
                                 <option value="underground">Underground Parking</option>
                                 <option value="outdoor">Outdoor Stalls</option>
                              </select>
                              @if ($errors->has('parking_floor'))
                              <span class="m-form__help error">{{ $errors->first('parking_floor') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Parking No</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Parking No" name="parking_no" maxlength="20" id="parking_no" value="{{ $parkingObj->parking_no }}">
                              @if ($errors->has('parking_no'))
                              <span class="help-block">{{ $errors->first('parking_no') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('parking') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script>
   $(document).ready(function() {
      $("#frmUser").validate({
      ignore: [],
       debug: false,
       rules: {
           socity: {
             required: true,
           },
           block: {
             required: true,
           },
           floor: {
             required: true,
           },
           unit_number: {
            required: true,
           },
           parking_floor: {
            required: true,
           },
           parking_no:{
            required: true,
           },
         },
       messages: {
           socity:{
             required: "Please select your socity name",
           },
           block:{
             required: "Please select your block",
           },
           floor:{
            required: "Please select your floor",
           },
           unit_number:{
            required: "Please select your unit number",
           },
           parking_floor:{
            required: "Please select your parking floor",
           },
           parking_no:{
             required: "Please enter your parking number",
           },
       },
     });
   /*Start State*/
   $('#block').change(function(){
   var blockId = $(this).val();
   if(blockId)
   {
       $.ajax({
       type:"GET",
       url:"{{url('admin/parking/floor')}}?blockid="+blockId,
       success:function(res){
         if(res){
           $("#floor").empty();
           $("#floor").append('<option>Select Floor</option>');
           $.each(res,function(key,value){
           $("#floor").append('<option value="'+key+'">'+value+'</option>');
           });
         }
         else{
             $("#floor").empty();
         }
       }
     });
   }
   else
   {
       $("#floor").empty();
   }
   });
   /*End State*/
   $('#floor').change(function(){
   var floorId = $("#floor option:selected").text();
   var blockId = $('#block').val();
   if(floorId)
   {
    $.ajax({
       type:"GET",
       url:"{{url('admin/parking/unit')}}?blockid="+blockId+"&floorid="+floorId,
       success:function(res){
         if(res){
           $("#unit").empty();
           $("#unit").append('<option>Select Units Number</option>');
           $.each(res,function(key,value){
           $("#unit").append('<option value="'+key+'">'+value+'</option>');
           });
         }
         else{
             $("#unit").empty();
         }
       }
     });
   }
   else
   {
       $("#unit").empty();
   }
   });
   });
</script>
@endsection