@extends('layouts.app')
@section('title')
Add Posts
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Posts</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Posts</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Posts
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-category') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Category Name:*</label>
                           <div class="col-lg-6">
                              <select name="category_name" id="category_name" class="form-control" autocomplete="off">
                                <option>Select Category Name</option>
                                @foreach($category as $val)
                                 <option value="{{ $val->id }}">{{ $val->category_name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('category_name'))
                              <span class="help-block">{{ $errors->first('category_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">User Name:*</label>
                           <div class="col-lg-6">
                              <select name="User_name" id="User_name" class="form-control" autocomplete="off">
                                <option>Select User Name</option>
                                @foreach($user as $val)
                                 <option value="{{ $val->id }}">{{ $val->first_name }} {{$val->last_name}}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('user_name'))
                              <span class="help-block">{{ $errors->first('user_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-md-3 form-control-label" for="text-input">Media Type:</label>
                              <div style="margin-top: 0px;margin-left: 19px;" id="type">
                                <div id= "media">
                                 <input type="radio" name="type" id="image" value="image">
                                 <lable>Image</lable>
                                 <input type="radio" name="type" id="video" value="video">
                                 <lable>Video</lable>
                               </div>
                              </div>
                        </div>
                     </div>
                     <div id="select" style="display: none;">
                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Profile Picture:</label>
                           <div class="col-lg-6">
                              <input type="file" class="m-input form-control" accept="image/*" name="image" id="image">
                              @if ($errors->has('image'))
                              <span class="help-block">{{ $errors->first('image') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     </div>
                     <div id="text" style="display: none;">
                       <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Video Url</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Url" name="url" maxlength="20" id="url" value="{{ old('url') }}">
                              @if ($errors->has('url'))
                              <span class="help-block">{{ $errors->first('url') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Media</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Media" maxlength="20" id="media" name="media" value="{{ old('media') }}">
                              @if ($errors->has('media'))
                              <span class="help-block">{{ $errors->first('media') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Description</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Description" maxlength="20" id="description" name="description" value="{{ old('description') }}">
                              @if ($errors->has('description'))
                              <span class="help-block">{{ $errors->first('description') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-md-3 form-control-label" for="text-input">Visibility</label>
                              <div style="margin-top: 0px;margin-left: 19px;" id="type">
                                 <input type="radio" name="type" id="private" value="0">
                                 <lable>Private</lable>
                                 <input type="radio" name="type" id="public" value="1">
                                 <lable>Public</lable>
                               </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-md-3 form-control-label" for="text-input">Enable Review</label>
                              <div style="margin-top: 0px;margin-left: 19px;" id="type">
                                 <input type="radio" name="type" id="Disable" value="0">
                                 <lable>Disable</lable>
                                 <input type="radio" name="type" id="Enable" value="1">
                                 <lable>Enable</lable>
                               </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Menu:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_menu" id="is_menu" value="1" @if(old('is_menu') == 1) checked="checked" @endif>
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Active:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="is_active" id="is_active" value="1" @if(old('is_active') == 1) checked="checked" @endif> 
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('category') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script>
   $(document).ready(function(){
     $("#frmUser").validate({
             rules: {
               category_name: {
                 required: true,
               }
             },
             messages:{
               category_name: {
                   required: "please Enter Your Category Name"
               }
             },
             highlight: function(e) {
                 $(e).closest(".form-label-group").addClass("has-error")
             },
             success: function(e) {
                 e.closest(".form-label-group").removeClass("has-error"), e.remove()
             },
             submitHandler: function(e) {
                 e.submit()
             }
       });
     $('input[type="radio"]').change(function(){
          if( $(this).val()==="image"){
          $("#select").show()
          }
          else{
          $("#select").hide()
          }
          if( $(this).val()==="video"){
          $("#text").show()
          }
          else{
          $("#text").hide()
          }
      });
     });
</script>
@endsection