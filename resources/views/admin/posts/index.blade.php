@extends('layouts.app')
@section('title')
Post
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Post</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Post</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="m-portlet m-portlet--mobile">
         <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
               <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                     Post
                  </h3>
               </div>
            </div>
            <div class="m-portlet__head-tools">
               <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                     <a href="{{ route('add-posts') }}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                     <span>
                     <i class="la la-plus"></i>
                     <span>Add Post</span>
                     </span>
                     </a>
                  </li>
                  <li class="m-portlet__nav-item"></li>
               </ul>
            </div>
         </div>
         <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="datatable">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Category Name</th>
                     <th>User Name</th>
                     <th>Media Type</th>
                     <th>Post Title</th>
                     <th>Visibility</th>
                     <th>Enable Review</th>
                     <th width="200px">Action</th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
   </div>
</div>
@endsection
@section('script')
<script>
   $(function() {
     var table = $('#datatable').DataTable({
       processing: true,
       serverSide: true,
       ajax: {
           "url" : "{{ route('get-posts') }}",
           "type": "POST",
           "data" : {
            "_token": "{{ csrf_token() }}"
           }
       },
       "lengthMenu": [
             [10, 15, 20, 50, 100, -1],
             [10, 15, 20, 50, 100, "All"]
         ],
       columns:[
                 { data: 'id', name: 'id' },
                 { data: 'category_name', name: 'category_name' },
                 { data: 'user_name', name: 'user_name' },
                 { data: 'media_type', name: 'media_type' },
                 { data: 'post_title', name: 'post_title' },
                 { data: 'visibility', name: 'visibility' },
                 { data: 'enable_review', name: 'enable_review' },
                 { data: 'action', name: 'action' },
               ]
     });
     table.columns().every( function () {
       var that = this;
       $('input', this.footer()).on( 'keyup change', function () {
           if ( that.search() !== this.value ) {
               that.search(this.value).draw();
           }
       });
     });
   });
</script>
@endsection