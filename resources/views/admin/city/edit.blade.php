@extends('layouts.app')
@section('title')
Edit City
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">City</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Edit City</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Edit City
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-city') }}" enctype='multipart/form-data' >
                  @csrf
                  <input type="hidden" name="id" ia="id" value="{{$cityObj->id}}">
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Country Name:*</label>
                           <div class="col-lg-6">
                              <select class="form-control m-input" id="country_name" name="country_name" >
                              @if(!empty($country))
                              @foreach($country as $loc)
                              <option value="{{$loc->id}}" @if($loc->id == $cityObj->country_id) selected @endif>{{$loc->country_name}} </option>
                              @endforeach
                              @endif
                              </select>
                              @if ($errors->has('country_name'))
                              <span class="help-block">{{ $errors->first('country_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">State Name:*</label>
                           <div class="col-lg-6">
                              <select class="form-control m-input" id="state_name" name="state_name" >
                              @if(!empty($state))
                              @foreach($state as $loc)
                              <option value="{{$loc->id}}" @if($loc->id == $cityObj->state_id) selected @endif>{{$loc->state_name}} </option>
                              @endforeach
                              @endif
                              </select>
                              @if ($errors->has('state_name'))
                              <span class="help-block">{{ $errors->first('state_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">city Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter City Name" name="city_name" maxlength="20" id="city_name" value="{{ $cityObj->city_name}}">
                              @if ($errors->has('city_name'))
                              <span class="help-block">{{ $errors->first('city_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('city') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
            country_name:{
              required: true,
            },
            state_name:{
              required: true,
            },
            city_name: {
              required: true,
            },
            },
          messages: {
            country_name:{
              required: "Please Select Your Country Name",
            },
            state_name:{
              required: "Please Select Your Staet Name",
            },
            city_name: {
              required: "Please enter your city name",
            },
          },
        });
   });
</script>
<script>
   $(document).ready(function() {
   /*Start State*/
   $('#country_name').change(function(){
   var countryID = $(this).val();
   if(countryID)
   {
       $.ajax({
       type:"GET",
       url:"{{url('admin/city/getstate')}}?countryid="+countryID,
       success:function(res){
         if(res){
           $("#state_name").empty();
           $("#state_name").append('<option>Select State</option>');
           $.each(res,function(key,value){
           $("#state_name").append('<option value="'+key+'">'+value+'</option>');
           });
         }
         else{
             $("#state_name").empty();
         }
       }
     });
   }
   else
   {
       $("#state_name").empty();
   }
   });
   });
</script>
@endsection