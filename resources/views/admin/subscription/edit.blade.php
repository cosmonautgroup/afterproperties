@extends('layouts.app')
@section('title')
Edit Subscription
@endsection
@section('content')
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Subscription</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Edit Subscription</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Edit Subscription
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('subscription-create') }}" enctype='multipart/form-data' >
                  @csrf
                  <input type="hidden" name="id" id="id" value="{{$subObj->id}}">
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Plan Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Plan Name" name="plan_name" maxlength="20" id="plan_name" value="{{ $subObj->plan_name }}">
                              @if ($errors->has('plan_name'))
                              <span class="help-block">{{ $errors->first('plan_name') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Validity:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Validity" name="validity" maxlength="20" id="validity" value="{{ $subObj->validity }}">
                              @if ($errors->has('validity'))
                              <span class="help-block">{{ $errors->first('validity') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Price:</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Price" name="price" id="price" value="{{ $subObj->price }}">
                              @if ($errors->has('price'))
                              <span class="help-block">{{ $errors->first('price') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('subscription') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
              plan_name: {
                required: true,
              },
              validity: {
                required: true,
              },
              price: {
                required: true,
              },
            },
          messages: {
              plan_name:{
                required: "Please enter your plan name",
              },
              validity:{
                required: "Please enter your validity",
              },
              price:{
                required: "Please enter your price",
              },
          },
        });
   });
</script>
@endsection