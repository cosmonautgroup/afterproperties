@extends('layouts.app')
@section('title')
Add Units
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Units</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Units</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Units
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-units') }}" enctype='multipart/form-data' >
                  @csrf
                  <input type="hidden" name="id" value="{{ $unitsObj->id }}">
                  <div class="m-portlet__body" id="form_main">
                     <div class="row" style="margin-left: 121px;margin-right: -162px;">
                        <div class="form-group m-form__group col-md-3" style="margin-bottom: -1px;margin-top: 15px;">
                           <label >Block Name</label>
                           <div class="input-class">
                              <select  name="block" class="form-control" autocomplete="off" id="block" >
                              @if(!empty($blocks))
                              @foreach($blocks as $val)
                              <option value="{{$val->id}}" @if($val->id == $unitsObj->block_id) selected @endif>{{$val->name}} </option>
                              @endforeach
                              @endif
                              </select>
                              @if ($errors->has('block'))
                              <span class="help-block">
                              <strong>{{ $errors->first('block') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label >Floor</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="floor" name="floor" >
                                 <option>Select Floor Name</option>
                                 @if(!empty($floor))
                                 @foreach($floor as $val)
                                 <option value="{{$val->id}}" @if($val->id == $unitsObj->floor_id) selected @endif>{{$val->floor}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('floor'))
                              <span class="help-block">
                              <strong>{{ $errors->first('floor') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label >Unit</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="unit" name="unit" >
                                 <option>Select Unit</option>
                                 @if(!empty($unit))
                                 @foreach($unit as $val)
                                 <option value="{{$val->id}}" @if($val->id == $unitsObj->unit_id) selected @endif>{{$val->unit_name}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('unit'))
                              <span class="help-block">
                              <strong>{{ $errors->first('unit') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-2" style="margin-top: 19px">
                           <label >Unit Detail:</label>
                        </div>
                        <div class="form-group m-form__group col-md-3" style="margin-left: -23px;">
                           <label>Bhks</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="bhk" name="bhk" >
                                 <option>Select Bhks</option>
                                 @if(!empty($bhk))
                                 @foreach($bhk as $val)
                                 <option value="{{$val->id}}" @if($val->id == $unitsObj->bhk) selected @endif>{{$val->bhk}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('bhk'))
                              <span class="m-form__help error">{{ $errors->first('bhk') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label>View</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter View" name="view" maxlength="20" id="view" value="{{ $unitsObj->view }}">
                              @if ($errors->has('view'))
                              <span class="m-form__help error">{{ $errors->first('view') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label>Squarfeet</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter Squarfit" name="squarfit" maxlength="20" id="squarfit" value="{{ $unitsObj->squarfeet }}">
                              @if ($errors->has('squarfit'))
                              <span class="m-form__help error">{{ $errors->first('squarfit') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Parking No</label>
                           <div class="col-lg-6" style="margin-left: -106px;">
                              <select class="form-control m-input" id="parking_no" name="parking_no" >
                                 <option>Select Parking</option>
                                 @if(!empty($parking))
                                 @foreach($parking as $val)
                                 <option value="{{$val->id}}" @if($val->id == $unitsObj->parking_no) selected @endif>{{$val->parking_no}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('parking_no'))
                              <span class="help-block">{{ $errors->first('parking_no') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Assign To User</label>
                           <div class="col-lg-6" style="margin-left: -106px;">
                              <select class="form-control m-input" id="user" name="user" >
                                 <option>Select User</option>
                                 @if(!empty($user))
                                 @foreach($user as $val)
                                 <option value="{{$val->id}}" @if($val->id == $unitsObj->assign_user) selected @endif>{{$val->first_name}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('user'))
                              <span class="help-block">{{ $errors->first('user') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('units') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
              block: {
                required: true,
              },
              floor: {
                required: true,
              },
              unit: {
                required: true,
              },
              bhk: {
                required: true,
              },
              view: {
                required: true,
              },
              squarfeet: {
                required: true,
              },
              parking_no: {
                required: true,
              },
              user:{
                required: true,
              },
            },
          messages: {
              block:{
                required: "Please select your block name",
              },
              floor:{
                required: "Please select your floor",
              },
              unit:{
                required: "Please select your unit",
              },
              bhk:{
                required: "Please select your bhk",
              },
              view:{
                required: "Please enter your view",
              },
              squarfeet: {
                required: "Please enter your squarfeet",
              },
              parking_no:{
                required: "Please enter your parking number",
              },
              user:{
                required: "Please select your user",
              },
          },
        });
   });
</script>
<script>
   $(document).ready(function() {
   /*Start State*/
   $('#block').change(function(){
   var blockId = $(this).val();
   if(blockId)
   {
       $.ajax({
       type:"GET",
       url:"{{url('admin/units/floor')}}?blockid="+blockId,
       success:function(res){
         if(res){
           $("#floor").empty();
           $("#floor").append('<option>Select Floor</option>');
           $.each(res,function(key,value){
           $("#floor").append('<option value="'+key+'">'+value+'</option>');
           });
   
         }
         else{
             $("#floor").empty();
         }
       }
     });
   }
   else
   {
       $("#floor").empty();
   }
   });
   /*End State*/
   $('#floor').change(function(){
   var floorId = $("#floor option:selected").text();
   var blockId = $('#block').val();
   if(floorId)
   {
    $.ajax({
       type:"GET",
       url:"{{url('admin/units/unit')}}?blockid="+blockId+"&floorid="+floorId,
       success:function(res){
         if(res){
           $("#unit").empty();
           $("#unit").append('<option>Select Units Number</option>');
           $.each(res,function(key,value){
           $("#unit").append('<option value="'+key+'">'+value+'</option>');
           });
         }
         else{
             $("#unit").empty();
         }
       }
     });
   }
   else
   {
       $("#unit").empty();
   }
   });
   });
</script>
@endsection