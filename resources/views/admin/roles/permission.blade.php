@extends('layouts.app')
@section('title')
Edit Permission
@endsection
@section('content')
<!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Permissions</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                  <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                      <i class="m-nav__link-icon la la-home"></i>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="{{url('admin/roles')}}" class="m-nav__link">
                      <span class="m-nav__link-text">Roles</span>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="javascript:;" class="m-nav__link">
                      <span class="m-nav__link-text">Edit Permission</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div>
              
              </div>
            </div>
          </div>

          <!-- END: Subheader -->
          <div class="m-content">
            @if (session('error'))
                  <div class="alert alert-danger" role="alert">
                      {{ session('error') }}
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session('success') }}
                  </div>
              @endif
            <div class="row">
              <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                          <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                          Edit Permission
                        </h3>
                      </div>
                    </div>
                  </div>

                  <!--begin::Form-->
                  <form class="m-form" id="frmPermission"  method="POST" action="{{ route('create-permission') }}">
                    @csrf
                    <div class="m-portlet__body">
                      <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Permission Menus:</label>
                          <div class="col-lg-6">
                            <?php $permissions = \Spatie\Permission\Models\Permission::where('is_parent',0)->orderBy('order_by','asc')->get();
                            if (!empty($permissions)) {
                              foreach ($permissions as $permission) { ?>
                                <div class="m-checkbox-list">
                                  <?php  $role_has_permission = \DB::table('role_has_permissions')->where(['role_id'=>$role->id,'permission_id'=>$permission->id])->first(); ?>
                                  <label class="m-checkbox">
                                      <input type="checkbox" class="multi selectAll_{{ $permission->id }}"  id="permission_{{ $permission->id }}" name="permission[]" value="{{ $permission->id }}"  <?php if (!empty($role_has_permission)) { echo "checked='checked'"; } ?> > {{ $permission->name }} 
                                      <span></span>
                                  </label>

                                    <div class="col-lg-6">
                                    <?php $sub_permissions = \Spatie\Permission\Models\Permission::where('is_parent',$permission->id)->get(); 
                                      if (!empty($sub_permissions)) { ?>
                                        <?php foreach ($sub_permissions as $sub_permission) {?>
                                          <div class="m-checkbox-list">
                                            <?php $role_has_permissions = \DB::table('role_has_permissions')->where(['role_id'=>$role->id,'permission_id'=>$sub_permission->id])->first(); ?>
                                            <label class="m-checkbox">
                                                <input type="checkbox" class="single selectSingle_{{ $permission->id }}" data-id="parent_{{ $permission->id }}"  id="permission_{{ $sub_permission->id }}" name="permission[]" value="{{ $sub_permission->id }}" <?php if (!empty($role_has_permissions)) { echo "checked='checked'"; } ?> > {{ $sub_permission->name }} 
                                                <span></span>
                                            </label>
                                          </div>
                                      <?php } } ?>
                                      </div>
                                </div>
                            <?php } } ?>
                         </div>
                      </div>
                    </div>
                    
                    <input type="hidden" class="form-control" id="role_id" name="role_id" value="{{ $role->id }}" >
                    
                    <div class="m-portlet__foot m-portlet__foot--fit">
                      <div class="m-form__actions m-form__actions">
                        <div class="row">
                          <div class="col-lg-3"></div>
                          <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Submit</button>
                            <a  href="{{ route('roles') }}" class="btn btn-danger">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

                  <!--end::Form-->
                </div>

                <!--end::Portlet-->
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection

@section('script')
<script>
  $(document).ready(function(){
      $("body").on("click",".multi",function(){
          var str = $(this).attr("id");
          var str_ary = str.split("_");
          var id = str_ary[1];
          $('.selectSingle_'+id).not(this).prop('checked', this.checked);
      });

      $("body").on("click",".single",function(){
          var str = $(this).attr("data-id");
          var str_ary = str.split("_");
          var id = str_ary[1];
          var parent_class = ".selectAll_"+id;
          var child_class = ".selectSingle_"+id;
          
          $(parent_class).prop('checked', ($(child_class+':checked').length == $(child_class).length));
      });
  }); 
</script>
@endsection