@extends('layouts.app')
@section('title')
Edit Role
@endsection
@section('content')
<!-- END: Left Aside -->
        <div class="m-grid__item m-grid__item--fluid m-wrapper">

          <!-- BEGIN: Subheader -->
          <div class="m-subheader ">
            <div class="d-flex align-items-center">
              <div class="mr-auto">
                <h3 class="m-subheader__title m-subheader__title--separator">Roles</h3>
                <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                  <li class="m-nav__item m-nav__item--home">
                    <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                      <i class="m-nav__link-icon la la-home"></i>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="{{url('admin/roles')}}" class="m-nav__link">
                      <span class="m-nav__link-text">Roles</span>
                    </a>
                  </li>
                  <li class="m-nav__separator">-</li>
                  <li class="m-nav__item">
                    <a href="" class="m-nav__link">
                      <span class="m-nav__link-text">Edit Role</span>
                    </a>
                  </li>
                </ul>
              </div>
              <div>
              
              </div>
            </div>
          </div>

          <!-- END: Subheader -->
          <div class="m-content">
            @if (session('error'))
                  <div class="alert alert-danger" role="alert">
                      {{ session('error') }}
                  </div>
              @endif
              @if (session('success'))
                  <div class="alert alert-success" role="alert">
                      {{ session('success') }}
                  </div>
              @endif
            <div class="row">
              <div class="col-lg-12">

                <!--begin::Portlet-->
                <div class="m-portlet">
                  <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                      <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                          <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                          Edit Role
                        </h3>
                      </div>
                    </div>
                  </div>

                  <!--begin::Form-->
                  <form class="m-form" id="frmRole" method="POST" action="{{ route('update-role') }}" >
                    @csrf
                    <div class="m-portlet__body">
                      <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                          <label class="col-lg-3 col-form-label">Name:*</label>
                          <div class="col-lg-6">
                            <input type="text" class="form-control m-input" value="{{ $role->name }}" placeholder="Enter Name" name="name" id="name">
                            @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span> 
                            @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    <input type="hidden" class="form-control" id="role_id" name="role_id" value="{{ $role->id }}" >
                    <div class="m-portlet__foot m-portlet__foot--fit">
                      <div class="m-form__actions m-form__actions">
                        <div class="row">
                          <div class="col-lg-3"></div>
                          <div class="col-lg-6">
                            <button type="submit" class="btn btn-success">Update</button>
                            <a  href="{{ route('roles') }}" class="btn btn-danger">Cancel</a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>

                  <!--end::Form-->
                </div>

                <!--end::Portlet-->
              </div>
            </div>
          </div>
        </div>
      </div>
@endsection
@section('script')
  <script>
  $(document).ready(function(){
    $("#frmRole").validate({
      rules: {
        name: {
          required: true
        }
      },
      messages:{
        name:{
         required:"Please Enter Your Role Name"
        }
      }
    });
    });
  </script>
@endsection