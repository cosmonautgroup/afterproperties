@extends('layouts.app')
@section('title')
Add Amenites
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Amenites</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="" class="m-nav__link">
                  <span class="m-nav__link-text">Amenites</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Amenites
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-amenites') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Socity Name:*</label>
                           <div class="col-lg-6">
                              <select class="form-control m-input" id="socity" name="socity" >
                                 <option value="">Select Socity Name</option>
                                    @foreach ($socity as $val)
                                 <option value="{{ $val->id }}">{{ $val->society_name }}</option>
                                 @endforeach
                              </select>
                              @if ($errors->has('socity'))
                              <span class="m-form__help error">{{ $errors->first('socity') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Amenites Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Amenites Name" name="name" maxlength="20" id="name" value="{{ old('name') }}">
                              @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="form-group m-form__group col-md-6" style="margin-bottom: -1px;margin-top: 15px;">
                           <label >Opening Time</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input timepicker" placeholder="Enter Opening Time" name="time" maxlength="20" id="time" value="{{ old('time') }}">
                              @if ($errors->has('time'))
                              <span class="m-form__help error">{{ $errors->first('time') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-6">
                           <label>Closing Time</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input timepicker" placeholder="Enter Opening Time" name="end_time" maxlength="20" id="end_time" value="{{ old('end_time') }}">
                              @if ($errors->has('end_time'))
                              <span class="m-form__help error">{{ $errors->first('end_time') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Days Available</label>
                           <div class="col-lg-6">
                              <select  name="avilable" class="form-control" autocomplete="off" id="avilable" >
                                 <option value="{{old('avilable')}}">Select Days Available</option>
                                 <option>All</option>
                                 <option>Monday-Friday</option>
                                 <option>Monday-Saturday</option>
                                 <option>Saturday-Sunday</option>
                                 <option>Only Sunday</option>
                              </select>
                              @if ($errors->has('avilable'))
                              <span class="help-block">{{ $errors->first('avilable') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Amenites Rules:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Amenites Rules" name="rules" maxlength="20" id="rules" value="{{ old('rules') }}">
                              @if ($errors->has('rules'))
                              <span class="help-block">{{ $errors->first('rules') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Amenites Image</label>
                           <div class="col-lg-6">
                              <input type="file" class="m-input form-control" accept="image/*" name="image" id="image">
                              @if ($errors->has('image'))
                              <span class="help-block">{{ $errors->first('image') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('amenites') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   // Class definition
   var KTBootstrapTimepicker = function () {
           var demos = function () {
            $('#time').timepicker({
               minuteStep: 1,
               showSeconds: true,
               showMeridian: true
           });
            $('#end_time').timepicker({
               minuteStep: 1,
               showSeconds: true,
               showMeridian: true
           });
       }
       return {
           init: function() {
               demos();
           }
       };
   }();
   jQuery(document).ready(function() {
       KTBootstrapTimepicker.init();
   });
</script>
<script type="text/javascript">
   $(document).ready(function(){
   $("#frmUser").validate({
        ignore: [],
          debug: false,
          rules: {
            socity: {
              required: true,
            },
            name: {
              required: true,
            },
              time: {
                required: true,
              },
              end_time: {
                required: true,
              },
              avilable: {
                required: true,
              },
              rules: {
                required: true,
              },
            },
          messages: {
            socity: {
              required: "Please select your socity",
            },
            name: {
              required: "Please enter your amenites name",
            },
              time:{
                required: "Please select your amenites opening time",
              },
              end_time:{
                required: "Please select your amenites closing time",
              },
              avilable: {
                required: "Please select your avilable days",
              },
              rules: {
                required: "Please enter your amenites rules",
              },
          },
        });
   });
</script>
@endsection