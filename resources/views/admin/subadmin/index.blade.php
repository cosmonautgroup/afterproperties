@extends('layouts.app')
@section('title')
Subadmin
@endsection
@section('content')
<!-- page content -->
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Subadmin</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Subadmin</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="m-portlet m-portlet--mobile">
         <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
               <div class="m-portlet__head-title">
                  <h3 class="m-portlet__head-text">
                     Providers
                  </h3>
               </div>
            </div>
            <div class="m-portlet__head-tools">
               <ul class="m-portlet__nav">
                  <li class="m-portlet__nav-item">
                     <a href="{{ route('add-subadmin') }}" class="btn btn-info m-btn m-btn--custom m-btn--icon m-btn--air">
                     <span>
                     <i class="la la-plus"></i>
                     <span>Add Subadmin</span>
                     </span>
                     </a>
                  </li>
                  <li class="m-portlet__nav-item"></li>
               </ul>
            </div>
         </div>
         <div class="m-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="datatable">
               <thead>
                  <tr>
                     <th>ID</th>
                     <th>Profile Picture</th>
                     <th>First Name</th>
                     <th>Last Name</th>
                     <th>Email</th>
                     <th>Registration Date/Time</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
      <!-- END EXAMPLE TABLE PORTLET-->
   </div>
</div>
@endsection
@section('script')
<script>
   $(function() {
     $('#datatable').DataTable({
       processing: true,
       serverSide: true,
       ajax: {
           "url" : "{{ route('get-subadmin') }}",
           "type": "POST",
           "data" : {
            "_token": "{{ csrf_token() }}"
           }
       },
        "lengthMenu": [
             [10, 15, 20, 50, 100, -1],
             [10, 15, 20, 50, 100, "All"]
         ],
       columns:[
                 { data: 'id', name: 'id' },
                 { data: 'image', name: 'image' },
                 { data: 'first_name', name: 'first_name' },
                 { data: 'last_name', name: 'last_name' },
                 { data: 'email', name: 'email' },
                 { data: 'created_at', name: 'created_at' },
                 { data: 'is_active', name: 'is_active' },
                 { data: 'action', name: 'action' },
               ]
     });
   });
</script>
@endsection