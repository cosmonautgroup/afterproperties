@extends('layouts.app')
@section('title')
Add Block
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Block</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Add Block</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Add Block
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-block') }}" enctype='multipart/form-data' >
                  @csrf
                  <input type="hidden" name="id" value="{{ $blockObj->id }}">
                  <div class="m-portlet__body" id="form_main">
                     <div class="row">
                        <div class="form-group m-form__group col-md-3">
                           <label >Socity Name:</label>
                           <div class="input-class">
                              <select class="form-control m-input" id="socity_name" name="socity_name" >
                                 <option>Select Socity Name</option>
                                 @if(!empty($socity))
                                 @foreach($socity as $loc)
                                 <option value="{{$loc->id}}" @if($loc->id == $blockObj->socity_id) selected @endif>{{$loc->society_name}} </option>
                                 @endforeach
                                 @endif
                              </select>
                              @if ($errors->has('socity_name'))
                              <span class="help-block">
                              <strong>{{ $errors->first('socity_name') }}</strong>
                              </span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="row" id="exist-row">
                        <div class="form-group m-form__group col-md-3" style="margin-bottom: -1px;margin-top: 15px;">
                           <label >Block Name:*</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input name" placeholder="Enter Block Name" name="name[]" maxlength="20" id="name" value="{{ $blockObj->name }}">
                              @if ($errors->has('name'))
                              <span class="help-block">{{ $errors->first('name') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-2">
                           <label>Floor:*</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter Floor" name="floor[]" maxlength="20" id="floor" value="{{ $blockObj->floor }}">
                              @if ($errors->has('floor'))
                              <span class="help-block">{{ $errors->first('floor') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label>No Of Units:*</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter No Of Units" name="units[]" maxlength="20" id="units" value="{{$blockObj->unit_no }}">
                              @if ($errors->has('units'))
                              <span class="help-block">{{ $errors->first('units') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-3">
                           <label>Unit Name Start With:*</label>
                           <div class="input-class">
                              <input type="text" class="form-control m-input" placeholder="Enter Unit Name Start With" name="unit_name[]" maxlength="20" id="unit_name" value="{{ $blockObj->unit_num_start_with }}">
                              @if ($errors->has('unit_name'))
                              <span class="help-block">{{ $errors->first('unit_name') }}</span>
                              @endif
                           </div>
                        </div>
                        <div class="form-group m-form__group col-md-1 append-btn ">
                           <span>
                              <i class="la la-plus plus"></i>
                              <!-- <span>Add</span> -->
                           </span>
                        </div>
                        <!-- <div class="form-group m-form__group col-md-1">
                           <i class="fa fa-trash" onclick = "remove_row(this)" ></i>
                           </div> -->
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ route('block') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
   $(document).ready(function(){
      $("#frmUser").validate({
         ignore: [],
           debug: false,
           rules: {
               socity: {
                  required: true,
               },
               "name[]": {
                 required: true,
               },
               "floor[]": {
                  required: true,
               },
               "units[]": {
                 required: true,
               },
               "unit_name[]": {
                  required: true,
               },
             },
           messages: {
               socity: {
                  required: "Please select your socity",
               },
               "name[]":{
                 required: "Please enter your block name",
               },
               "floor[]": {
                  required: "Please enter your floor",
               },
               "units[]":{
                 required: "Please enter your no of units",
               },
               "unit_name[]": {
                  required: "Please enter your unit number start with",
               },
           },
         });
      var rowCount = 1;
      $('.plus').click(function(){
         rowCount++;
         var html = '<input type="hidden" name="multi[]" ><div class="row" id="row_'+rowCount+'"><div class="form-group m-form__group col-md-3" style="margin-bottom: -1px;margin-top: 15px;"><label >Block Name:*</label><div class="input-class"><input type="text" class="form-control m-input name" placeholder="Enter Block Name" name="name[]" maxlength="20" id="name">@if ($errors->has("name"))<span class="help-block">{{ $errors->first("name") }}</span>@endif</div></div><div class="form-group m-form__group col-md-2"><label>Floor:*</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter Floor" name="floor[]" maxlength="20" id="floor">@if ($errors->has("floor"))<span class="help-block">{{ $errors->first("floor") }}</span>@endif</div></div><div class="form-group m-form__group col-md-3"><label>No Of Units:*</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter No Of Units" name="units[]" maxlength="20" id="units">@if ($errors->has("units"))<span class="help-block">{{ $errors->first("units") }}</span>@endif</div></div><div class="form-group m-form__group col-md-3"><label>Unit Name Start With:*</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter Unit Name Start With" name="unit_name[]" maxlength="20" id="unit_name" >@if ($errors->has("unit_name"))<span class="help-block">{{ $errors->first("unit_name") }}</span>@endif</div></div><div class="form-group m-form__group col-md-1 append-btn "><i class="la la-trash-o minus" data-id="'+rowCount+'"></i></div></div>';
         $('#form_main').append(html);
      })
      $(document).on('click','.minus',function(){
         var id = $(this).attr('data-id');
         $('#row_'+id).remove();
      })
      $(document).on('click','.add_block',function(){
         rowCount++;
         var html = '<input type="hidden" name="multi[]" ><div class="row" id="row_'+rowCount+'"><div class="form-group m-form__group col-md-3" style="margin-bottom: -1px;margin-top: 15px;"><label class="col-lg-3 col-form-label">Block Name</label><div class="input-class"><input type="text" class="form-control m-input name" placeholder="Enter Block Name" name="name[]" maxlength="20" id="name">@if ($errors->has("name"))<span class="m-form__help error">{{ $errors->first("name") }}</span>@endif</div></div><div class="form-group m-form__group col-md-2"><labelclass="col-lg-3 col-form-label">Floor</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter Floor" name="floor[]" maxlength="20" id="floor">@if ($errors->has("floor"))<span class="m-form__help error">{{ $errors->first("floor") }}</span>@endif</div></div><div class="form-group m-form__group col-md-3"><labelclass="col-lg-3 col-form-label">No Of Units</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter No Of Units" name="units[]" maxlength="20" id="units">@if ($errors->has("units"))<span class="m-form__help error">{{ $errors->first("units") }}</span>@endif</div></div><div class="form-group m-form__group col-md-3"><label class="col-lg-3 col-form-label">{{ trans("message.unit_name_start_with") }}</label><div class="input-class"><input type="text" class="form-control m-input" placeholder="Enter Unit Name Start With" name="unit_name[]" maxlength="20" id="unit_name" >@if ($errors->has("floor"))<span class="m-form__help error">{{ $errors->first("floor") }}</span>@endif</div></div><div class="form-group m-form__group col-md-1 append-btn"><i class="la la-trash-o minus" data-id="'+rowCount+'"></i></div></div>';
         $('#form_main').append(html);
      })
      $(document).on('click','.block_minus',function(){
         var id = $(this).attr('data-id');
         $('#row_'+id).remove();
      })
   });
</script>
@endsection