@extends('layouts.app')
@section('title')
Update Profile
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Profile</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="" class="m-nav__link">
                  <span class="m-nav__link-text">Edit Profile</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                           Edit Profile
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('profile-update') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">First Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter First Name" name="first_name" id="first_name" value="{{ $user->first_name }}">
                              @if ($errors->has('first_name'))
                              <span class="help-block">{{ $errors->first('first_name') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Last Name:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Last Name" name="last_name" id="last_name" value="{{ $user->last_name }}">
                              @if ($errors->has('last_name'))
                              <span class="help-block">{{ $errors->first('last_name') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Email:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Email" name="email" id="email" value="{{ $user->email }}" >
                              @if ($errors->has('email'))
                              <span class="help-block">{{ $errors->first('email') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__group form-group row">
                        <label class="col-lg-3 col-form-label">Change Password:</label>
                        <div class="col-lg-6">
                           <div class="m-checkbox-inline">
                              <label class="m-checkbox">
                              <input type="checkbox" name="change_password" id="change_password" value="1" @if ($errors->has('old_password') || $errors->has('password') || $errors->has('password_confirmation')) checked="checked" @endif> 
                              <span></span>
                              </label>
                           </div>
                        </div>
                     </div>
                     <div id="changePassword" @if ($errors->has('old_password') || $errors->has('password') || $errors->has('password_confirmation')) style="display:block;" @else style="display:none;" @endif >
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Old Password:*</label>
                           <div class="col-lg-6">
                              <input type="password" class="form-control m-input" placeholder="Enter Old Password" name="old_password" id="old_password">
                              @if ($errors->has('old_password'))
                              <span class="help-block">{{ $errors->first('old_password') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">New Password:*</label>
                           <div class="col-lg-6">
                              <input type="password" class="form-control m-input" placeholder="Enter New Password" name="password" id="password">
                              @if ($errors->has('password'))
                              <span class="help-block">{{ $errors->first('password') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Confirm Password:*</label>
                           <div class="col-lg-6">
                              <input type="password" class="form-control m-input" placeholder="Enter Confirm Password" name="password_confirmation" id="password_confirmation">
                              @if ($errors->has('password_confirmation'))
                              <span class="help-block">{{ $errors->first('password_confirmation') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <br>
                  <?php if (isset($user->image) && $user->image!='' && file_exists(public_path().'/images/users/'.$user->image)) { 
                     $image = url('/public').'/images/users/'.$user->image;
                     }else{
                     $image = url('/public').'/images/users/no-image.png';
                     } ?>
                  <div class="m-form__section m-form__section--first">
                     <div class="form-group m-form__group row">
                        <label class="col-lg-3 col-form-label">Profile Picture:</label>
                        <div class="col-lg-4">
                           <input type="file" class="m-input form-control "  name="image" id="image">
                           @if ($errors->has('image'))
                           <span class="help-block">{{ $errors->first('image') }}</span> 
                           @endif
                        </div>
                        <div class="col-lg-2">
                           <img src="{{ $image }}" height="50" width="50">
                        </div>
                     </div>
                  </div>
                  <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ $user->id }}" >
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
            <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-6">
            <button type="submit" class="btn btn-success">Submit</button>
            <a  href="{{ route('dashboard') }}" class="btn btn-danger">Cancel</a>
            </div>
            </div>
            </div>
            </div>
            </form>
            <!--end::Form-->
         </div>
         <!--end::Portlet-->
      </div>
   </div>
</div>
</div>
@endsection
@section('script')
<script>
   $(document).ready(function(){
     $("#frmUser").validate({
             // Specify vaidation rules
             rules: {    
               first_name: {
                 required: true,
                 lettersonly: true
               },
               last_name: {
                 required: true,
                 lettersonly: true
               },
               email: {
                 required: true,        
                 email: true
               },
               old_password: {
                   required:function(elem)
                           {
                               return $('#change_password').is(":checked");
                           }
               },
               password: {
                   required: function(elem)
                           {
                               return $('#change_password').is(":checked");
                           },
                   minlength: 6
               },
               password_confirmation: {
                   required: function(elem)
                           {
                               return $('#change_password').is(":checked");
                           },
                   equalTo: "#password"
               }
             },
             messages:{
               first_name: {
                   lettersonly: "please enter characters only",
               },
               last_name: {
                   lettersonly: "please enter characters only",
               }
             }
       }); 
   
       $("body").on('change','#change_password',function(){
           if ($('#change_password').is(":checked")) {
               $('#changePassword').show();
           } else {
               $('#changePassword').hide();
           }
       }).trigger('change'); 
   
       jQuery.validator.addMethod("lettersonly", function(value, element) {
          return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/.test(value);
       });
   });
</script>
@endsection