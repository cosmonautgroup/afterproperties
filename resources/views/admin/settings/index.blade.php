@extends('layouts.app')
@section('title')
Settings
@endsection
@section('content')
<!-- END: Left Aside -->
<div class="m-grid__item m-grid__item--fluid m-wrapper">
   <!-- BEGIN: Subheader -->
   <div class="m-subheader ">
      <div class="d-flex align-items-center">
         <div class="mr-auto">
            <h3 class="m-subheader__title m-subheader__title--separator">Settings</h3>
            <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
               <li class="m-nav__item m-nav__item--home">
                  <a href="{{url('admin/dashboard')}}" class="m-nav__link m-nav__link--icon">
                  <i class="m-nav__link-icon la la-home"></i>
                  </a>
               </li>
               <li class="m-nav__separator">-</li>
               <li class="m-nav__item">
                  <a href="javascript:;" class="m-nav__link">
                  <span class="m-nav__link-text">Settings</span>
                  </a>
               </li>
            </ul>
         </div>
         <div>
         </div>
      </div>
   </div>
   <!-- END: Subheader -->
   <div class="m-content">
      @if (session('error'))
      <div class="alert alert-danger" role="alert">
         {{ session('error') }}
      </div>
      @endif
      @if (session('success'))
      <div class="alert alert-success" role="alert">
         {{ session('success') }}
      </div>
      @endif
      <div class="row">
         <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="m-portlet">
               <div class="m-portlet__head">
                  <div class="m-portlet__head-caption">
                     <div class="m-portlet__head-title">
                        <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                        </span>
                        <h3 class="m-portlet__head-text">
                        </h3>
                     </div>
                  </div>
               </div>
               <!--begin::Form-->
               <form class="m-form" id="frmUser" method="POST" action="{{ route('create-settings') }}" enctype='multipart/form-data' >
                  @csrf
                  <div class="m-portlet__body">
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Meta Title:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Meta Title" name="meta_title" maxlength="255" id="meta_title" value="{{ isset($settings->meta_title)?($settings->meta_title):'' }}">
                              @if ($errors->has('meta_title'))
                              <span class="help-block">{{ $errors->first('meta_title') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Meta Keywords:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Keywords" name="meta_keyword" maxlength="255" id="meta_keyword" value="{{ isset($settings->meta_keyword)?($settings->meta_keyword):'' }}">
                              @if ($errors->has('meta_keyword'))
                              <span class="help-block">{{ $errors->first('meta_keyword') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Meta Descriptions:*</label>
                           <div class="col-lg-6">
                              <textarea class="form-control m-input" placeholder="Enter Descriptions" name="meta_description" maxlength="255" id="meta_description">{{ isset($settings->meta_description)?($settings->meta_description):'' }}</textarea>
                              @if ($errors->has('meta_description'))
                              <span class="help-block">{{ $errors->first('meta_description') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Site Title:*</label>
                           <div class="col-lg-6">
                              <textarea class="form-control m-input" placeholder="Enter Site Title" name="site_title" maxlength="255" id="site_title">{{ isset($settings->site_title)?($settings->site_title):'' }}</textarea>
                              @if ($errors->has('site_title'))
                              <span class="help-block">{{ $errors->first('site_title') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Copyright Text:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Copyright Text" name="copyright_text" maxlength="255" id="copyright_text" value="{{ isset($settings->copyright_text)?($settings->copyright_text):'' }}">
                              @if ($errors->has('copyright_text'))
                              <span class="help-block">{{ $errors->first('copyright_text') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Contact Us Email:*</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Contact Us Email" name="contactus_email" maxlength="255" id="contactus_email" value="{{ isset($settings->contactus_email)?($settings->contactus_email):'' }}">
                              @if ($errors->has('contactus_email'))
                              <span class="help-block">{{ $errors->first('contactus_email') }}</span> 
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Address:*</label>
                           <div class="col-lg-6">
                              <textarea class="form-control m-input" placeholder="Enter Address" name="address" maxlength="255" id="address">{{ isset($settings->address)?($settings->address):'' }}</textarea>
                              @if ($errors->has('address'))
                              <span class="help-block">{{ $errors->first('address') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Facebook Link:</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Facebook Link" name="facebook_link" maxlength="255" id="facebook_link" value="{{ isset($settings->facebook_link)?($settings->facebook_link):'' }}">
                              @if ($errors->has('facebook_link'))
                              <span class="help-block">{{ $errors->first('facebook_link') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Twitter Link:</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Twitter Link" name="twitter_link" maxlength="255" id="twitter_link" value="{{ isset($settings->twitter_link)?($settings->twitter_link):'' }}">
                              @if ($errors->has('twitter_link'))
                              <span class="help-block">{{ $errors->first('twitter_link') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                     <div class="m-form__section m-form__section--first">
                        <div class="form-group m-form__group row">
                           <label class="col-lg-3 col-form-label">Instagram Link:</label>
                           <div class="col-lg-6">
                              <input type="text" class="form-control m-input" placeholder="Enter Instagram Link" name="instagram_link" maxlength="255" id="instagram_link" value="{{ isset($settings->instagram_link)?($settings->instagram_link):'' }}">
                              @if ($errors->has('instagram_link'))
                              <span class="help-block">{{ $errors->first('instagram_link') }}</span>
                              @endif
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="m-portlet__foot m-portlet__foot--fit">
                     <div class="m-form__actions m-form__actions">
                        <div class="row">
                           <div class="col-lg-3"></div>
                           <div class="col-lg-6">
                              <button type="submit" class="btn btn-success">Submit</button>
                              <a  href="{{ url('/') }}" class="btn btn-danger">Cancel</a>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <!--end::Form-->
            </div>
            <!--end::Portlet-->
         </div>
      </div>
   </div>
</div>
@endsection
@section('script')
<script>
   $(document).ready(function(){
     $("#frmUser").validate({
             // Specify vaidation rules
             rules: {    
               meta_title: {
                 required: true,
               },
               meta_keyword: {
                 required: true
               }, 
               meta_description: {
                 required: true
               },
               site_title: {
                  required: true
               },
               copyright_text: {
                 required: true,
               },
               contactus_email: {
                 required: true
               }, 
               address: {
                 required: true
               }
             },
             messages:{
               meta_title:{
                  required: "Please Enter Your Meta Title"
               },
               meta_keyword:{
                  required: "Please Enter Your Meta Keywords"
               },
               meta_description:{
                  required: "Please Enter Your Meta Descriptions"
               },
               site_title: {
                   required: "Please enter your site title",
               },
               copyright_text:{
                  required: "Please Enter Your Copyright Text"
               },
               contactus_email:{
                  required: "Please Enter Your Contact Us Email"
               },
               address: {
                 required:"Please enter your Address",
               }
          },
       });
       jQuery.validator.addMethod("lettersonly", function(value, element) {
          return this.optional(element) || /^[a-zA-Z\u0600-\u06FF,-][\sa-zA-Z\u0600-\u06FF,-]*$/.test(value);
       });
     });
</script>
@endsection