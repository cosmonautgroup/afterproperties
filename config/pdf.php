<?php 

	return [
		'mode'                 => 'utf-8',
		'format'               => 'A4',
		'default_font_size'    => '12',
		'default_font'         => 'arabic',
		'margin_left'          => 10,
		'margin_right'         => 10,
		'margin_top'           => 10,
		'margin_bottom'        => 10,
		'margin_header'        => 0,
		'margin_footer'        => 0,
		'orientation'          => 'P',
		'title'                => 'Laravel mPDF',
		'author'               => '',
		'watermark'            => '',
		'show_watermark'       => false,
		'watermark_font'       => 'arabic',
		'display_mode'         => 'fullpage',
		'watermark_text_alpha' => 0.1,
		'custom_font_dir'      => base_path('storage/fonts/'),
		'custom_font_data'     =>[
									'arabic' => [
											'R'  => 'XB Riyaz.ttf',    // regular font
											'B'  => 'DejaVuSerif-Bold.ttf',       // optional: bold font
											'I'  => 'DejaVuSerif-Italic.ttf',     // optional: italic font
											'BI' => 'DejaVuSerif-BoldItalic.ttf' // optional: bold-italic font
									]
									// ...add as many as you want.
								],
	];
	
?>